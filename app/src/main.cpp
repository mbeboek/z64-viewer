/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include <cstdio>
#include <exception>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
#include <websocketpp/base64/base64.hpp>

#include "cmd.h"
#include "rand.h"
#include "server/httpServer.h"
#include "server/wsServer.h"
#include "logger.h"
#include "fs.h"
#include "args.h"

using namespace std::chrono;
using namespace std::chrono_literals;

namespace
{
  namespace Commands
  {
    //constexpr u8 NOP = 0;
    constexpr u8 READ_FILE    = 1;
    constexpr u8 WRITE_FILE   = 2;
    constexpr u8 OPEN_DIR     = 3;
    constexpr u8 SET_BASE_DIR = 4;
    constexpr u8 SCAN_DIR     = 5;
    constexpr u8 EXEC_MAKE    = 6;
    constexpr u8 READ_CONFIG  = 7;
    constexpr u8 WRITE_CONFIG = 8;
  }

  std::string currentDir{};

  std::string openDirPicker()
  {
    std::string dir{};
    try {
      dir = CMD::exec("zenity --file-selection --directory --title='Open decomp directory (\"oot\")'");
    } catch(...) {
      dir = "";
    }

    if(dir.length()) {
      dir[dir.length()-1] = '/'; // replace newline with '/'
    }
    return dir;
  }

  void validateMakeTask(const std::string& name) {
    for(char c : name) {
      if(c >= 'a' && c <= 'z')continue;
      if(c >= 'A' && c <= 'Z')continue;
      if(c >= '0' && c <= '9')continue;
      if(c == '-' || c == '_')continue;
      throw std::runtime_error("Invalid Make task-name, only a-z, 0-9, '-' and '_' are allowed");
    }
  }
}

int main(s32 argc, char** argv)
{
  // Args
  EnvArgs args{argc, argv};
  auto browserName = args.getStringArg("--browser");
  auto urlOverride = args.getStringArg("--url");
  auto testMode = args.checkArg("--test-mode");

  if(testMode) {
    setbuf(stdout, NULL); // disable output buffering, messes with automated testing
  }

  // Directories & Files
  auto progDir = FS::getProgramDir();
  auto homeDir = FS::getHomeDir();
  auto configPath = homeDir + "/.config/kibako_config.json";

  // Generate key for ws auth
  auto key = Rand::getHexString();
  if (key.length() == 0) {
    print_error("Failed to fetch random bytes, abort")
    return 1;
  }

  //printf_info("Secret-Key: %s", key.c_str())

  WsServer wsServer{key, [configPath](u8 command, const std::string &arg, auto argData, auto cbStdOut, auto cbDone) {
    std::vector<u8> out{0,0,0,0};

    auto argFullPath = currentDir + arg;
    switch (command) {
      case Commands::READ_FILE:
        out = FS::readFile(argFullPath, 4);
        break;
      case Commands::WRITE_FILE:
        FS::writeFile(argFullPath, argData);
        break;

      case Commands::OPEN_DIR:
        currentDir = openDirPicker();
        out.insert(out.end(), currentDir.begin(), currentDir.end());
      break;

      case Commands::SET_BASE_DIR:
        currentDir = arg;
      break;

      case Commands::SCAN_DIR: {
        auto scanRes = FS::scanDir(argFullPath);
        for (const auto &entry: scanRes) {
          out.push_back(entry.isDir ? 'D' : 'F');
          out.insert(out.end(), entry.name.begin(), entry.name.end());
          out.push_back(0);
        }
      }
      break;

      // This command runs asynchronous:
      case Commands::EXEC_MAKE: {
        validateMakeTask(arg);

        // start sub-command in new thread, copy callbacks, and stream stdout
        auto cmd = "make -C " + currentDir + " -j " + arg;
        std::thread threadExec{[cmd, cbStdOut, cbDone] {
          std::vector<u8> out{0,0,0,0};
          try {
            CMD::exec(cmd, cbStdOut);
            cbDone(out);
          } catch(const std::exception &e) {
            printf_error("Command failed: %s", e.what())
            out[2] = 1; // mark as failed
            cbDone(out);
          }
        }};
        return threadExec.detach(); // avoids hanging up the WS thread (and allows streaming)
      }

      case Commands::READ_CONFIG: out = FS::readFile(configPath, 4); break;
      case Commands::WRITE_CONFIG: FS::writeFile(configPath, argData); break;

      default: printf_error("Unknown Command: %d", command)
    }

    cbDone(out);
  }};

  // Start websocket
  std::thread wsThread{[&wsServer](){
    wsServer.run();
  }};

  // Start HTTP-server
  HttpServer httpServer{progDir+"/data"};
  std::thread httpThread{[&httpServer](){
    httpServer.run();
  }};

  print_info("Waiting for ws-server...")
  while(!wsServer.isReady())std::this_thread::sleep_for(1ms);

  print_info("Waiting for http-server...")
  while(!httpServer.isReady())std::this_thread::sleep_for(1ms);

  print_info("Server ready");
  auto wsUrl = wsServer.getUrl();

  auto indexHtmlUrl = urlOverride.length()
    ? urlOverride
    : httpServer.getUrl() + "/index.html";

  printf_info("Target-URL: %s", indexHtmlUrl.c_str());

  auto wsInfoJson = "{"
    "\"key\":\"" + key + "\","
    "\"url\":\"" + wsUrl + "\""
  "}";

  if(!browserName.length()) {
    browserName = "xdg-open";
  }

  auto targetUrl = indexHtmlUrl + "?wsInfo=" + websocketpp::base64_encode(wsInfoJson);
  if(testMode) {
    printf_info("URL: %s", targetUrl.c_str())
  } else {
    auto cmd = browserName + " " + targetUrl;
    CMD::exec(cmd);
  }

  // main-thread loop
  u32 noConnCount = 0;
  for(;;) {
    std::this_thread::sleep_for(100ms);

    // stop if the websocket has no connection for 5 seconds
    noConnCount = (wsServer.getActiveConnections() > 0) ? 0 : (noConnCount+1);
    if(noConnCount > 50) {
      print_info("No live connections, abort")
      break;
    }
  }

  // shut-down
  print_info("Shutting down WS-Server...")
  wsServer.stop();
  wsThread.join();

  print_info("Shutting down HTTP-Server...")
  httpServer.stop();
  httpThread.join();

  print_info("Bye!")
  return 0;
}
