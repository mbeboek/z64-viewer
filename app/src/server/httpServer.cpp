/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#include "httpServer.h"
#include "logger.h"
#include <httplib.h>

namespace {
  httplib::Server *serverInstance{nullptr};
}

void HttpServer::run() {

  httplib::Server server;
  serverInstance = &server;

  printf_info("Mount HTTP-Server to: %s", basePath.c_str())
  auto ret = server.set_mount_point("/", basePath);
  if (!ret) {
    throw std::runtime_error("Could not mount dir to HTTP server: " + basePath);
  }

  port = static_cast<u32>(server.bind_to_any_port("127.0.0.1"));
  serverReady = true;

  printf_info("HTTP listening on: 127.0.0.1:%d", port.load())
  server.listen_after_bind();
}

void HttpServer::stop() {
  if(!serverInstance)return;
  serverInstance->stop();
  serverInstance = nullptr;
}
