/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include <atomic>
#include <string>
#include "types.h"

class HttpServer
{
  private:
    std::string basePath;
    std::atomic_bool serverReady{false};
    std::atomic<u32> port{0};

  public:
    explicit HttpServer(std::string basePath)
      : basePath{std::move(basePath)}
    {}

    void run();
    void stop();

    bool isReady() { return serverReady.load(); }
    u32 getPort() { return port.load(); }

    std::string getUrl() {
      return "http://127.0.0.1:" + std::to_string(getPort());
    }
};