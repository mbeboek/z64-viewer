/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#include "rand.h"
#include "types.h"

#ifdef _WIN32
    #include <windows.h>
#else
    #include <sys/random.h>
#endif

namespace {
  constexpr u32 KEY_BYTE_LENGTH = 64;
  constexpr char HEX_CHARS[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
}

std::string Rand::getHexString()
{
  u8 keyBuffer[KEY_BYTE_LENGTH];
  #ifdef _WIN32
    BCryptGenRandom(nullptr, keyBuffer, sizeof(keyBuffer), BCRYPT_USE_SYSTEM_PREFERRED_RNG);
  #else
    if(getentropy(keyBuffer, KEY_BYTE_LENGTH)) {
      return "";
    }
  #endif

  std::string s(KEY_BYTE_LENGTH * 2, '0');
  for (u32 i=0; i<KEY_BYTE_LENGTH; ++i) {
    s[2*i + 0] = HEX_CHARS[(keyBuffer[i] & 0xF0) >> 4];
    s[2*i + 1] = HEX_CHARS[(keyBuffer[i] & 0x0F) >> 0];
  }
  return s;
}
