/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include <string>
#include <unordered_map>
#include "types.h"

class EnvArgs
{
  private:
    EnvArgs(const EnvArgs&) = delete;
    EnvArgs(EnvArgs&&) = delete;
    std::unordered_map<std::string, std::string> argMap;

  public:
    EnvArgs(s32 argc, char** argv)
    {
      for(auto i=1; i<argc; ++i)
      {
        auto arg = std::string(argv[i]);

        if(arg.length() < 2 || arg[0] != '-')continue;
        auto equalPos = arg.find('=');
        auto argName = arg.substr(0, equalPos);
        auto argVal = equalPos == std::string::npos ? "" : arg.substr(equalPos+1);
        argMap[argName] = argVal;
      }
    }

    bool checkArg(const std::string &argName) const
    {
      return argMap.contains(argName);
    }

    std::string getStringArg(const std::string &argName)
    {
      return argMap[argName];
    }

    u32 getU32Arg(const std::string &argName) {
      return static_cast<u32>(std::stoi(argMap[argName]));
    }
};
