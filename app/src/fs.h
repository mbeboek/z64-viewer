/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include <exception>
#include <string>
#include <filesystem>
#include <string_view>
#include <vector>

namespace FS
{
  struct ScanDirEntry {
    std::string name{};
    bool isDir{false};
  };

  inline std::vector<u8> readFile(const std::string &filePath, u32 paddingStart = 0)
  {
    // fs stat is faster than scanning to the end of file
    auto fileSize = std::filesystem::file_size(filePath);
    // old fopen() is faster than stream
    FILE* f = fopen(filePath.data(), "rb");
    if(!f) {
      throw std::runtime_error("Failed to open file");
    }

    std::vector<u8> res(fileSize + paddingStart);
    fread(&res[0] + paddingStart, sizeof(char), fileSize, f);
    fclose(f);
    return res;
  }

  inline void writeFile(const std::string &filePath, std::span<const u8> data)
  {
    FILE* f = fopen(filePath.data(), "wb");
    if(!f) {
      throw std::runtime_error("Failed to open file for write");
    }

    if(!fwrite(data.data(), data.size_bytes(), 1, f)) {
      fclose(f);
      throw std::runtime_error("Failed to write data");
    }
    fclose(f);
  }

  inline std::vector<ScanDirEntry> scanDir(const std::string &path)
  {
    std::vector<ScanDirEntry> res{};
    for(const auto &file: std::filesystem::directory_iterator(path)) {
      res.emplace_back(file.path().filename().string(), !file.is_regular_file());
    }
    return res;
  }

  inline std::string getProgramDir() {
    return std::filesystem::canonical("/proc/self/exe").parent_path();
  }

  inline std::string getHomeDir() {
    return getenv("HOME");
  }
}