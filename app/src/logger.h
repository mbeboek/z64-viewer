/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include <iostream>
#include "types.h"

#define FEATURE_DEBUG_LOG 1

// __LINE__ as a string, only use S__LINE__
#define S_LINE_STR1(x) #x
#define S_LINE_STR2(x) S_LINE_STR1(x)
#define S_LINE_ S_LINE_STR2(__LINE__)

#undef ERROR // windows bullshit

enum class LoggingLevelMask
{
  NONE  = 0,
  ERROR = (1 << 1),
  WARN  = (1 << 2),
  INFO  = (1 << 3),
  TRACE = (1 << 4),
  ALL   = 0xFFFF,
};

constexpr LoggingLevelMask operator |(LoggingLevelMask a, LoggingLevelMask b) {
    return (LoggingLevelMask)((s32)a | (s32)b);
}
constexpr LoggingLevelMask operator &(LoggingLevelMask a, LoggingLevelMask b) {
    return (LoggingLevelMask)((s32)a & (s32)b);
}
constexpr bool32 operator !(LoggingLevelMask a) {
    return !(bool32)a;
}

inline LoggingLevelMask loggingLevel{LoggingLevelMask::ALL};

#ifdef FEATURE_DEBUG_LOG
  #define TRACE_STR "[" __FILE__  ":" S_LINE_ "] "

  #define printf_error(str, ...) if((s32)loggingLevel & (s32)LoggingLevelMask::ERROR) { printf("\033[31;7m" "[ERRO] " TRACE_STR str "\033[0m\n", __VA_ARGS__); }
  #define printf_warn(str, ...)  if((s32)loggingLevel & (s32)LoggingLevelMask::WARN ) { printf("\033[33;7m" "[WARN] " TRACE_STR str "\033[0m\n", __VA_ARGS__); }
  #define printf_info(str, ...)  if((s32)loggingLevel & (s32)LoggingLevelMask::INFO ) { printf(             "[INFO] " TRACE_STR str        "\n", __VA_ARGS__); }
  #define printf_trace(str, ...) if((s32)loggingLevel & (s32)LoggingLevelMask::TRACE) { printf("\033[30;1m" "[TRAC] " TRACE_STR str "\033[0m\n", __VA_ARGS__); }
  //#define printf_trace(...)
#else
  #define TRACE_STR

  #define printf_error(...)
  #define printf_warn(...)
  #define printf_info(...)
  #define printf_trace(...)
#endif

#define print_error(str) printf_error("%s", str)
#define print_warn(str) printf_warn("%s", str)
#define print_info(str) printf_info("%s", str)
#define print_trace(str) printf_trace("%s", str)
