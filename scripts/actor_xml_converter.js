/**
 * Converts the Fast64 "ActorList.xml" to json that Kibako can read.
 * The resulting JSON should not be modified, to allow updates in the future.
 * 
 * Original XML Credits/Link: 
 * https://github.com/Fast-64/fast64/blob/main/fast64_internal/oot/data/xml/ActorList.xml
 */

const XMLParser = require("./fxparser.min.js");
const fs = require("fs");

const game = process.argv[2] === "mm" ? "mm" : "oot";
const xmlFile = fs.readFileSync(process.argv[3]);
const objListFile = process.argv[4] ? fs.readFileSync(process.argv[4]) : "";

const parser = new XMLParser({
  ignoreAttributes : false,
  allowBooleanAttributes: true
});

function getShiftAmount(mask) {
  for(let i=0; i<16; ++i) {
    if(mask & 1)return i;
    mask >>= 1;
  }
  return 0;
}

function forceArray(xmlElem) {
  if(!xmlElem)return [];
  return Array.isArray(xmlElem) ? xmlElem : [xmlElem];
}

function getBasicData(xmlElem) 
{
  const typeFilter = xmlElem["@_TiedActorTypes"] 
    ? xmlElem["@_TiedActorTypes"].split(",").map(x => parseInt(x, 16))
    : [];

  return {
    typeFilter,
    target: (xmlElem["@_Target"] || "params").toLowerCase(),
    shift: 0,
  }
}

const objectEnumMap = {
  obj_gameplay_field_keep: "OBJECT_GAMEPLAY_FIELD_KEEP",
  obj_gameplay_dangeon_keep: "OBJECT_GAMEPLAY_DANGEON_KEEP"

}; // XML key to decomp enum name map

if(objListFile.length) {
  const xmlObjects = parser.parse(objListFile);
  for(const objEntry of xmlObjects.Table.Object) {
    objectEnumMap[objEntry["@_Key"]] = objEntry["@_ID"];
  }
}

const xml = parser.parse(xmlFile);
const xmlActors = xml.Table.Actor;

const actors = {};
let collectibles = [];
let chestContents = [];

for(const xmlActor of xmlActors)
{
  let objDeps = [];
  if(xmlActor["@_ObjectKey"]) {
    objDeps = xmlActor["@_ObjectKey"].split(",").map(x => objectEnumMap[x] || x);
  }

  const actor = {
    name: xmlActor["@_Name"],
    category: xmlActor["@_Category"],
    objDeps,
    params: [],
    notes: xmlActor.Notes ? (Array.isArray(xmlActor.Notes) ? xmlActor.Notes.join("\n") : xmlActor.Notes) : ""
  };
  const actorId = xmlActor["@_ID"];

  // Main Type / Selectbox
  if(xmlActor.Type) 
  {
    const maskStr = (xmlActor.Type["@_Mask"] || "").replace("0x", "");
    actor.params.push({
      ...getBasicData(xmlActor.Type),
      type: 'type',
      name: "Type",
      mask:  maskStr === "" ? 0xFFFF : parseInt(maskStr, 16), 
      values: forceArray(xmlActor.Type.Item)
        .map(entry => ({
          value: parseInt(entry["@_Params"], 16) + "", 
          name: entry["#text"].replace(/\/\/.*/, "").trim()
        }))
    });
  }

  // Sub-Types / Enum
  const xmlEnums = forceArray(xmlActor.Enum);
  if(xmlEnums.length) 
  {
    actor.params.push(...xmlEnums
      .sort((a,b) => parseInt(a["@_Index"]) - parseInt(b["@_Index"]))
      .map(entry => {
        const maskStr = (entry["@_Mask"] || "").replace("0x", "");
        return {
          ...getBasicData(entry),
          type: 'enum',
          name: entry["@_Name"].trim(),
          mask:  maskStr === "" ? 0xFFFF : parseInt(maskStr, 16), 
          values: entry.Item
            .map(item => ({
              value: parseInt(item["@_Value"].replace("0x", ""), 16) + "", 
              name: item["@_Name"].replace(/\/\/.*/, "").trim()
            }))
        };
      })
    );
  }

  // Collectible
  if(xmlActor.Collectible) 
  {
    const xmlCol = xmlActor.Collectible;
    
    const maskVal = parseInt(xmlCol["@_Mask"].replace("0x", ""), 16);
    actor.params.push({
      ...getBasicData(xmlCol),
      type: 'collectible',
      name: "Item",
      mask:  maskVal,
      shift: getShiftAmount(maskVal),
    });
  }


  // Chest Content
  if(xmlActor.ChestContent) 
  {
    const xmlCol = xmlActor.ChestContent;
    
    const maskVal = parseInt(xmlCol["@_Mask"].replace("0x", ""), 16);
    actor.params.push({
      ...getBasicData(xmlCol),
      type: 'chest',
      name: "Content",
      mask:  maskVal,
      shift: getShiftAmount(maskVal),
    });
  }

  // Masked Integer
  const xmlProps = forceArray(xmlActor.Property);
  if(xmlProps.length) {
    actor.params.push(...xmlProps
      .sort((a,b) => parseInt(a["@_Index"]) - parseInt(b["@_Index"]))
      .map(entry => {
        const maskVal = parseInt(entry["@_Mask"].replace("0x", ""), 16);
        return {
          ...getBasicData(entry),
          type: "number",
          name: entry["@_Name"].replace(/\/\/.*/, "").trim(),
          mask: maskVal, 
          shift: getShiftAmount(maskVal)
        };
      })
    );
  }

  // Flags (masked values?)
  const xmlFlags = forceArray(xmlActor.Flag);
  if(xmlFlags.length) {
    actor.params.push(...xmlFlags
      .sort((a,b) => parseInt(a["@_Index"]) - parseInt(b["@_Index"]))
      .map(entry => {
          const maskVal = parseInt(entry["@_Mask"].replace("0x", ""), 16);
          return {
          ...getBasicData(entry),
          type: "flag",
          name: entry["@_Type"],
          mask: maskVal,
          shift: getShiftAmount(maskVal),
        };
      })
    );
  }

  // Bool / Checkbox
  const xmlBools = forceArray(xmlActor.Bool);
  if(xmlBools.length) {
    actor.params.push(...xmlBools
      .sort((a,b) => parseInt(a["@_Index"]) - parseInt(b["@_Index"]))
      .map(entry => ({
        ...getBasicData(entry),
        type: "bool",
        name: entry["@_Name"].replace(/\/\/.*/, "").trim(),
        mask: parseInt(entry["@_Mask"].replace("0x", ""), 16),
      }))
    );
  }

  //console.log(actorId, xmlActor);
  actors[actorId] = actor;
}

// Collectibles
const xmlCol = xml.Table.List.find(x => x["@_Name"] === "Collectibles")
collectibles = new Array(25).fill(0).map((_, i) => ({value: i+"", name: "[Unused 0x"+i.toString(16)+"]"}));
for(const xmlEntry of xmlCol.Item) {
  const id = parseInt(xmlEntry["@_Value"].toLowerCase().replace("0x", ""), 16);
  //collectibles[id] = [xmlEntry["@_Key"], xmlEntry["@_Name"]];
  collectibles[id] = {value: id+"", name: xmlEntry["@_Name"]};
}

// Chest-Content
const xmlChest = xml.Table.List.find(x => x["@_Name"] === "Chest Content")
chestContents = new Array(120).fill(0).map((_, i) => ({value: i+"", name: "[Unused 0x"+i.toString(16)+"]"}));
for(const xmlEntry of xmlChest.Item) {
  const id = parseInt(xmlEntry["@_Value"].toLowerCase().replace("0x", ""), 16);
  //collectibles[id] = [xmlEntry["@_Key"], xmlEntry["@_Name"]];
  chestContents[id] = {value: id+"", name: xmlEntry["@_Name"]};
}

fs.writeFileSync(
  "./src/backend/z64/actors/"+game+"/actorData.ts", 
`//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
import { ActorData } from '../actorDataType';
export const actorData: ActorData = ` +
  JSON.stringify(actors, null, 2).replaceAll("’", "'")
  + ";\n"
);

fs.writeFileSync(
  "./src/backend/z64/actors/"+game+"/collectibleData.ts", 
`//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
export const collectibleData: Array<{name: string, value: string}> = ` +
  JSON.stringify(collectibles, null, 2)
  + ";\n"
);

fs.writeFileSync(
  "./src/backend/z64/actors/"+game+"/chestData.ts", 
`//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
export const chestData: Array<{name: string, value: string}> = ` +
  JSON.stringify(chestContents, null, 2)
  + ";\n"
);