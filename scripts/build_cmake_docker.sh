# Run this inside docker with:
# docker run -it -v ${PWD}:/src silkeh/clang:16 sh
set -e

cd app

# Build C++
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -Bbuild
cmake --build build --target kibako_native -- -j 4
