# Kibako
A web-based tool to view, edit and (partially) emulate maps in Decomp-OOT.

## Usage
The Web-Version / Native-App can be found at: https://kibako.gitlab.io 
<br>
At the moment, only Chromium-based browsers are supported in the web version.<br>
For the native app, only linux build are available, but they are able to run inside WSL.

### Linux
Make sure that both `zenity` and `xdg-open` are installed.<br>
Extract the app, and run `kibako`, a new tab should open in your browser.<br>
<br>

#### Extended settings
To overwrite the browser used by `xdg-open`, run:
```sh
kibako --browser=firefox 
```
If, for whatever reason, you want to host your own server, run:
```sh
kibako --url=http://example.com:12345 
```
When using the URL parameter, make sure to **not** add a slash.
The path and key is appended automatically upon start. 

### Window (WSL)

TODO: works, but docs are needed (Pro-Tip: don't use Windows)

<br>
<hr>

## Project Structure
This project has two main components: the web-app, and a native launcher app.<br>
Most of the logic is located inside the web-app, while the native-app provides extended access to OS features such as file access.

### Web-App

The web-app has two components, the backend and the front-end.<br>
Since this tools runs only locally, the "backend" is just normal code included in the repo.<br>
However both are strictly separated in `src/backend` and `src/frontend`, while the front-end has a dependency on the backend.

#### Backend
Contains logic to read and convert data from a ROM/Decomp, as well as N64/Z64 specific code including emulation.<br>
The code itself is structured into 3 groups:
- `n64` - N64 logic, independent of the game
- `z64` - Z64 specific code
- `utils` - Generic helper
- `native` / `fileapi` - Helper for file-access and the native app

#### Frontend
The Front-end is written in React using TypeScript and Components.<br>
Whenever any game specific logic or ROM access is needed, functions from the backend can be imported and called.<br>
Rendering is done using three.js, with the "react-three-fiber" package to link the two.<br>

#### Emulation
Emulation is handled in a custom emulator i wrote in C++, running via WebAssembly.<br>
It has reduced functionality (a "headless-N64"), and has an API to manually call functions.<br>
Note that this tool does not fully emulate the game, but rather manually patches the memory and runs actors in isolation.

The code is in a separate project: https://gitlab.com/kibako/kibako64

## Native-App

The native app is written in C++, and provides a websocket as well as a simple static-webserver to launch the app in your browser.<br>
The webserver hosts the same files as the web-version does, while the websocket provides a fast and secure API to access files.<br>
Sources are located in `./app` and can be build using CMake.

## Security
Since files are accessed via a websocket, security checks must be in place to prevent misuse.<br>
This mainly focuses on preventing websites in your browser from accessing the socket.<br>
(On a side-note, a lot of people don't seem to know that any website can just access localhost.)
<br>
<br>
The following security checks are in place:
- Only bind to localhost (loopback only)
- Secret key:
  - In order to connect, a 64-byte long key must be provided,<br>
    a fresh key is generated on each start using crypto. secure RNG
  - The key is passed to the browser in the URL itself as a GET parameter
- Origin-header check
- Random port on each start
- Only allow 1 connection at a time
- Stop if nothing is connected for 5s. 
- The file API cannot delete files

Things that are not secured:
- The webserver, since it only serves static files
- The connection itself (HTTP only):
  - If something on your PC can sniff the network, they might as well just access files directly
  - Since this is loopback only, other devices in your network can't see the data. 

<br>
<hr>

## Building

### Web-App
After checking out, make sure you have NodeJS (v20) installed and run:
```
yarn install
```

To start in dev-mode, run:
```
yarn start
```

For a production build, run:
```
yarn build
```

### Native-App

TODO (it's just CMake)

### Native Dev-Mode

To test the native app, while still using hot-reloading, start the script:
```
./scripts/start.sh
```

Make sure to build the app project once before doing that.
The executable is expected to be located at `./app/cmake-build-release/kibako_native`

<br>
<hr>

## Credits
Tool made by `Max Bebök`, aka `HailToDodongo`.

Documentation of all file formats where taken from: https://wiki.cloudmodding.com/oot/Main_Page
