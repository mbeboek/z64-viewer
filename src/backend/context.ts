/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export enum Game{ 
  OOT, MM
};

/**
 * Global context.
 * This data should not change, and only be set during the initial load.
 * Note: Keep number of members to an absolute minimum
 */
export const Context = {
  rom: new Uint8Array([]),
  game: Game.OOT,

  isOOT: () => Context.game === Game.OOT,
  isMM: () => Context.game === Game.MM,
};