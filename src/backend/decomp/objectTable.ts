/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import FileAPI from "../fileapi/fileAPI";
import { strip } from "./c/parser";

export let objectTableDecomp: ObjectTable = {
  list: [],
  enum: {}
};

export interface ObjectTable {
  list: ({
    name: string;
    varName: string;
  } | undefined)[];
  enum: Record<string, number>;
}

export const parseObjectTable = async (): Promise<ObjectTable> =>
{
  objectTableDecomp = {
    list: [],
    enum: {}
  };

  const tableFile = strip(await FileAPI.readFileText("include/tables/object_table.h"));

  const lines = tableFile.split("\n");
  for(let line of lines) 
  {
    line = line.trim();
    const macro = line.match(/DEFINE_OBJEC[_A-Z]+\((.*)\)/m);
    if(macro && macro[1]) 
    {
      if(macro[0].startsWith("DEFINE_OBJECT_UNSET") || macro[0].startsWith("DEFINE_OBJECT_NULL")) {
        objectTableDecomp.list.push(undefined);
        continue;
      }

      const args = macro[1].split(",").map(x => x.trim());
      const name = args[1] || "";

      objectTableDecomp.enum[name] = objectTableDecomp.list.length;
      objectTableDecomp.list.push({
        name,
        varName: args[0] || "",
      });
    }
  }

  return objectTableDecomp;
}

export const parseObjectTableCached = async (): Promise<ObjectTable> =>
{
  if(objectTableDecomp.list.length === 0) {
    await parseObjectTable();
  }
  return objectTableDecomp;
}