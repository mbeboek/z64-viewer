
import FileAPI from "../fileapi/fileAPI";
import { parseObjectTable } from "./objectTable";

describe('Decomp - ObjectTable', () => 
{
  let testFileObject = "";

  FileAPI.setAPI({
    type: FileAPI.APIType.NODE,
    readFileBuffer: async function (path: string): Promise<ArrayBuffer> {
      throw new Error("Tried to read unknown file: " + path);
    },
    readFileText: async function (path: string): Promise<string> {
      switch(path) {
        case 'include/tables/object_table.h': return testFileObject;
        default: throw new Error("Tried to read unknown file: " + path); break;
      }
    },
    writeFileText: async function(){},
    openDir: async function(){},
    scanDir: async function() {
      return {files: [], dirs: []};
    },
  });

  test("Parse empty", async () => {
    testFileObject = "";

    const table = await parseObjectTable();
    expect(table).toEqual({
      list: [],
      enum: {},
    });
  });

  test("Parse example", async () => {

    testFileObject = `/**
* Blah
* Some comment
*/

/* 0x0000 */ DEFINE_OBJECT(object_a, OBJECT_A)
/* 0x0001 */ DEFINE_OBJECT(object_b, OBJECT_B)

/* 0x0002 */ DEFINE_OBJECT_UNSET(OBJECT_UNUSED_00)
/* 0x0003 */ DEFINE_OBJECT_NULL(object_c, OBJECT_C)

/* 0x0004 */ DEFINE_OBJECT(object_d, OBJECT_D)

    `;
    const table = await parseObjectTable();

    expect(table).toEqual({
      list: [
        {name: "OBJECT_A", varName: "object_a"},
        {name: "OBJECT_B", varName: "object_b"},
        undefined,
        undefined,
        {name: "OBJECT_D", varName: "object_d"},
      ],
      enum: {
        OBJECT_A: 0x00,
        OBJECT_B: 0x01,
        OBJECT_D: 0x04,
      }
    });
  });
});
