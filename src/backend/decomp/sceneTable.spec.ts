import { Decomp } from "./sceneTable";
import FileAPI from "../fileapi/fileAPI";

describe('Decomp - SceneTable', () => 
{
  let testFileScene = "";
  let testFileSceneHeader = "";
  let testFileSpec = "";

  FileAPI.setAPI({
    type: FileAPI.APIType.NODE,
    readFileBuffer: async function (path: string): Promise<ArrayBuffer> {
      throw new Error("Tried to read unknown file: " + path);
    },
    readFileText: async function (path: string): Promise<string> {
      switch(path) {
        case 'spec': return testFileSpec;
        case 'include/tables/scene_table.h': return testFileScene;
        case 'include/z64scene.h': return testFileSceneHeader;
        default: throw new Error("Tried to read unknown file: " + path); break;
      }
    },
    writeFileText: async function(){},
    openDir: async function(){},
    scanDir: async function() {
      return {files: [], dirs: []};
    },
  });

  test("Parse empty", async () => {
    testFileScene = "";
    testFileSpec = "";
    testFileSceneHeader = "";

    const table = await Decomp.parseSceneTable();
    expect(table).toEqual([]);
  });

  test("Parse example", async () => {
    testFileSpec = `
some other garbage

beginseg
    name "test_scene0"
    romalign 0x1000
    include "build/assets/scenes/test_scene0.o"
    number 2
endseg

beginseg
    name "test_scene2"
    romalign 0x1000
    include "build/assets/scenes/test_levels/pathA/pathB/test_scene2.o"
    number 3
endseg

some more garbage
`;

    testFileScene = `/**
 * Scene Table
 *
 * DEFINE_SCENE should be used for all scenes (with or without a title card, see argument 2)
 */
/* 0x00 */ DEFINE_SCENE(test_scene0, aa, SCENE_TEST0, SDC_TEST0, 0, 1)
/* 0x01 */ DEFINE_SCENE(test_scene1, bb, SCENE_TEST1, SDC_TEST1, 1, 3)

// DEFINE_SCENE(unused_scene, xx, SCENE_UNUSED, SDC_UNUSED, 1, 4)
/* DEFINE_SCENE(some_other_garbage, yy, SCENE_IDC, NULL, 2, 5) */
/* 0x42 */ DEFINE_SCENE(test_scene2, cc, SCENE_TEST2, SDC_TEST3, 5, 5)
    `;

    testFileSceneHeader = `
    other garbage

    typedef enum {
      /*  0 */ SDC_TEST0,
      /*  1 */ SDC_TEST1,
      /*  2 */ SDC_TEST2,
      /*  3 */ SDC_TEST3,
    } SceneDrawConfig;

    even more garbage
    `;

    const table = await Decomp.parseSceneTable();

    expect(table).toEqual([      
      {
        name: 'test_scene0',
        path: 'assets/scenes',
        titleName: 'aa',
        enumName: 'SCENE_TEST0',
        drawCfgEnum: 'SDC_TEST0',
        drawCfgId: 0,
      },
      {
        name: 'test_scene1',
        path: '',
        titleName: 'bb',
        enumName: 'SCENE_TEST1',
        drawCfgEnum: 'SDC_TEST1',
        drawCfgId: 1,
      },
      {
        name: 'test_scene2',
        path: 'assets/scenes/test_levels/pathA/pathB',
        titleName: 'cc',
        enumName: 'SCENE_TEST2',
        drawCfgEnum: 'SDC_TEST3',
        drawCfgId: 3,
      }
    ]);
  });
});
