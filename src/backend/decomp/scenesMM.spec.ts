import { Game } from "../context";
import { SceneActorDependency } from "../z64/scenes/sceneList";
import { parseScene } from "./scenes";
//import { Decomp } from "./actorTable";

describe('Decomp - MM - Scenes', () => 
{
  test("Example - Single Layer", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_sceneCommands[] = {
    SCENE_CMD_SOUND_SETTINGS(1,2,3),
    SCENE_CMD_TRANSITION_ACTOR_LIST(3, someroom_sceneTransitionActorList_000084),
    SCENE_CMD_SPECIAL_FILES(2, OBJECT_GAMEPLAY_FIELD_KEEP),
    SCENE_CMD_SKYBOX_SETTINGS(0x42, 0, 0, true),
    SCENE_CMD_ENV_LIGHT_SETTINGS(2, someroom_sceneLightSettings0x0000BB),
    SCENE_CMD_PATH_LIST(someroom_scenePathway),
    SCENE_CMD_END(),
};

TransitionActorEntry someroom_sceneTransitionActorList_000084[] = {
  { 0, 255, 1, 255, ACTOR_EN_ABC, 10, 20, 30, 11647, 0x0100 },
  { 2, 255, 3, 255, ACTOR_EN_DEF, 10, 20, 30, 42, 0x0101 },
  { 4, 255, 5, 255, ACTOR_EN_ABC, 10, 20, 30, 11647, 0x0102 },
};

u8 someroom_scene_possiblePadding_000FFF = 42;

LightSettings someroom_sceneLightSettings0x0000BB[] = {
  { 0x68, 0x5A, 0x50, 0x49, 0x49, 0x33, 0xFF, 0xFF, 0xFA, 0xB7, 0xB7, 0xB7, 0x32, 0x32, 0x5A, 0x64, 0x64, 0x78, 0x07C0, 0x0FA0 },
  { 0x10, 0x5A, 0x50, 0x49, 0x49, 0x33, 0xFF, 0xFF, 0xFA, 0xB7, 0xB7, 0xB7, 0x32, 0x32, 0x5A, 0x64, 0x64, 0x78, 0x07C0, 0x0FA0 },
};

Vec3s someroom_scenePathway_PointA[] = {
  {  -1200,   200,   -44 },
};

Vec3s someroom_scenePathway_PointB[] = {
  {  1, 2, 3 },
  {  4, 5, 6 },
};

Path someroom_scenePathway[] = {
  { 4, 1, -1,  someroom_scenePathway_PointA },
  { 4, -1, 42, someroom_scenePathway_PointB },
};

`;
    
    const actorTable = {
      list: [],
      enum: {
        ACTOR_EN_ABC: 0x10,
        ACTOR_EN_DEF: 0x42,
      }
    };

    const roomData = parseScene(src, "someroom", actorTable, Game.MM);

    roomData.paths.someroom_scenePathway[0].uuid = "UUID";
    roomData.paths.someroom_scenePathway[1].uuid = "UUID";

    expect(roomData).toEqual({
      
        mainHeader: {
          name: "someroom_sceneCommands",
          map: {
            SCENE_CMD_SOUND_SETTINGS: [1, 2, 3],
            SCENE_CMD_TRANSITION_ACTOR_LIST: [3, "someroom_sceneTransitionActorList_000084"],
            SCENE_CMD_SPECIAL_FILES: [2, "OBJECT_GAMEPLAY_FIELD_KEEP"],
            SCENE_CMD_SKYBOX_SETTINGS: [0x42, 0, 0, "true"],
            SCENE_CMD_ENV_LIGHT_SETTINGS: [2, "someroom_sceneLightSettings0x0000BB"],
            SCENE_CMD_PATH_LIST: ["someroom_scenePathway"],
            SCENE_CMD_END: [],
          },
          indices: {
            SCENE_CMD_SOUND_SETTINGS: 0,
            SCENE_CMD_TRANSITION_ACTOR_LIST: 1,
            SCENE_CMD_SPECIAL_FILES: 2,
            SCENE_CMD_SKYBOX_SETTINGS: 3,
            SCENE_CMD_ENV_LIGHT_SETTINGS: 4,
            SCENE_CMD_PATH_LIST: 5,
            SCENE_CMD_END: 6,
          }
        },
        altHeaders: [],
        actors: {
          someroom_sceneTransitionActorList_000084: [
            {
              roomFront: 0,
              camIdxFront: 255,
              roomBack: 1,
              camIdxBack: 255,
              id: 0x10,
              pos: [10, 20, 30],
              rotY: 16384,
              params: 0x0100,
              cutsceneIndex: 127,
            },
            {
              roomFront: 2,
              camIdxFront: 255,
              roomBack: 3,
              camIdxBack: 255,
              id: 0x42,
              pos: [10, 20, 30],
              rotY: 0,
              params: 0x0101,
              cutsceneIndex: 42,
            },
            {
              roomFront: 4,
              camIdxFront: 255,
              roomBack: 5,
              camIdxBack: 255,
              id: 0x10,
              pos: [10, 20, 30],
              rotY: 16384,
              params: 0x0102,
              cutsceneIndex: 127,
            },
          ]
        },
        lighting: {
          someroom_sceneLightSettings0x0000BB: [
            {
              ambientColor: [104, 90, 80],
              diff0Dir: [73, 73, 51],
              diff0Color: [255, 255, 250],
              diff1Dir: [-73, -73, -73],
              diff1Color: [50, 50, 90],
              fogColor: [100, 100, 120],
              forStart: 1984,
              drawDist: 4000
            },
            {
              ambientColor: [16, 90, 80],
              diff0Dir: [73, 73, 51],
              diff0Color: [255, 255, 250],
              diff1Dir: [-73, -73, -73],
              diff1Color: [50, 50, 90],
              fogColor: [100, 100, 120],
              forStart: 1984,
              drawDist: 4000
            }
          ]
        },
        dependency: SceneActorDependency.FIELD,
        collision: {
          normals: [],
          vertices: [],
          verticesNoIndex: [],
          waterBoxes: [],
        },
        textureBank: 0x42,
        sound: {
          someroom_sceneCommands: {
            preset: 1,
            ambience: 2,
            bgm: 3,
          }
        },
        paths: {
          someroom_scenePathway: [
            {
              uuid: "UUID",
              additionalPathIdx: 1, // MM only
              customValue: -1, // MM only
              points: [
                [-1200,   200,   -44],
              ]
            },
            {
              uuid: "UUID",
              additionalPathIdx: -1, // MM only
              customValue: 42, // MM only
              points: [
                [1, 2, 3 ],
                [4, 5, 6 ],
              ]
            }
          ]
        }
    });
  });
});
