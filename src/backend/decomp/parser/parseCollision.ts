/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { CollisionData } from "../../z64/collision/collisionTypes";
import { DataObject, u16ToS16 } from "../c/parser";
import { COLPOLY_SNORMAL } from "../c/utils";

const EMPTY_VERT = [0,0,0];

export function parseCollision(vars: DataObject[], collVar: DataObject, collision: CollisionData) 
{
  let offset = 0;
  if(!Array.isArray(collVar.data[0]))offset += 2;
  if(!Array.isArray(collVar.data[1]))offset += 2;
  //const aabbMin = collVar.data[0];
  //const aabbMax = collVar.data[1];
  let vertVar  = vars.find(v => v.name === collVar.data[offset + 3]);
  let polyVar  = vars.find(v => v.name === collVar.data[offset + 5]);
  let waterVar = vars.find(v => v.name === collVar.data[offset + 9]);

  if(vertVar && polyVar) {
    collision.vertices = (vertVar.data as any[]).flatMap(v => v);

    for(const poly of polyVar.data) {
      
      const vertA = vertVar.data[poly[1] & 0x1FFF] || EMPTY_VERT;
      const vertB = vertVar.data[poly[2] & 0x1FFF] || EMPTY_VERT;
      const vertC = vertVar.data[poly[3] & 0x1FFF] || EMPTY_VERT;
      
      collision.verticesNoIndex.push(
        vertA[0], vertA[1], vertA[2],
        vertB[0], vertB[1], vertB[2],
        vertC[0], vertC[1], vertC[2],
      );

      const normRaw = [
        Array.isArray(poly[4]) ? COLPOLY_SNORMAL(poly[4][1]) : poly[4],
        Array.isArray(poly[5]) ? COLPOLY_SNORMAL(poly[5][1]) : poly[5],
        Array.isArray(poly[6]) ? COLPOLY_SNORMAL(poly[6][1]) : poly[6],
      ]

      const norm = [
        u16ToS16(normRaw[0]) / 0x7FFF, 
        u16ToS16(normRaw[1]) / 0x7FFF, 
        u16ToS16(normRaw[2]) / 0x7FFF,
      ];

      collision.normals.push(
        norm[0], norm[1], norm[2],
        norm[0], norm[1], norm[2],
        norm[0], norm[1], norm[2],
      );
    }
  }

  if(waterVar) {
    for(const waterData of waterVar.data) {
      collision.waterBoxes.push({
        pos: waterData.slice(0,3),
        sizeX: waterData[3],
        sizeZ: waterData[4],
        flags: waterData[5],
      });
    }
  }
}
