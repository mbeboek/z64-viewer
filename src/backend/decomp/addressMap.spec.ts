import FileAPI from "../fileapi/fileAPI";
import { readAddressMap } from "./addressMap";

let fileContent = "";

FileAPI.setAPI({
  type: FileAPI.APIType.NODE,
  readFileBuffer: async function (path: string): Promise<ArrayBuffer> {
    throw new Error("Tried to read unknown file: " + path);
  },
  readFileText: async function (path: string): Promise<string> {
    switch(path) {
      case 'build/z64.map': return fileContent;
      default: throw new Error("Tried to read unknown file: " + path); break;
    }
  },
  writeFileText: async function(){},
  openDir: async function(){},
  scanDir: async function() {
    return {files: ["blah.mapp", "z64.map"], dirs: []};
  },
});

describe('Decomp - AddressMap', () => 
{
  test("Empty File", async () => {
    fileContent = "";
    const map = await readAddressMap();
    expect(map).toEqual({
      _COUNT: 0
    });
  });

  test("Exmpale", async () => {
    fileContent = `
.reginfo       0x0000000000000000       0x18 build/src/makerom/some_header.o
.reginfo       0x0000000000000000       0x18 build/src/boot/some_obj.o

LOAD build/src/lib/test.o
LOAD build/src/lib/bla.o

build/src/lib/test.o(.rodata.cst4)
                0x0000000080012340                . = ALIGN (0x10)

build/src/overlays/actors/ovl_Obj_Example/z_obj_example.o(.text)
.text          0x0000000080b94ca0      0xb90 build/src/overlays/actors/ovl_Obj_Example/z_obj_example.o
               0x0000000080ABCD00                ObjExample_Init
               0x0000000080ABCF00                ObjExample_Draw

/DISCARD/
*(*)
OUTPUT(a_cool_game.elf elf32-tradbigmips)
`;

    const map = await readAddressMap();
    expect(map).toEqual({
      ObjExample_Init: 0x80ABCD00,
      ObjExample_Draw: 0x80ABCF00,
      _COUNT: 2
    });
  });
});