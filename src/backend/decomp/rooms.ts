/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { ActorStruct } from "./actor";
import { DataObject, parseFileVars, parseValue } from "./c/parser";
import { ActorTable, parseActorTableCached } from "./actorTable";
import FileAPI from "../fileapi/fileAPI";
import { ObjectTable, parseObjectTableCached } from "./objectTable";
import { SceneDataHeader, parseAltHeaders, parseHeaderVars } from "./scenes";
import { readAddressMapCached } from "./addressMap";
import { Vector3 } from "../z64/types";
import { convertRotMM, DEG_TO_BINANG } from "./c/utils";
import { Game } from "../context";

export interface RoomData {
  mainHeader: SceneDataHeader,
  altHeaders: Array<SceneDataHeader|undefined>,
  actors: Record<string, ActorStruct[]>;
  objList: Record<string, number[]>;
  mesh: {
    opaque: number[];
    transparent: number[];
  };
}

const MESH_TYPES = ["RoomShapeDListsEntry",  "PolygonDlist", "PolygonDlist2", "RoomShapeCullableEntry"];
export const MM_ROT_MASK_X = 0x4000;
export const MM_ROT_MASK_Y = 0x8000;
export const MM_ROT_MASK_Z = 0x2000;

export function sortRoomFiles(files: string[]) {
  return files.sort((a,b) => 
    parseInt((a.match(/[0-9]+\.c/) || [""])[0]) -
    parseInt((b.match(/[0-9]+\.c/) || [""])[0])
  );
}

/**
 * Returns a list of all .c room files.
 * @param sceneDir 
 * @returns 
 */
export async function scanRooms(sceneDir: string) 
{
  if(!sceneDir || !sceneDir.length) {
    return [];
  }

  const scanRes = await FileAPI.scanDir(sceneDir); 
  return sortRoomFiles(
    scanRes.files.filter(f => f.match(/_room_[0-9]+\.c$/))
  );  
};

function parseActorListOOT(vars: DataObject[], actorTable: ActorTable): Record<string, ActorStruct[]>
{
  const res: Record<string, ActorStruct[]> = {};

  const actorVars = vars.filter(v => v.type === "ActorEntry" && Array.isArray(v.data));
  for(const actorVar of actorVars) 
  {
    const actors = [] as ActorStruct[];
    for(const entry of actorVar.data as any) 
    {
      let rot: Vector3;

      // Fast64: uses {DEG_TO_BINANG(1.000), ...}
      if(Array.isArray(entry[2][0]) && entry[2][0][0] === "DEG_TO_BINANG") {
        rot = [
          DEG_TO_BINANG(entry[2][0][1]),
          DEG_TO_BINANG(entry[2][1][1]),
          DEG_TO_BINANG(entry[2][2][1]),
        ];
      } else {
        rot = [
          parseInt(entry[2][0]) || 0,
          parseInt(entry[2][1]) || 0,
          parseInt(entry[2][2]) || 0,
        ];
      }
      
      actors.push({
        id: (actorTable.enum[entry[0]] || 0),
        pos: [
          parseInt(entry[1][0]),
          parseInt(entry[1][1]),
          parseInt(entry[1][2]),
        ],
        rot,
        params: parseInt(entry[3]),
        spawnTime: 0,
        cutsceneIndex: 0,
        rotMask: 0,
      });
    }

    res[actorVar.name] = actors;
  }

  return res;
}

function parseActorListMM(vars: DataObject[], actorTable: ActorTable): Record<string, ActorStruct[]>
{
  const res: Record<string, ActorStruct[]> = {};

  const actorVars = vars.filter(v => v.type === "ActorEntry" && Array.isArray(v.data));
  for(const actorVar of actorVars) 
  {
    const actors = [] as ActorStruct[];
    for(const entry of actorVar.data as any) 
    {
      let rot: Vector3;

      const nameParts = entry[0].split("|");
      const name = nameParts[0];
      const rotMask = parseValue(nameParts[1] || "") || 0;

      const rotRaw = entry[2];
      const cutsceneIndex = rotRaw[1][2] & 0x7F;
      const spawnTime = ((rotRaw[0][2] & 7) << 7) | (rotRaw[2][2] & 0x7F);

      rot = [
        convertRotMM(rotRaw[0][1], !!(rotMask & MM_ROT_MASK_X)),
        convertRotMM(rotRaw[1][1], !!(rotMask & MM_ROT_MASK_Y)),
        convertRotMM(rotRaw[2][1], !!(rotMask & MM_ROT_MASK_Z)),
      ];

      actors.push({
        id: (actorTable.enum[name] || 0),
        pos: [
          parseInt(entry[1][0]),
          parseInt(entry[1][1]),
          parseInt(entry[1][2]),
        ],
        rot,
        params: parseInt(entry[3]),
        spawnTime, cutsceneIndex,
        rotMask: rotMask
      });
    }

    res[actorVar.name] = actors;
  }

  return res;
}

export function parseRoom(src: string, roomPrefix: string, actorTable: ActorTable, objectTable: ObjectTable, addressMap: Record<string, number>, game: Game): RoomData
{
  const vars = parseFileVars(src, roomPrefix);
  const headerVars = parseHeaderVars(vars);

  const resolveAddress = (name: string) => {
    return (addressMap[name] || 0) & 0x00FF_FFFF;
  };

  const data: RoomData = {
    mainHeader: headerVars[0],
    altHeaders: [],
    actors: {},
    objList: {},
    mesh: {
      opaque: [],
      transparent: []
    }
  };

  // Check for alternate headers
  if(data.mainHeader) {
    data.altHeaders = parseAltHeaders(data.mainHeader, headerVars, vars);
  }
  
  // Read Object-List
  const objListVars = vars.filter(v => 
    v.name.toLowerCase().includes("objectlist")
    && Array.isArray(v.data)
  );

  for(const objList of objListVars) {
    data.objList[objList.name] = objList.data.map((x: string) => objectTable.enum[x] || 0);
  }

  // Read Mesh
  const meshVars = vars.filter(v => 
    MESH_TYPES.includes(v.type)
    && Array.isArray(v.data)
  );
  for(const meshVar of meshVars)
  {
    const dpls = meshVar.data.map((v: any[]) => v.slice(-2));

    // Ignore pre-rendered for now (is a flat array)
    if(!dpls.length || !Array.isArray(dpls[0]))continue;

    for(const dpl of dpls) {
      if(dpl[0])data.mesh.opaque.push(resolveAddress(dpl[0]));
      if(dpl[1])data.mesh.transparent.push(resolveAddress(dpl[1]));
    }
  }

  // Read Actors
  if(game === Game.OOT) {
    data.actors = parseActorListOOT(vars, actorTable);
  } else {
    data.actors = parseActorListMM(vars, actorTable);
  }

  // might happen on old vanilla test-maps, fake header
  if(!data.mainHeader) {
    console.warn("No Room-Header found!, using dummy header");
    //..fake header
    data.mainHeader = {
      name: "dummy_header",
      map: {}, indices:{}
    };
  }

  return data;
}

export async function parseRoomFile(basePath: string, file: string, game: Game): Promise<RoomData>
{
  const [actorTable, objectTable, addressMap] = await Promise.all([
    parseActorTableCached(),
    parseObjectTableCached(),
    readAddressMapCached(),
  ]);

  const fullPath = basePath + "/" + file;
  const roomName = file.substring(0, file.length-2); // remove ".c" at the end
  const src = await FileAPI.readFileText(fullPath);
  return parseRoom(src, roomName, actorTable, objectTable, addressMap, game);
}
