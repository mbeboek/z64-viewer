/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
import { DEG_TO_BINANG, BINANG_TO_DEG } from './utils';

describe('C - Utils', () => 
{
  test('Binary-Angle - Angle Limits', () => 
  {
    expect(DEG_TO_BINANG(0)).toEqual(0);
    expect(DEG_TO_BINANG(180)).toEqual(32768);
    expect(DEG_TO_BINANG(360)).toEqual(65535);
  });

  test('Binary-Angle - Angle RW', () => 
  {
    // Test all possible values and see if converting back and forth gives the same results
    for(let angleExpected=0; angleExpected<=360; ++angleExpected) 
    {
      const valDeg = DEG_TO_BINANG(angleExpected);
      const angleActual = BINANG_TO_DEG(valDeg);
      expect(angleActual).toEqual(angleExpected);
    }
  });
});