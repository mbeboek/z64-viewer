/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
import { parseFileVars } from './parser';
import { patchFileVar } from './writer';

describe('C - Read/Write', () => 
{
  test('Stable R/W/R/W', () => 
  {
    const srcIn = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_room_0Commands[] = {
    SCENE_CMD_ECHO_SETTINGS(0),
    SCENE_CMD_END(),
};

s16 someroom_room_0ObjectList_000040[] = {
    OBJECT_ABC,
    OBJECT_DEF,
};

ActorEntry someroom_room_0ActorEntry_00005C[] = {
    { ACTOR_EN_ABC,        {   111,      0,   -10 }, {      0, 0X4000,      0 }, 0xABCD },
    { ACTOR_EN_DEF,        {   222,      5,   -20 }, {      0, 0X4234,      0 }, 0x1234 },
    { ACTOR_EN_ABC,        {   333,      0,   -30 }, {      0, 0XABCD,      0 }, 0x5678 },
};

u8 someroom_room_0_possiblePadding_000FFF = 42;

Gfx someroom_room_0DL_000A50[] = {
  gsDPSomeCoolCmd(),
  gsDPSomeCoolCmd(11),
};

u64 someroom_room_0Tex_000E00[] = {
  #include "assets/scenes/someroom/someroom_room_0Tex_000E00.rgba16.inc.c"
};`;

    // First parse the data like it would be in decomp
    const fileVars0 = parseFileVars(srcIn, "someroom_room_0");
    const actorVar0 = fileVars0.find(v => v.name.includes("ActorEntry"));

    expect(actorVar0).toBeDefined(); 
    if(!actorVar0)throw new Error(); // make TS happy
    expect(actorVar0?.type).toBe("ActorEntry");

    // Write out the same data again...
    const srcPatch0 = patchFileVar(srcIn, actorVar0) || "";
    expect(srcPatch0.length).toBeGreaterThan(0);

    // ...and parse it again...
    const fileVars1 = parseFileVars(srcPatch0, "someroom_room_0");
    const actorVar1 = fileVars0.find(v => v.name.includes("ActorEntry"));

    expect(fileVars1).toBeDefined(); 
    if(!actorVar1)throw new Error(); // make TS happy
    expect(actorVar1?.type).toBe("ActorEntry");

    // ...it should still match the data we just wrote
    expect(fileVars1).toEqual(fileVars0);

    // now write it once more, just to see the actual output text.
    // after the first write it might have changed (different formating)
    // but after writing it twice it should be stable.
    const srcPatch1 = patchFileVar(srcIn, actorVar1) || "";
    expect(srcPatch0).toBe(srcPatch1);
  });
});