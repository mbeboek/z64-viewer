import { DataObject } from "./parser";

/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
const TAB = "    ";

export enum FormatHint
{
  DEFAULT, HEX,
  FLOAT
}

export function writeValue(value: any, hint: FormatHint = FormatHint.DEFAULT): string
{
  if(typeof(value) === "string")return value;

  switch(hint) {
    case FormatHint.HEX: 
      if(value === 0)return "0";
      return (value < 0 ? "-" : "") 
        + "0x" 
        + Math.abs(parseInt(value)).toString(16).toUpperCase();

    case FormatHint.FLOAT: {
      if(isNaN(value))return "NAN";
      if(!isFinite(value))return value > 0 ? "INFINITY" : "-INFINITY";

      const str = parseFloat(value).toString();
      return str + (str.includes(".") ? "f" : ".f");
    }

    default: return value.toString();
  }
}

export function writeArray(value: any[], align = false): string
{
  const hint = (value as any).hint || FormatHint.DEFAULT;
  const nl = (align ? "\n" : "");
  const maxLineLen = align ? 10 : 50;

  return "{" + nl 
    + value.map(v => Array.isArray(v) ? writeArray(v) : writeValue(v, hint))
      .map(v => align ? (TAB+v) : v)
      .reduce((a, b) => 
        a 
        + (a.length ? (a.length > maxLineLen ? ",\n" : ", ") : "") 
        + b
      , "")
  + nl + "}";
}

export function patchFileVar(src: string, dataObj: DataObject, align = false): string|false
{
  const objName = dataObj.name.trim();
  if(!objName.length) {
    console.warn("Tried to patch var with empty name!");
    return false;
  }

  const isArray = Array.isArray(dataObj.data);
  const fullSourceName = objName + (isArray ? "[" : ""); // what might be in source

  let varIdx = src.indexOf(fullSourceName);
  if(varIdx < 0) {
    console.warn("Could not find var: " + objName);
    return false;
  }

  if(src[varIdx-1] !== " ") {
    return false; // avoid partial matches (start)
  }

  const arrStartIdx = varIdx + fullSourceName.length;
  const arrEndIdx = isArray ? (src.indexOf("]", arrStartIdx)+1) : arrStartIdx;

  if((arrEndIdx - varIdx) > 100) {
    console.warn("Error detecting end of array");
    return false;
  }
  varIdx = arrEndIdx;

  if(src[varIdx] !== " " && src[varIdx] !== "=") {
    return false; // avoid partial matches (end)
  }

  const dataIdxStart = src.indexOf("=", varIdx);
  const dataIdxEnd = src.indexOf(";", dataIdxStart);
  if(dataIdxStart < 0 || dataIdxEnd < 0) {
    console.warn("Could not find start/end of value: " + objName);
    return false;
  }

  const newSrc = src.slice(0, dataIdxStart) + "= "
    + (isArray ? writeArray(dataObj.data, align) : writeValue(dataObj.data))
    + src.slice(dataIdxEnd);

  if(isArray) {
    // remove array size if it was set
    return newSrc.substring(0, arrStartIdx) + newSrc.substring(arrEndIdx-1);
  } else {
    return newSrc;
  }
}

/**
 * Patches macro functions in source files
 * @param src source file
 * @param containerName name of the variable which contains the macro (should be something unique in the file)
 * @param macroName name of the macro itself (is not unique)
 * @param args argument inside the macro call
 * @returns patched source on success, false on any error
 */
export function patchFileMacro(src: string, containerName: string, macroName: string, args: Array<number|string>): string|false
{
  const containerIdx = src.indexOf(containerName + "["); // avoid maatching references, only take arrays
  if(containerIdx === -1)return false;

  const macroIdx = src.indexOf(macroName, containerIdx);
  if(macroIdx === -1)return false;

  const macroEndIdx = src.indexOf(")", macroIdx);
  if(macroEndIdx === -1)return false;

  const newMacro = macroName + "(" + args.join(", ") + ")";

  const srcStart = src.substring(0, macroIdx);
  const srcEnd = src.substring(macroEndIdx+1);

  return srcStart + newMacro + srcEnd;
}

/**
 * Removes a variable from a source file
 * @param src source file
 * @param varName complete var name to delete (excl. '[]' or type)
 * @returns patched source on success, false on any error
 */
export function removeVar(src: string, varName: string): string|false
{
  let varIdx = src.indexOf(" " + varName + " ");
  if(varIdx < 0)varIdx = src.indexOf(" " + varName + "=");
  if(varIdx < 0)varIdx = src.indexOf(" " + varName + "[");
  if(varIdx < 0)return false;

  let varIdxStart = src.lastIndexOf("\n", varIdx);
  if(varIdxStart < 0)return false;
  
  let varIdxEnd = src.indexOf(";", varIdx);
  if(varIdxEnd < 0)return false;

  if(src[varIdxStart-1] === "\n")--varIdxStart; // avoid double line breaks

  return src.substring(0, varIdxStart) + src.substring(varIdxEnd+1);
}

/**
 * Inserts a variable after a given one, or at the end if not found
 * @param src source file
 * @param varName reference var-name, data is inserted after
 * @param data data to insert, no checks or conversions are done
 * @returns patched source, nalways succeedes
 */
  export function injectVar(src: string, varName: string, data: string): string
  {
    const varIdx = src.indexOf(" " + varName);   
    const varIdxEnd = src.indexOf(";", varIdx);
    if(varIdx >= 0 && varIdxEnd >= 0) {
      return src.substring(0, varIdxEnd+1) 
        + "\n" + data 
        + src.substring(varIdxEnd+1);
    }

    const needsNewLine = (src[src.length-1] !== "\n");
    return src + (needsNewLine ? "\n" : "") + data + "\n";
  }