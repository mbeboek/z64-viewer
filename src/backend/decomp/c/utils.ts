// Helper from decomp to convert degrees to s16 angles
export function DEG_TO_BINANG(degrees: number) {
  const res = (degrees) * (0x8000 / 180.0);
  return Math.min(Math.round(res), 0xFFFF);
}

export function BINANG_TO_DEG(angle: number) {
  return Math.round(angle / (0x8000 / 180.0));
}

export function COLPOLY_SNORMAL(x: number) {
  return Math.floor(Math.min(x * 32767.0));
}

export function convertRotMM(angleRaw: number, isDegree: boolean) {
  if(isDegree) {
    return angleRaw > 180 ? (angleRaw - 360) : angleRaw;
  }
  return DEG_TO_BINANG(angleRaw);
}