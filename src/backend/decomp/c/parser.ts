/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
const REGEX_INT = /^-?[0-9.]+[fFuU]?$/;

const knownDataTypes = [
  "u8", "s8", "u16", "s16", "u32", "s32", 
  "ActorEntry", "TransitionActorEntry",
  "SceneCmd", "SceneCmd*",
  "LightSettings", "EnvLightSettings",
  "PolygonDlist", "PolygonDlist2", "RoomShapeDListsEntry",
  "RoomShapeCullableEntry", // older decomp version of "PolygonDlist2"
  "CollisionHeader", "Vec3s", "CollisionPoly", "WaterBox",
  "Path",
];

// data-types where variables don't have a '[]', should be listed here
const complexTypes = new Set(["CollisionHeader"]);

// Contains a variable of an type, even nested arrays
export interface DataObject {
  type: string;
  name: string;
  data: any;
};

export function strip(str: string): string {
  // https://stackoverflow.com/a/59094308
  return str.replace(/\/\*[\s\S]*?\*\/|(?<=[^:])\/\/.*|^\/\/.*/g,'').trim();
}

export function u8ToS8(num: number) {
  return ((num) << 24) >> 24;
}

export function u16ToS16(num: number) {
  return ((num) << 16) >> 16;
}

/**
 * Parses a value (int, macro, string)
 * @param str input e.g. "0x1234"
 * @returns parsed value
 */
export function parseValue(str: string): any
{
  const strLower = str.toLowerCase();

  // Macro-Functions
  const bracePos = strLower.indexOf("(");
  if(bracePos >= 0 && strLower.endsWith(")")) {
    if(bracePos === 0) {
      // just a parentheses, not a macro
      return parseValue(str.substring(1, str.length-1));
    }
    const macroName = str.substring(0, bracePos);
    const macroContent = str.substring(bracePos+1, str.length-1);
    return [macroName, ...parseArray("{"+macroContent+"}")];
  }

  // Hex-Integer
  if(strLower.startsWith("0x")) {
    return parseInt(strLower.substring(2), 16);
  }

  // Binary-Integer
  if(strLower.startsWith("0b")) {
    return parseInt(strLower.substring(2), 2);
  }

  // Decimal-Integer
  if(REGEX_INT.test(strLower)) {
    return strLower.includes(".")
      ? parseFloat(strLower)
      : parseInt(strLower, 10);
  }

  if(str === "NULL")return 0;

  // Default: read as is (strings, constants, macro defines)
  return str;
}

/**
 * Parses a (nested) C-Array with values
 * @param str 
 * @returns 
 */
export function parseArray(str: string)
{
  str = str.replace(/[ \n]/g, "");
  let pos = -1;

  const parseSubArray = () => 
  {
    ++pos;
    const data: any[] = [];

    let currentVal: string|any[] = "";
    let braceLevel = 0;
    let inString = false;

    const emitValue = () => {
      if(currentVal) {
        data.push(Array.isArray(currentVal) ? currentVal : parseValue(currentVal));
      }
      currentVal = "";
      return data;
    }

    for(; pos<str.length; ++pos) 
    {
      const c = str[pos];

      if(c === '"') {
        inString = !inString;
      }else if(c === '(' || c === ')') {
        braceLevel += c === '(' ? 1 : -1;
      }
      else if(!inString && braceLevel <= 0) 
      {
        switch(c) {
          case '}': return emitValue();
          case '{': currentVal = parseSubArray(); continue;
          case ',': emitValue(); continue;
        }
      }

      currentVal += c;
    }

    return emitValue();
  };

  return parseSubArray()[0];
}

/**
 * Parses/Extracts variable definitions and their values.
 * @param str source code
 * @param varPrefix optional prefix to filter vars
 * @returns array ob objects
 */
export function parseFileVars(str: string, varPrefix: string = ""): DataObject[]
{
  str = strip(str);
  const lines = str.split("\n");

  let currObj: DataObject = {
    type: "", name: "", data: []
  };
  let entries: DataObject[] = [];
  let pos = 0;

  for(let lineRaw of lines) 
  {
    pos += lineRaw.length + 1;
    const line = lineRaw.trim();

    if(line.startsWith("#") || line.startsWith("//")) { 
      continue;
    }

    // line contains our room name (might be a var or some reference to it)
    if(line.includes(varPrefix)) 
    {
      // only parse definitions that use the room name
      const tokens = line.split(" ").map(x => x.trim());
      if(tokens.length > 1 && knownDataTypes.includes(tokens[0])) 
      {
        let name = tokens[1];
        const arrayStart = name.indexOf("[");
        const isArray = arrayStart >= 0;

        if(isArray) {
          name = name.substring(0, arrayStart);
        }

        currObj = {type: tokens[0], name, data: []};
        entries.push(currObj);

        // extract data string
        const dataStartPos = pos - lineRaw.length + lineRaw.lastIndexOf("=");
        const dataEndPos = str.indexOf(";", dataStartPos);
        let dataStr = str.substring(dataStartPos, dataEndPos).trim();

        if(isArray || complexTypes.has(currObj.type)) {
          currObj.data = parseArray(dataStr);
        } else {
          currObj.data = parseValue(dataStr);
        }
      }
    }
  }
  return entries;
}

export function parseFileTypedefEnum(str: string, enumName: string): {
  list: string[],
  enum: Record<string, number>
}
{
  const res = {
    list: [] as string[],
    enum: {} as Record<string, number>
  };

  str = strip(str).replaceAll(/[ \n\t]+/g, "");

  const idxEnumName = str.indexOf("}" + enumName + ";");
  if(idxEnumName === -1)return res;

  const ENUM_START = "typedefenum{";
  const idxEnumStart = str.lastIndexOf(ENUM_START, idxEnumName);
  if(idxEnumStart === -1)return res;

  const idxEnumEnd = str.lastIndexOf("}", idxEnumName);
  if(idxEnumEnd === -1)return res;

  const enumBody = str.substring(idxEnumStart + ENUM_START.length, idxEnumEnd);

  res.list = enumBody.split(",").filter(name => name);

  for(let i=0; i<res.list.length; ++i) {
    res.enum[res.list[i]] = i;
  }

  return res;
}