import * as C from './parser';

describe('C - Parser', () => 
{
  describe('File - Vars', () => 
  {
    test("Empty File", () => {
      expect(C.parseFileVars(``)).toEqual([]);
      expect(C.parseFileVars(`  `)).toEqual([]);
      expect(C.parseFileVars(` 
       `)).toEqual([]);
       expect(C.parseFileVars(` 
        //  some commment
        /*
        s8 x = 42;
        */
       `)).toEqual([]);
    });

    test("Scalar", () => 
    {
      expect(C.parseFileVars(`u8 some_var = 42;`)).toEqual([
        {
          name: "some_var",
          data: 42,
          type: 'u8'
        }
      ]);

      expect(C.parseFileVars(`
        u8 some_var = 42;
        u32 some_var2 = 0x50u;
      `)).toEqual([
        {
          name: "some_var",
          data: 42,
          type: 'u8'
        },
        {
          name: "some_var2",
          data: 0x50,
          type: 'u32'
        }
      ]);
    });

    test("Array", () => 
    {
      expect(C.parseFileVars(`u8 some_var[] = {1,2,3};`)).toEqual([
        {
          name: "some_var",
          data: [1,2,3],
          type: 'u8'
        }
      ]);

      expect(C.parseFileVars(`
        u8 some_var[] = {1,2,3};
        ActorEntry some_var2[] = {1,2, {3,4, "test"}};
      `)).toEqual([
        {
          name: "some_var",
          data: [1,2,3],
          type: 'u8'
        },
        {
          name: "some_var2",
          data: [1,2, [3,4, `"test"`]],
          type: 'ActorEntry'
        }
      ]);

      expect(C.parseFileVars(`u8 some_special_var[WITH_LENGTH] = {1,2,3};`)).toEqual([
        {
          name: "some_special_var",
          data: [1,2,3],
          type: 'u8'
        }
      ]);

      expect(C.parseFileVars(`u8 some_special_var[456] = {1,2,3};`)).toEqual([
        {
          name: "some_special_var",
          data: [1,2,3],
          type: 'u8'
        }
      ]);
    });    

    test("Prefix Filter", () => 
    {
      expect(C.parseFileVars(`
        // some comment
        u8 some_var_a[] = {1, 2, 3};
        u8 test_some_var_a[] = {4, 5, 6}; // should be used
        u8 some_var_b[] = {7, 8, 9};
        u8 test_some_var_b[] = {10, 11, 12}; // should be used
      `, "test_")).toEqual([
        {
          name: "test_some_var_a",
          data: [4,5,6],
          type: 'u8'
        },{
          name: "test_some_var_b",
          data: [10, 11, 12],
          type: 'u8'
        }
      ]);
    });
  });

  describe('File - Vars - Examples', () => 
  {
    test("Room - 0", () => 
    {
      const data = C.parseFileVars(`
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_room_0Commands[] = {
    SCENE_CMD_ECHO_SETTINGS(0),
    SCENE_CMD_SPECIAL_FILES(0x00, OBJECT_ABC),
    SCENE_CMD_END(),
};

s16 someroom_room_0ObjectList_000040[] = {
    OBJECT_ABC,
    OBJECT_DEF,
};

ActorEntry someroom_room_0ActorEntry_00005C[] = {
    { ACTOR_EN_ABC,        {   111,      0,   -10 }, {      0, 0X4000,      0 }, 0xABCD },
    { ACTOR_EN_DEF,        {   222,      5,   -20 }, {      0, 0X4234,      0 }, 0x1234 },
    { ACTOR_EN_ABC,        {   333,      0,   -30 }, {      0, 0XABCD,      0 }, 0x5678 },
};

u8 someroom_room_0_possiblePadding_000FFF = 42;

Gfx someroom_room_0DL_000A50[] = {
  gsDPSomeCoolCmd(),
  gsDPSomeCoolCmd(11),
};

u64 someroom_room_0Tex_000E00[] = {
  #include "assets/scenes/someroom/someroom_room_0Tex_000E00.rgba16.inc.c"
};
      `, "someroom");
      
      expect(data).toEqual([
        {
          name: "someroom_room_0Commands",
          data: [
            ["SCENE_CMD_ECHO_SETTINGS", 0], 
            ["SCENE_CMD_SPECIAL_FILES", 0, "OBJECT_ABC"],
            ["SCENE_CMD_END"],
          ],
          type: 'SceneCmd'
        },
        {
          name: "someroom_room_0ObjectList_000040",
          data: ["OBJECT_ABC", "OBJECT_DEF"],
          type: 's16'
        },
        {
          name: "someroom_room_0ActorEntry_00005C",
          data: [
            ["ACTOR_EN_ABC", [111,  0, -10], [0, 0x4000, 0], 0xABCD],
            ["ACTOR_EN_DEF", [222,  5, -20], [0, 0x4234, 0], 0x1234],
            ["ACTOR_EN_ABC", [333,  0, -30], [0, 0xABCD, 0], 0x5678],
          ],
          type: 'ActorEntry'
        },
        {
          name: "someroom_room_0_possiblePadding_000FFF",
          data: 42,
          type: 'u8'
        },
      ]);
    });

  });

  describe('File - Typedef-Enum', () => 
  {
    test("Empty File", () => {
      expect(C.parseFileTypedefEnum(``, "TestEnum")).toEqual({list: [], enum: {}});

      expect(C.parseFileTypedefEnum(`  `, "TestEnum")).toEqual({list: [], enum: {}});

      expect(C.parseFileTypedefEnum(` 
       `, "TestEnum")).toEqual({list: [], enum: {}});
       
       expect(C.parseFileTypedefEnum(` 
        //  some commment
        /*
        s8 x = 42;
        */
       `, "TestEnum")).toEqual({list: [], enum: {}});
    });

    test("Simple Enum", () => {
       expect(C.parseFileTypedefEnum(` 
        //  some commment
        
        typedef enum {
          /* 0 */ VAL_A,
          VAL_B,

          VAL_C,
        } TestEnum;

        some_garbage();

       `, "TestEnum")).toEqual({
          enum: {"VAL_A": 0, "VAL_B": 1, "VAL_C": 2}, 
          list: ["VAL_A", "VAL_B", "VAL_C"]
        });
    });

    test("Non-Matching Enum", () => {
      expect(C.parseFileTypedefEnum(` 
       //  some commment
       
       typedef enum {
         /* 0 */ VAL_A,
         VAL_B,

         VAL_C,
       } TestEnum2;

       some_garbage();

      `, "TestEnum")).toEqual({list: [], enum: {}});
   });

    test("Multi Enum", () => {
      expect(C.parseFileTypedefEnum(` 
       //  some commment
       
       typedef enum {
        ENTRY_A, ENTRY_B
       } OtherEnum

       typedef enum {
         /* 0 */ VAL_A,
         VAL_B,

         VAL_C, VAL_D
       }  TestEnum ;

       typedef enum {
         NAME_A, NAME_B
       } AndOneMoreEnum;

       some_garbage();

      `, "TestEnum")).toEqual({
        enum: {"VAL_A": 0, "VAL_B": 1, "VAL_C": 2, "VAL_D": 3}, 
        list: ["VAL_A", "VAL_B", "VAL_C", "VAL_D"]
      });
   });
  });
});
