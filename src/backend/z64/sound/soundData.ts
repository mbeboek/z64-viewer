/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { soundDataBGM as soundDataBGMOOT } from "./oot/soundDataBGM";
import { soundDataBGM as soundDataBGMMM } from "./mm/soundDataBGM";

import { soundDataAmbient as soundDataAmbientOOT } from "./oot/soundDataAmbient";
import { soundDataAmbient as soundDataAmbientMM } from "./mm/soundDataAmbient";

import { Context } from "../../context";

export const getGameSoundDataBGM     = () => Context.isMM() ? soundDataBGMMM : soundDataBGMOOT;
export const getGameSoundDataAmbient = () => Context.isMM() ? soundDataAmbientMM : soundDataAmbientOOT;
