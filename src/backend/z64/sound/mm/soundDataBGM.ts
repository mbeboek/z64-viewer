/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

// Taken from: https://wiki.cloudmodding.com/mm/Music_Sequences
export const soundDataBGM = [
  {
    value: "0",
    name: "[Nothing]"
  },
  {
    value: "1",
    name: "[Nothing 1]"
  },
  {
    value: "2",
    name: "Termina Field"
  },
  {
    value: "3",
    name: "Forest Chase"
  },
  {
    value: "4",
    name: "Majora's Theme"
  },
  {
    value: "5",
    name: "The Clock Tower"
  },
  {
    value: "6",
    name: "Stone Tower Temple"
  },
  {
    value: "7",
    name: "Stone Tower Temple Inverted"
  },
  {
    value: "8",
    name: "Missed Event 1"
  },
  {
    value: "9",
    name: "Title"
  },
  {
    value: "10",
    name: "Mask Salesman"
  },
  {
    value: "11",
    name: "Song of Healing"
  },
  {
    value: "12",
    name: "Southern Swamp"
  },
  {
    value: "13",
    name: "Ghost Attack"
  },
  {
    value: "14",
    name: "Mini Game"
  },
  {
    value: "15",
    name: "Sharp's Curse"
  },
  {
    value: "16",
    name: "Great Bay Coast"
  },
  {
    value: "17",
    name: "Ikana Valley"
  },
  {
    value: "18",
    name: "Court of the Deku King"
  },
  {
    value: "19",
    name: "Mountain Village"
  },
  {
    value: "20",
    name: "Pirates' Fortress"
  },
  {
    value: "21",
    name: "Clock Town Day 1"
  },
  {
    value: "22",
    name: "Clock Town Day 2"
  },
  {
    value: "23",
    name: "Clock Town Day 3"
  },
  {
    value: "24",
    name: "[File Select]"
  },
  {
    value: "25",
    name: "Event Clear"
  },
  {
    value: "26",
    name: "Battle"
  },
  {
    value: "27",
    name: "Boss Battle"
  },
  {
    value: "28",
    name: "Woodfall Temple"
  },
  {
    value: "29",
    name: "Clock Town Day 1"
  },
  {
    value: "30",
    name: "Forest Ambush"
  },
  {
    value: "31",
    name: "House"
  },
  {
    value: "32",
    name: "Game Over"
  },
  {
    value: "33",
    name: "Boss Clear"
  },
  {
    value: "34",
    name: "Item Catch"
  },
  {
    value: "35",
    name: "Clock Town Day 2"
  },
  {
    value: "36",
    name: "Complete a Heart Piece"
  },
  {
    value: "37",
    name: "Playing Minigame"
  },
  {
    value: "38",
    name: "Goron Race"
  },
  {
    value: "39",
    name: "Music Box House"
  },
  {
    value: "40",
    name: "Fairy's Fountain"
  },
  {
    value: "41",
    name: "Zelda's Lullaby"
  },
  {
    value: "42",
    name: "Rosa Sisters' Dance"
  },
  {
    value: "43",
    name: "Open Chest"
  },
  {
    value: "44",
    name: "Marine Research Laboratory"
  },
  {
    value: "45",
    name: "The Four Giants"
  },
  {
    value: "46",
    name: "Guru-Guru's Song"
  },
  {
    value: "47",
    name: "Romani Ranch"
  },
  {
    value: "48",
    name: "Goron Village"
  },
  {
    value: "49",
    name: "Mayor Dotour"
  },
  {
    value: "50",
    name: "Ocarina Epona's Song"
  },
  {
    value: "51",
    name: "Ocarina Sun's Song"
  },
  {
    value: "52",
    name: "Ocarina Song of Time"
  },
  {
    value: "53",
    name: "Ocarina Song of Storms"
  },
  {
    value: "54",
    name: "Zora Hall"
  },
  {
    value: "55",
    name: "A New Mask"
  },
  {
    value: "56",
    name: "Mini Boss"
  },
  {
    value: "57",
    name: "Small Item Catch"
  },
  {
    value: "58",
    name: "Astral Observatory"
  },
  {
    value: "59",
    name: "Clock Town Cavern"
  },
  {
    value: "60",
    name: "Milk Bar Latte"
  },
  {
    value: "61",
    name: "Meet Zelda (OoT)"
  },
  {
    value: "62",
    name: "Woods of Mystery"
  },
  {
    value: "63",
    name: "Goron Race Goal"
  },
  {
    value: "64",
    name: "Gorman Race"
  },
  {
    value: "65",
    name: "Race Finish"
  },
  {
    value: "66",
    name: "Gorman Bros."
  },
  {
    value: "67",
    name: "Kotake's Potion Shop"
  },
  {
    value: "68",
    name: "Store"
  },
  {
    value: "69",
    name: "Gaebora's Theme"
  },
  {
    value: "70",
    name: "Target Practice"
  },
  {
    value: "71",
    name: "Ocarina Song of Soaring"
  },
  {
    value: "72",
    name: "Ocarina Song of Healing"
  },
  {
    value: "73",
    name: "Inverted Song of Time"
  },
  {
    value: "74",
    name: "Song of Double Time"
  },
  {
    value: "75",
    name: "Sonata of Awakening"
  },
  {
    value: "76",
    name: "Goron Lullaby"
  },
  {
    value: "77",
    name: "New Wave Bossa Nova"
  },
  {
    value: "78",
    name: "Elegy of Emptiness"
  },
  {
    value: "79",
    name: "Oath to Order"
  },
  {
    value: "80",
    name: "Sword Training"
  },
  {
    value: "81",
    name: "Ocarina Goron Lullaby Intro"
  },
  {
    value: "82",
    name: "New Song"
  },
  {
    value: "83",
    name: "Bremen March"
  },
  {
    value: "84",
    name: "Ballad of the Wind Fish"
  },
  {
    value: "85",
    name: "Song of Soaring"
  },
  {
    value: "86",
    name: "Milk Bar Latte"
  },
  {
    value: "87",
    name: "Final Hours"
  },
  {
    value: "88",
    name: "Mikau's Tale"
  },
  {
    value: "89",
    name: "[Unused 89]"
  },
  {
    value: "90",
    name: "Don Gero's Song"
  },
  {
    value: "91",
    name: "Ocarina Sonata of Awakening"
  },
  {
    value: "92",
    name: "Ocarina Goron Lullaby"
  },
  {
    value: "93",
    name: "Ocarina New Wave Bossa Nova"
  },
  {
    value: "94",
    name: "Ocarina Elegy of Emptiness"
  },
  {
    value: "95",
    name: "Ocarina Oath to Order"
  },
  {
    value: "96",
    name: "Majora's Lair"
  },
  {
    value: "97",
    name: "[Unused 97]"
  },
  {
    value: "98",
    name: "Bass and Guitar Session"
  },
  {
    value: "99",
    name: "Piano Solo"
  },
  {
    value: "100",
    name: "The Indigo-Go's Rehearsal"
  },
  {
    value: "101",
    name: "Snowhead Temple"
  },
  {
    value: "102",
    name: "Great Bay Temple"
  },
  {
    value: "103",
    name: "[Unused 103]"
  },
  {
    value: "104",
    name: "[Unused 104]"
  },
  {
    value: "105",
    name: "Majora's Wrath"
  },
  {
    value: "106",
    name: "Majora's Incarnation"
  },
  {
    value: "107",
    name: "Majora's Mask Battle"
  },
  {
    value: "108",
    name: "Bass Practice"
  },
  {
    value: "109",
    name: "Drums Practice"
  },
  {
    value: "110",
    name: "Piano Practice"
  },
  {
    value: "111",
    name: "Ikana Castle"
  },
  {
    value: "112",
    name: "Calling the Four Giants"
  },
  {
    value: "113",
    name: "Kamaro's Dance"
  },
  {
    value: "114",
    name: "Cremia's Carriage"
  },
  {
    value: "115",
    name: "Keaton"
  },
  {
    value: "116",
    name: "The End/Credits I"
  },
  {
    value: "117",
    name: "Forest Ambush (?)"
  },
  {
    value: "118",
    name: "Title Screen"
  },
  {
    value: "119",
    name: "Surfacing of Woodfall"
  },
  {
    value: "120",
    name: "Woodfall Clear"
  },
  {
    value: "121",
    name: "Snowhead Clear"
  },
  {
    value: "122",
    name: "Music Box House"
  },
  {
    value: "123",
    name: "To the Moon"
  },
  {
    value: "124",
    name: "[Unused 124]"
  },
  {
    value: "125",
    name: "Tatl and Tael"
  },
  {
    value: "126",
    name: "Moon's Destruction"
  },
  {
    value: "127",
    name: "The End/Credits II "
  }
]