/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { ScenePath } from "../../decomp/parser/parsePaths";
import { Vector3 } from "../types";
import { v4 as uuid } from 'uuid';
import { vec3Add } from "../../utils/math";

export interface PathRaycaseEntry {
  uuid: string;
  pointIdx: number;
  distance: number;
}

export interface PathRaycaseResult {
  holdingShift: boolean;
  entries: PathRaycaseEntry[];
}

export function getPathIdx(paths: ScenePath[], uuid: string) {
  return paths.findIndex(p => p.uuid === uuid);
}

export function patchPathAdd(paths: ScenePath[], refPos: Vector3) {
  const newPaths = [...paths];
  newPaths.push({
    uuid: uuid(),
    additionalPathIdx: 0,
    customValue: 0,
    points: [
      refPos,
      vec3Add(refPos, [100, 0, 100])
    ]
  });
  return newPaths;
}

export function patchPathPos(paths: ScenePath[], pathIdx: number, pointIdx: number, axis: number, newValue: number) {
  const newPaths = structuredClone(paths);
  if(newPaths[pathIdx] && newPaths[pathIdx].points[pointIdx]) {
    newPaths[pathIdx].points[pointIdx][axis] = newValue;
  }
  return newPaths;
}

export function setPathPosById(paths: ScenePath[], pathUUID: string, pointIdx: number, newPos: Vector3) {
  const pathIdx = getPathIdx(paths, pathUUID);
  if(pathIdx < 0)return paths;
  
  paths[pathIdx].points = [...paths[pathIdx].points];
  paths[pathIdx].points[pointIdx] = newPos;
  return paths;
}

export function patchPathDelPoint(paths: ScenePath[], pathIdx: number, pointIdx: number) {
  if(!paths[pathIdx])return paths;

  const newPaths = structuredClone(paths);
  newPaths[pathIdx].points.splice(pointIdx, 1);
  return newPaths;
}

export function patchPathAddPoint(paths: ScenePath[], pathIdx: number, pointIdx: number, pointInsertIdx: number = -1) {
  if(!paths[pathIdx])return paths;

  if(pointInsertIdx < 0)pointInsertIdx = pointIdx;

  const newPaths = structuredClone(paths);
  const newPoint = structuredClone(newPaths[pathIdx].points[pointIdx] || [0,0,0]);
  newPaths[pathIdx].points.splice(pointInsertIdx+1, 0, newPoint);
  return newPaths;
}
