/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { GpuTexture } from "./texture";

/**
 * An object that represent everything needed to render something.<br>
 * This is something that is generated via the GPU-Microcode interpreter,<br>
 * and then used later by the UI to render it (or generate OpenGL objects from it).
 * 
 *   
  struct VertexGL
  {
    [0x00] Math::Vec3 pos{};

    [0x0C] u32 faceSettings[2]{0};
    [0x14] u32 textureIds[2]{0};

    [0x1C] Math::Vec4 uv{};
    [0x2C] Math::Vec4 uvEnd{};
    [0x3C] Math::Vec3S8 normals;
    u8 padding0{};

    [0x40] RGBA8 colorA1{};
    [0x44] RGBA8 colorB1{};
    [0x48] RGBA8 colorC1{};
    [0x4C] RGBA8 colorD1{};

    [0x50] RGBA8 colorA2{};
    [0x54] RGBA8 colorB2{};
    [0x58] RGBA8 colorC2{};
    [0x5C] RGBA8 colorD2{};
  };*/
 
export const GpuObjectVertexSize = 0x60;

export interface GpuObject {
  buffer: ArrayBuffer,
  view: DataView,
  textures: GpuTexture[], // dynamic count with dynamic size
  currentVertex: number // current vertex, this is useful if the arrays are already sized correctly
};

export const emptyGpuObject = (): GpuObject => {
  const buff = new ArrayBuffer(0);
  return {
    buffer: buff,
    view: new DataView(buff),
    textures: [],
    currentVertex: 0
  };
};
