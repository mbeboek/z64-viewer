/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { v4 as uuid } from 'uuid';

export enum GpuColorFormat {
  RGBA, YUV, PALETTE, RED_ALPHA, RED
};
export const gpuColorFormatNames = [
  'RGBA', 'YUV', 'Palette', 'Red/Alpha', 'Mono'
];

export enum GpuBitSize {
  SIZE_4BIT,
  SIZE_8BIT,
  SIZE_16BIT,
  SIZE_32BIT,
};

export enum GpuTextureWrapping {
  REPEAT,
  MIRROR,
  CLAMP,
}

export type GpuColorRGBA = number;
export type GpuColorRGB = number;

export interface GpuTileInfo {
  uuid: string,
  wrappingU: GpuTextureWrapping,
  wrappingV: GpuTextureWrapping,
  width: number,
  height: number,
  format: GpuColorFormat,
  bitSize: number,
}

export const emptyGpuTileInfo = (): GpuTileInfo => ({
  uuid: uuid(),
  
  wrappingU: GpuTextureWrapping.REPEAT,
  wrappingV: GpuTextureWrapping.REPEAT,
  width: 0,
  height: 0,
  format: GpuColorFormat.RGBA,
  bitSize: 0
});


export interface GpuTexture {
  rgbaBuffer: Uint8Array,
  tile: GpuTileInfo,
  globalIndex: number
};
