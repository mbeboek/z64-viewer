/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { v4 as uuid } from 'uuid';
import { SceneEntry } from '../scenes/sceneList';
import { RomOffset } from '../offsets';
import { Z64Mesh } from '../mesh/mesh';
import { Actor } from '../actors/actorList';
import { ActorType, VRamAddress } from '../types';
import { MemLayout } from '../emulator/memoryLayout';
import { parseRoomFile, RoomData, scanRooms } from 'backend/decomp/rooms';
import { readAddressMapCached } from '../../decomp/addressMap';
import { SceneDataHeader } from '../../decomp/scenes';
import { Context } from '../../context';

export interface RoomEntry {
  uuid: string,
  roomId: number,
  altId: number,
  filePath: string;

  offsetStart: RomOffset,
  offsetEnd: RomOffset,
  ramOffsetStart: VRamAddress,
  mesh?: Z64Mesh,
  actorsVar: string;
  actors: Actor[],
  meshData: {
    opaque: number[];
    transparent: number[];
  };
  
  header: SceneDataHeader,
  altRooms: Array<RoomEntry|undefined>,

  objectDependencies: number[];
}

/**
 * Read all rooms in a scene.
 * This will also load and initialize all actors in it.
 * @param scene scene to load from
 */
export const getRoomsFromScene = async (scene: SceneEntry): Promise<RoomEntry[]> => {
  const [roomFiles, addressMap] = await Promise.all([
    scanRooms(scene.decompData.path),
    readAddressMapCached()
  ]);

  let ramOffset = MemLayout.ROOM_DATA;

  // Preload source (avoid promises in main converter loop)
  const roomSources: {decompRoom: RoomData, roomName: string, filePath: string}[] = [];
  for(let r=0; r<roomFiles.length; ++r) 
  {
    const filePath = roomFiles[r];
    const roomName = filePath.substring(0, filePath.length-2); // remove ".c"
    const decompRoom = await parseRoomFile(scene.decompData.path, filePath, Context.game);
    roomSources[r] = {decompRoom, roomName, filePath};
  }

  // Convert data
  const rooms = [] as RoomEntry[];
  for(let r=0; r<roomSources.length; ++r) 
  {
    const {decompRoom, roomName, filePath} = roomSources[r];
    const z64MapRomStart = "_" + roomName + "SegmentRomStart";
    const z64MapRomEnd = "_" + roomName + "SegmentRomEnd";
    const roomId = r;

    const convertRoom = (decompRoom: RoomData, header: SceneDataHeader, ramOffsetStart: number) => 
    {
      const room: RoomEntry = {
        uuid: uuid(),
        roomId,
        altId: -1,
        filePath,
        offsetStart: addressMap[z64MapRomStart] || 0,
        offsetEnd: addressMap[z64MapRomEnd] || 0,
        ramOffsetStart,
        mesh: undefined, // Biggest room-mesh: 0xE2F60 (spirit-temple)
        meshData: {
          opaque: [],
          transparent: [],
        },
        actorsVar: "",
        actors: [],
        header,
        altRooms: [],
        objectDependencies: [],
      };

      if(header.map.SCENE_CMD_OBJECT_LIST) 
      {
        const objVarName = header.map.SCENE_CMD_OBJECT_LIST[1];
        room.objectDependencies = decompRoom.objList[objVarName] || [];
      }
  
      // convert relative address to segmented room address
      room.meshData.opaque = decompRoom.mesh.opaque.map(x => x | 0x8300_0000);
      room.meshData.transparent = decompRoom.mesh.transparent.map(x => x | 0x8300_0000);
  
      if(header.map.SCENE_CMD_ACTOR_LIST) 
      {
        const actorVarName = header.map.SCENE_CMD_ACTOR_LIST[1];
        const actorList = decompRoom.actors[actorVarName] || [];
    
        room.actorsVar = actorVarName as string;
        room.actors = actorList.map(a => ({
          uuid: uuid(),
          type: ActorType.ROOM,
          id: a.id,
          roomId: room.roomId,
          pos: a.pos,
          rot: a.rot,
          initValue: a.params,
          cutsceneIndex: a.cutsceneIndex,
          spawnTime: a.spawnTime,
          rotMask: a.rotMask,
        }));
      }
   
      return room;
    };

    const room = convertRoom(decompRoom, decompRoom.mainHeader, ramOffset);
    for(const altHeader of decompRoom.altHeaders) {
      if(altHeader) {
        const altRoom = convertRoom(decompRoom, altHeader, ramOffset);
        altRoom.altId = room.altRooms.length;
        room.altRooms.push(altRoom);
      } else {
        room.altRooms.push(undefined);
      }
    }

    ramOffset += (room.offsetEnd - room.offsetStart);
    rooms.push(room);
  }
  return rooms;
};
