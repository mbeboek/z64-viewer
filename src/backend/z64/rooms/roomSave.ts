/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Context } from "../../context";
import { ActorTable, parseActorTableCached } from "../../decomp/actorTable";
import { BINANG_TO_DEG } from "../../decomp/c/utils";
import { FormatHint, patchFileVar, writeValue } from "../../decomp/c/writer";
import { objectTableDecomp } from "../../decomp/objectTable";
import { MM_ROT_MASK_X, MM_ROT_MASK_Y, MM_ROT_MASK_Z } from "../../decomp/rooms";
import FileAPI from "../../fileapi/fileAPI";
import { Actor } from "../actors/actorList";
import { saveSceneHeader } from "../scenes/sceneHeaderSave";
import { SceneEntry } from "../scenes/sceneList";
import { RoomEntry } from "./roomList";

export function encodeRotMM(angleRaw: number, isDegree: boolean) {
  if(isDegree) {
    return angleRaw < 0 ? (angleRaw + 360) : angleRaw;
  }
  return BINANG_TO_DEG(angleRaw);
}

function convertActorMM(actorTable: ActorTable, a: Actor) {
  const timeBitsX = (a.spawnTime >> 7) & 7;
  const timeBitsZ = a.spawnTime & 0x7F;
  const cutscene = a.cutsceneIndex & 0x7F;

  const rotNorm = [
    encodeRotMM(a.rot[0], !!(a.rotMask & MM_ROT_MASK_X)),
    encodeRotMM(a.rot[1], !!(a.rotMask & MM_ROT_MASK_Y)),
    encodeRotMM(a.rot[2], !!(a.rotMask & MM_ROT_MASK_Z)),
  ]

  let actorName = (actorTable.list[a.id]?.name || a.id);
  if(a.rotMask) {
    actorName += " | 0x" + a.rotMask.toString(16);
  }

  const rotData = [
    `SPAWN_ROT_FLAGS(${writeValue(rotNorm[0], FormatHint.HEX)}, ${writeValue(timeBitsX, FormatHint.HEX)})`,
    `SPAWN_ROT_FLAGS(${writeValue(rotNorm[1], FormatHint.HEX)}, ${writeValue(cutscene, FormatHint.HEX)})`,
    `SPAWN_ROT_FLAGS(${writeValue(rotNorm[2], FormatHint.HEX)}, ${writeValue(timeBitsZ, FormatHint.HEX)})`,
  ];

  return [
    actorName,
    a.pos, 
    rotData, 
    writeValue(a.initValue, FormatHint.HEX)
  ];
}

export async function saveRoom(scene: SceneEntry, room: RoomEntry)
{
  const fullRoomPath = scene.decompData.path + "/" + room.filePath;
  
  let [src, actorTable] = await Promise.all([
    FileAPI.readFileText(fullRoomPath),
    await parseActorTableCached()
  ]);

  const roomList = [room, ...room.altRooms]
    .filter(r => r && r.actorsVar) as RoomEntry[]; // filter out rooms without actors

  // Collect data to save
  const actorVars = {} as Record<string, any[]>;
  for(const r of roomList) 
  {
    if(actorVars[r.actorsVar])continue;

    if(Context.isMM()) {
      actorVars[r.actorsVar] = r.actors.map(a => convertActorMM(actorTable, a));
    } else {
      actorVars[r.actorsVar] = r.actors.map(a => [
        (actorTable.list[a.id]?.name || a.id),
        a.pos, a.rot, a.initValue
      ]);
    }
  }

  // Patch source (actor list)
  for(const [varName, actors] of Object.entries(actorVars)) {
    const newSrc = patchFileVar(src, {
      type: 'ActorEntry',
      name: varName,
      data: actors
    }, true);

    if(!newSrc)throw new Error("Could not patch source!");
    src = newSrc;
  }

  // Patch source (actor count in header)
  for(const room of roomList) 
  {    
    const header = structuredClone(room.header);  
    const actorHeader = header.map.SCENE_CMD_ACTOR_LIST || [];
    if(actorHeader.length === 0)continue;

    const actorVarName = (actorHeader[1] || "").toString();
    const actorCount = (actorVars[actorVarName] || []).length;

    header.map.SCENE_CMD_ACTOR_LIST = [actorCount, actorVarName];
    
    // Patch Object-List (Header + Data)
    const objHeader = header.map.SCENE_CMD_OBJECT_LIST || [];
    if(objHeader.length > 0) 
    {
      header.map.SCENE_CMD_OBJECT_LIST[0] = room.objectDependencies.length;
      const depsEnums = room.objectDependencies.map(d => objectTableDecomp.list[d]?.name || d);

      const newSrc = patchFileVar(src, {
        type: 's16',
        name: objHeader[1] as string,
        data: depsEnums
      }, true);

      if(!newSrc)throw new Error("Could not patch source (object deps header)!");
      src = newSrc;
    }

    // write headers
    const newSrc = saveSceneHeader(header, src);
    if(!newSrc)throw new Error("Could not patch source! (header)");
    src = newSrc;
  }

  // Save back to room
  await FileAPI.writeFileText(fullRoomPath, src);
}