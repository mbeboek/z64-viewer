/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BinaryFile } from "backend/utils/binaryFile";
import { VRamAddress } from "backend/z64/types";
import { ActorCategory } from "backend/z64/actors/actorFlags";

export function writeActorListEntryArray(file: BinaryFile, actorLists: Record<number, VRamAddress[]>)
{
  for(let i=0; i<ActorCategory.SIZE; ++i) 
  {
    const actors = actorLists[i] || [];
    file.write('s32', actors.length);
    file.write('u32', actors[0] || 0);
    /*
    file.write('u32', actorPtrOffset);
    file.posPushAndSet(actorPtrOffset);
    for(const actorPtr of actors) {
      file.write('u32', actorPtr);
    }
    actorPtrOffset += actors.length * 4;
    file.posPop();*/
  }
}