/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
import { Context } from "../../context";
import { MemLayout } from "./memoryLayout";
import { Z64Emulator } from "./z64Emulator";

export const Code = {
  MOVEWORD: 0xDB,
  MOVEMEM: 0xDC,
};

const getPairOffsets = () => Context.isMM() ? [0x2A8, 0x2B8] : [0x02B8, 0x02C8];

/**
 * Closes the currently created display-list.
 * This should be done by the actor itself, but since thing can and will fail,
 * close it manually here.
 * Note: this does not check if it already was closed, it will just append a return to the end.
 * @param emu 
 */
export const closeDisplayList = (emu: Z64Emulator) => 
{
  const view = emu.getMemoryView();

  // aka: GraphicsContext [polyOpa, polyXlu]
  for(let pairOffset of getPairOffsets()) 
  { 
    const ptrPairStruct = emu.resolveAddress(MemLayout.GFX_CONTEXT + pairOffset + 8);
    const ptrLastCommand = view.getUint32(ptrPairStruct); // get ptr to last entry

    view.setUint32(emu.resolveAddress(ptrLastCommand), 0xDF00_0000); // insert end command
    view.setUint32(ptrPairStruct, ptrLastCommand + 8); // update pointer to last elem.
  }
};

export const addToDisplayLists = (emu: Z64Emulator, data: number[]) => 
{
  // aka: GraphicsContext [polyOpa, polyXlu]
  const view = emu.getMemoryView();
  for(let pairOffset of getPairOffsets()) 
  { 
    const ptrPairStruct = emu.resolveAddress(MemLayout.GFX_CONTEXT + pairOffset + 8);
    const ptrLastCommand = view.getUint32(ptrPairStruct); // get ptr to last entry
    let ptrLastCommandNorm = emu.resolveAddress(ptrLastCommand);

    for(const word of data) {
      view.setUint32(ptrLastCommandNorm, word); 
      ptrLastCommandNorm += 4;
    }
    view.setUint32(ptrPairStruct, ptrLastCommand + (data.length * 4)); // update pointer to last elem.
  }
};

export const resetDisplayList = (emu: Z64Emulator) => 
{
  const file = emu.getMemoryAsFile();
  const pairs = getPairOffsets();

  file.pos(emu.resolveAddress(MemLayout.GFX_CONTEXT) + pairs[0]);
  file.writeStackAllocator(MemLayout.GFX_DL_OPAQUE, MemLayout.GFX_DL_OPQ_END - MemLayout.GFX_DL_OPAQUE);

  file.pos(emu.resolveAddress(MemLayout.GFX_CONTEXT) + pairs[1]);
  file.writeStackAllocator(MemLayout.GFX_DL_TRANSP, MemLayout.GFX_DL_TRP_END - MemLayout.GFX_DL_TRANSP);
};