/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { createMipsiInstance, MipsiInstance, MipsiInstanceExports, RAM_SIZE } from "../../n64/mips/mipsi/mipsi";
import { ActorOverlay, cloneActorOverlay, overlayFileMovePointer } from "../actors/actorOverlayTable";
import { SegmentOffsetType } from "../offsets";
import { Actor } from "../actors/actorList";
import { initActorData, readActorBankIndex, readActorFunction } from "./actorEmuBridge";
import { allocMemory, ObjStatusEntry, setObjectContext, setRegisterEditorData, setupContext } from "./context";
import { ObjectTableEntry } from "../actors/objectTable";
import { addToDisplayLists, closeDisplayList } from "./displayList";
import { VRamAddress } from "../types";
import { readStringFromMemory, readZeroStringFromMemory } from "./printf";
import { BinaryFile, Endian } from "backend/utils/binaryFile";
import { MemLayout } from "./memoryLayout";
import { memcpy } from "backend/utils/memory";
import { SceneLighting } from "../lighting/lighting";
import { writeLight, writeLightContext } from "./structs/light";
import { readAddressMapCached } from "../../decomp/addressMap";
import { BusName, EventBus } from "frontend/helper/eventBus";
import { SceneActorDependency, SceneEntry } from "../scenes/sceneList";
import { Context } from "../../context";
import { MIPS_REG } from "../../n64/mips/mipsi/registerMapping";
import Config from "../../../config";

const OPERATIONS_LIMIT = 150_000; // failsafe to prevent inf. loops
const SAVESTATE_SIZE = (1024 * 1024 * 6) + 1024;//RAM_SIZE;

let instance: MipsiInstance | undefined;

export enum SavestateSlot {
  CLEAN_BOOT, SCENE, ACTOR
};

export interface Z64Savestate {
  uuid: string;
  sceneDep: SceneActorDependency;
  actorObjects: ObjStatusEntry[];
  objectDataOffset: number;
  memory: Uint8Array;
}

function createEmptySavestate() {
  return {
    uuid: "-", 
    sceneDep: SceneActorDependency.NONE, 
    actorObjects: [], 
    objectDataOffset: 0, 
    memory: new Uint8Array(SAVESTATE_SIZE)
  }
}

export class Z64Emulator
{
    private static z64EmuInstance: Z64Emulator | undefined;

    private rom: Uint8Array;
    mipsi!: MipsiInstanceExports;

    private memory: Uint8Array = new Uint8Array();
    public memoryView: DataView = new DataView(this.memory.buffer);

    private savestates: Z64Savestate[] = [
      createEmptySavestate(), createEmptySavestate(), createEmptySavestate()
    ];
    private loadedSavestate: string = "";

    segmentTableAddress;

    private static mocks: Record<number, Function> = {};

    private currentSceneDep: SceneActorDependency = SceneActorDependency.NONE;
    private currentActorObjects: ObjStatusEntry[] = [];
    private currentObjectDataOffset = 0;

    static onGpuEmitTexture: (address: number, width: number, height: number, format: number, bbp: number) => void = () => {};
    addressMap: Record<string, number>;

    constructor(rom: Uint8Array)
    {
        this.rom = rom;
        this.addressMap = {};
        this.segmentTableAddress = 0;
    }

    static getInstance() {
      if(!this.z64EmuInstance) {
        if(Context.rom.byteLength === 0) {
          throw new Error("No ROM loaded!");
        }
        this.z64EmuInstance = new Z64Emulator(Context.rom);
      }
      return this.z64EmuInstance;
    }

    clearSegmentTable() 
    {
      for(let segment=0; segment<=0x0F; ++segment) {
        this.memoryView.setUint32(this.segmentTableAddress + (segment * 4), 0, false);
      }
      this.loadedSavestate = "";
    }

    setSegmentAddress(segment: number, address: number) 
    {
      address = address & 0x0FFFFFFF; // "resolve" the address
      if(segment !== 0) {
        this.memoryView.setUint32(this.segmentTableAddress + (segment * 4), address, false);
      }
      this.loadedSavestate = "";
    }

    resolveAddress(address: number)
    {
      address = address & 0x0FFFFFFF; // "resolve" the address
      
      const segment = (address >>> 24) & 0xFF;
      if(segment !== 0) 
      {
        const base = this.memoryView.getUint32(this.segmentTableAddress + (segment * 4), false);
        return (base + (address & 0x00FFFFFF)) & 0x0FFFFFFF;
      }
      return address;
    }

    getSegmentAddresses(): number[] {
      const res: number[] = [];
      for(let segment=0; segment<0xF; ++segment) {
        res.push(this.memoryView.getUint32(this.segmentTableAddress + (segment * 4), false));
      }
      return res;
    }

    static async loadWasm()
    {
      if(!instance) 
      {
          instance = await createMipsiInstance(Config.EMU_DEBUG_MODE, {
            hook_inject: (pc, arg0, arg1, arg2, arg3) => {
              this.mocks[pc & 0x0FFF_FFFF]?.(instance!.exports, instance!.memory, arg0, arg1, arg2, arg3);
            },
            gpuEmitTexture: (address: number, width: number, height: number, format: number, bpp: number) => {
              this.onGpuEmitTexture(address, width, height, format, bpp);
            }
          });
      } else {
        console.warn("Tried to load WASM twice!, ignore");
      }
    }

    // Loads data from an ROM-address into RAM
    romToRam(romStart: number, romEnd: number, ramStart: number) 
    {
      const ramOffsetStart = this.resolveAddress(ramStart);

      memcpy(
        this.memory, this.rom, romEnd - romStart,
        ramOffsetStart, romStart
      );
      this.loadedSavestate = "";
    };

    async resetAndBoot()
    {
      this.addressMap = await readAddressMapCached();
      this.segmentTableAddress = this.addressMap.gSegments & 0x00FF_FFFF;

      this.loadedSavestate = "";
      this.currentSceneDep = SceneActorDependency.NONE;
      this.currentActorObjects = [];
      this.currentObjectDataOffset = 0;

      if(!instance) {
        if(typeof window !== 'undefined') {
          setTimeout(() => { window.location.reload(); }, 1_000);
        }
        throw Error("Instance not preloaded!");
      }

      if(!this.hasSavestate(SavestateSlot.CLEAN_BOOT, "boot"))
      {
        this.mipsi = instance.exports;
        this.memory = instance.memory;
        this.memoryView = new DataView(this.memory.buffer, 0, RAM_SIZE);

        this.memory.fill(0, 0, RAM_SIZE); // clean RAM (important due to savestates!)
        this.memory.set(this.rom, RAM_SIZE); // insert ROM after RAM

        if(Context.isMM()) 
        {
          // @TODO: check if it safe for OOT now too
          this.mipsi.boot();
          this.mipsi.run(100_000);
        }

        this.mipsi.clearCache();
        
        this.clearSegmentTable();

        this.romToRam(this.addressMap._bootSegmentRomStart, this.addressMap._bootSegmentRomEnd, this.addressMap._bootSegmentStart);
        this.romToRam(this.addressMap._codeSegmentRomStart, this.addressMap._codeSegmentRomEnd, this.addressMap._codeSegmentStart); // 8001ce60 - 802511d0
        this.romToRam(this.addressMap._gameplay_keepSegmentRomStart, this.addressMap._gameplay_keepSegmentRomEnd, MemLayout.GAMEPLAY_DATA);
        this.setSegmentAddress(SegmentOffsetType.GAMEPLAY_DATA, MemLayout.GAMEPLAY_DATA>>>0);

        // backup initial ram state for clean emulation
        this.takeSavestate(SavestateSlot.CLEAN_BOOT, "boot");
      } else {
        this.restoreSavestate(SavestateSlot.CLEAN_BOOT, "boot");
      }

      setupContext(this);
    }

    getMemoryView(): DataView
    {
        return this.memoryView;
    }

    getMemoryBuffer(): ArrayBufferLike
    {
        return this.memory.buffer;
    }

    getMemoryAsFile(): BinaryFile
    {
      const file = new BinaryFile(this.memory);
      file.setEndian(Endian.BIG);
      return file;
    }

    copyIntoMemory(data: Uint8Array, offset: number) {
      this.memory.set(data, offset);
    }

    copyFromMemory(data: Uint8Array, size: number, offsetMem: number, offsetOut: number = 0) {
      data.set(this.memory.subarray(offsetMem, offsetMem+size), offsetOut);
    }

    /**
     * Takes and overwrites a new savestate in the given slot.
     * The state is marked with a given UUID to later verify if wasn't overwritten.
     * No UUID checks are done here.
     * @param slot 
     * @param newUUID 
     * @returns 
     */
    takeSavestate(slot: SavestateSlot, newUUID: string): void
    {
      const state = this.savestates[slot];
      if(!state) {
        console.error("Invalid savestate-slot!");
        return;
      }

      //console.time("takeSavestate " + newUUID);      
      state.uuid = slot + "__" + newUUID;
      state.actorObjects = this.currentActorObjects.map(e => ({...e}));
      state.objectDataOffset = this.currentObjectDataOffset;
      state.sceneDep = this.currentSceneDep;
      memcpy(state.memory, this.memory, SAVESTATE_SIZE);

      this.loadedSavestate = state.uuid;
      //console.timeEnd("takeSavestate " + newUUID);
    }

    /**
     * Restores a saved savestate.
     * This is ignored if the slow is empty or the UUID does not match.
     * @param slot 
     * @param expectedUUID 
     */
    restoreSavestate(slot: SavestateSlot, expectedUUID: string): boolean
    {
      expectedUUID = slot + "__" + expectedUUID;

      const state = this.savestates[slot];
      if(!state || state.uuid !== expectedUUID) {
        console.log("Savestate UUID mismatch, ignore (%s != %s)", state.uuid, expectedUUID);
        return false;
      }

      if(this.loadedSavestate === expectedUUID) {
        //console.log("Savestate already loaded, skip");
        return true;
      }

      //console.time("restoreSavestate " + expectedUUID);
      this.memory.set(state.memory);
      this.currentActorObjects = state.actorObjects.map(e => ({...e}));
      this.currentObjectDataOffset = state.objectDataOffset;
      this.currentSceneDep = state.sceneDep;
      this.loadedSavestate = state.uuid;
      //console.timeEnd("restoreSavestate " + expectedUUID);

      return true;
    }

    hasSavestate(slot: SavestateSlot, expectedUUID: string): boolean {
      return this.savestates[slot].uuid === expectedUUID;
    }

    clearSavestate(slot: SavestateSlot) {
      this.savestates[slot].uuid = "";
      this.loadedSavestate = " ";
    }

    clearSavestates() {
      for(let save of this.savestates)save.uuid = "";
      this.loadedSavestate = " ";
    }

    loadFieldData()
    {
      const offsetStart = this.addressMap._gameplay_field_keepSegmentRomStart;
      const offsetEnd = this.addressMap._gameplay_field_keepSegmentRomEnd;
      
      this.currentSceneDep = SceneActorDependency.FIELD;
      this.romToRam(offsetStart, offsetEnd, MemLayout.FIELD_DGN_DATA);
      this.setSegmentAddress(SegmentOffsetType.ACTOR_DEPEND, MemLayout.FIELD_DGN_DATA>>>0);
      setObjectContext(this, this.currentSceneDep, this.currentActorObjects);
    }

    loadDungeonData()
    {
      const offsetStart = this.addressMap._gameplay_dangeon_keepSegmentRomStart;
      const offsetEnd = this.addressMap._gameplay_dangeon_keepSegmentRomEnd;

      this.currentSceneDep = SceneActorDependency.DUNGEON;
      this.romToRam(offsetStart, offsetEnd, MemLayout.FIELD_DGN_DATA);
      this.setSegmentAddress(SegmentOffsetType.ACTOR_DEPEND, MemLayout.FIELD_DGN_DATA>>>0);
      setObjectContext(this, this.currentSceneDep, this.currentActorObjects);
    }

    loadScene(scene: SceneEntry) 
    {
      if(!scene || !scene.offsetStart) {
        console.error("loadScene() failed: No Scene loaded!");
        return;
      }
    
      this.loadedSavestate = "";

      // Load scene into RAM and set segment offset
      memcpy(
        this.memory, this.rom, scene.offsetEnd - scene.offsetStart, 
        this.resolveAddress(MemLayout.SCENE_DATA), scene.offsetStart
      );

      this.setSegmentAddress(SegmentOffsetType.SCENE, MemLayout.SCENE_DATA);

      const headerPtr = (this.addressMap[scene.header.name] || 0) & 0x00FF_FFFF;

      // Collision
      const initColl = this.addressMap.Scene_CommandCollisionHeader;

      const headerPtrCollision = headerPtr + ((scene.header.indices.SCENE_CMD_COL_HEADER || 0) * 0x08);
      if(headerPtrCollision === 0 || !initColl) {
        console.log("Scene has no collision!", initColl.toString(16));
      } else {
        this.executeFunction(
          initColl, [MemLayout.CONTEXT, (MemLayout.SCENE_DATA + headerPtrCollision) >>> 0], 
          20_000_000
        );
      }

      // Light
      const initLight = this.addressMap.Scene_CommandLightSettingsList || this.addressMap.Scene_CommandEnvLightSettings;
      const headerPtrLight = headerPtr + ((scene.header.indices.SCENE_CMD_ENV_LIGHT_SETTINGS || 0) * 0x08);

      if(headerPtrLight === 0) {
        console.log("Scene has no lighting!");
      } else {
        this.executeFunction(initLight, [MemLayout.CONTEXT, (MemLayout.SCENE_DATA + headerPtrLight) >>> 0]);
      }

      // Paths
      const initPath = this.addressMap.Scene_CommandPathList;
      const headerPtrPaths = headerPtr + ((scene.header.indices.SCENE_CMD_PATH_LIST || 0) * 0x08);

      if(headerPtrPaths === 0) {
        console.log("Scene has no paths!");
      } else {
        this.executeFunction(
          initPath, [MemLayout.CONTEXT, (MemLayout.SCENE_DATA + headerPtrPaths) >>> 0], 
          200_000
        );
      }

      // Special Texture (MM only)
      if(Context.isMM() && scene.textureBank)
      {
        const texFun = this.resolveAddress(this.addressMap.Scene_LoadAreaTextures);
        if(texFun) 
        {
          this.executeFunction(texFun, [MemLayout.CONTEXT, scene.textureBank]);

          const texPtr = this.memoryView.getUint32(this.resolveAddress(MemLayout.CONTEXT) + 0x186E0 + 0x74);
          this.setSegmentAddress(0x06, this.resolveAddress(texPtr));
          console.log("Loaded Texture-Bank: ", texPtr.toString(16));

        } else {
          console.warn("Scene_LoadAreaTextures not found!");
        }
      }
    }

    private frame = 0;
    callSceneDraw(scene: SceneEntry) 
    {
      if(Context.isMM())return; // MM has no special-draw function

      let tableOffset = this.resolveAddress(this.addressMap.sSceneDrawConfigs);
      tableOffset += (scene.renderInitId || 0) * 4;
      const funcPtr = this.getMemoryView().getUint32(tableOffset, false);

      // update global frame counter
      const frameCountOffset = this.resolveAddress(MemLayout.CONTEXT) + 0x11DE4;
      const file = this.getMemoryAsFile();
      file.pos(frameCountOffset);
      file.write('u32', this.frame);
      ++this.frame;

      // Scene specific draw function
      this.executeFunction(funcPtr, [MemLayout.CONTEXT>>>0], 20_000_000);
    }

    /**
     * Writes lights to RAM and appends commands to both opaque/transp. display lists
     * @param lighting 
     * @returns 
     */
    updateLights(lighting?: SceneLighting) 
    {
      if(!lighting)return;
      const file = this.getMemoryAsFile();

      // Write F3D2EX light struct to memory
      file.pos(MemLayout.LIGHTS);

      const offsetLight0 = writeLight(file, lighting.diff0Color, lighting.diff0Dir);
      const offsetLight1 = writeLight(file, lighting.diff1Color, lighting.diff1Dir);
      const offsetLight2 = writeLight(file, lighting.ambientColor);
    
      addToDisplayLists(this, [
        0xDB02_0000, 2,            // enable 2 lights (excl. ambient)
        0xDC08_060A, offsetLight0, // set light color 0
        0xDC08_090A, offsetLight1, // set light color 1
        0xDC08_0C0A, offsetLight2, // set ambient light
      ]);

      // Also write to global context struct
      const lightCtxOffset = Context.isMM() ? 0x818 : 0x7A8;
      const offsetLightCtx = this.resolveAddress(MemLayout.CONTEXT) + lightCtxOffset;
      const offsetData = file.pos() + 8; // continue struct after DPL light data
      file.pos(offsetLightCtx);
      writeLightContext(file, lighting, offsetData);
    }

    /**
     * Loads actor data and its overlay into RAM, this the the structure an actor instance has.
     * For future actor calls, the returned overlay object should be used.
     * Note: only one actor at a time can be loaded!
     * @param overlay 
     * @param actor 
     */
    loadActor(overlay: ActorOverlay, actor: Actor): ActorOverlay
    {
      let allocOverlay = cloneActorOverlay(overlay);
      this.loadedSavestate = "";

      if(overlay.fileOffsetStart > 0) 
      {
        const overlaySize = overlay.fileOffsetEnd - overlay.fileOffsetStart;
        
        memcpy(
          this.memory, this.rom, overlaySize,
          this.resolveAddress(MemLayout.OVERLAY), overlay.fileOffsetStart
        );

        const overlayOffset = this.resolveAddress(MemLayout.OVERLAY);
        const overlayOffsetEnd = overlayOffset + overlaySize;
        allocOverlay = cloneActorOverlay(overlay);
        const allocOffsetDiff = overlayOffset - this.resolveAddress(overlay.fileVramOffsetStart);

        const headerOffset = overlayOffsetEnd - this.memoryView.getUint32(overlayOffsetEnd - 4);

        this.executeFunction(this.addressMap.Overlay_Relocate, [
          MemLayout.OVERLAY,
          headerOffset,
          overlay.fileVramOffsetStart >>> 0
        ]);
        this.mipsi.clearCache();
        
        allocOverlay.fileVramOffsetStart = MemLayout.OVERLAY;
        allocOverlay.fileVramOffsetEnd = MemLayout.OVERLAY + overlaySize;
        overlayFileMovePointer(allocOverlay, allocOffsetDiff);
      }

      initActorData(this, allocOverlay, actor);
      return allocOverlay;        
    }

    /**
     * Loads an object into RAM and sets the segment offset to it.
     * Note: Only one object at a time can be loaded!
     * @param obj 
     */
    loadObject(obj: ObjectTableEntry) 
    {
      if(obj.index <= 3) {
        // ignore gameplay-data since it is always loaded, dungeon/field is manually set
        return;
      }

      this.loadedSavestate = "";
      const targetAddress = this.resolveAddress(MemLayout.OBJECT_DATA) + this.currentObjectDataOffset;
      const objSize = obj.offsetEnd - obj.offsetStart;

      if((targetAddress + objSize) > this.resolveAddress(MemLayout.GAMEPLAY_DATA)) {
        console.error("loadObject OOB, overwrites gameplay_data [idx="+obj.index+"]: " + targetAddress.toString(16) +" - " + (targetAddress+objSize).toString(16));        
        return;
      }

      memcpy(
        this.memory, this.rom, objSize,
        targetAddress>>>0, obj.offsetStart
      );

      // move next address forward, align and leave a bit of space
      this.currentObjectDataOffset += (Math.ceil(objSize / 4) * 4) + 8;

      // set default segment address again
      this.setSegmentAddress(SegmentOffsetType.OBJECT, MemLayout.OBJECT_DATA>>>0);

      this.currentActorObjects.push({
        id: obj.index,
        ptr: targetAddress>>>0
      });

      setObjectContext(this, this.currentSceneDep, this.currentActorObjects);
    }

    executeActorInit(overlay: ActorOverlay)
    {
        // Call some global init functions to reset data
        this.executeFunction(this.addressMap.Matrix_Init, [MemLayout.CONTEXT]); // [game-state]
        const viewOffset = Context.isOOT() ? 0xA8 /*??*/ : 0xB8;
        this.executeFunction(this.addressMap.View_Init, [MemLayout.CONTEXT + viewOffset, MemLayout.GFX_CONTEXT]); // [View*, GFX-Context]

        if(overlay.file && overlay.file.ptrInit) {
          this.executeFunction(overlay.file.ptrInit, [MemLayout.ACTOR_DATA, MemLayout.CONTEXT]);
        }
    }

    executeActorUpdate()
    {
      const actorFunc = readActorFunction(this);
      if(actorFunc.update) {
          this.executeFunction(actorFunc.update, [MemLayout.ACTOR_DATA, MemLayout.CONTEXT]);
      }
    }

    /**
     * Executes the draw function and returns the VRAM-address to the created display-list.
     * @param overlay 
     * @returns pair of opaque and transparent DPL VRAM addresses
     */
    executeActorDraw(): [VRamAddress, VRamAddress]
    {
        const bankIndex = readActorBankIndex(this);

        const bankObjEntry = this.currentActorObjects[bankIndex];
        if(bankObjEntry) {
          this.setSegmentAddress(SegmentOffsetType.OBJECT, bankObjEntry.ptr);
        }

        this.executeFunction(this.addressMap.Actor_Draw, [MemLayout.CONTEXT, MemLayout.ACTOR_DATA]); 
        closeDisplayList(this);

        return [MemLayout.GFX_DL_OPAQUE, MemLayout.GFX_DL_TRANSP];
    }

    /**
     * Mocks some debugging functions like Printf with dummy functions
     * This will greatly reduce execution time.
     */
    mockDebugFunctions() 
    {
      // This allocater hangs, patch is with the regular one
      const mallocAddress = this.resolveAddress(this.addressMap.ZeldaArena_MallocDebug || 0);
      if(mallocAddress) 
      {
        Z64Emulator.mocks[mallocAddress] = (cpu: MipsiInstanceExports, memory: Uint8Array, size: number, strFile: number, line: number) => {
          //console.log("Malloc: ", size, readStringFromMemory(memory, strFile, 20), line);
          const ptr = allocMemory(this, size);
          cpu.cpuSetRegisterU32(MIPS_REG.$v0, ptr); // returns new actor pointer, return NULL for now
        };
          
        this.mipsi.mockFunction(mallocAddress);
      }

      // Actor spawns fails and hang-up the thread, replace with dummy for now
      const actorSpawnAddress = this.resolveAddress(this.addressMap.Actor_Spawn || 0);
      if(actorSpawnAddress) 
      {
        Z64Emulator.mocks[actorSpawnAddress] = (cpu: MipsiInstanceExports, memory: Uint8Array, actorCtx: number, play: any, actorId: any, posX: number) => {
          //console.log("Z64: Spawn new Actor", actorId, posX);
          // @TODO: properly spawn and display in the editor
          cpu.cpuSetRegisterU32(MIPS_REG.$v0, MemLayout.GARBAGE_DATA); // returns new actor pointer
        };
          
        this.mipsi.mockFunction(actorSpawnAddress);
      }

      // debug-logging
        const proutSyncPrintfAddress = this.resolveAddress(this.addressMap.is_proutSyncPrintf || 0);
        if(proutSyncPrintfAddress) 
        {
          Z64Emulator.mocks[proutSyncPrintfAddress] = (cpu: MipsiInstanceExports, memory: Uint8Array, argUnused: number, strPtr: any, strLen: any, arg3: any) => {
            const textRaw = readStringFromMemory(memory, strPtr, strLen);
            cpu.cpuSetRegisterU32(MIPS_REG.$v0, 1);
            EventBus.send(BusName.N64_LOG, textRaw);
          };
            
          this.mipsi.mockFunction(proutSyncPrintfAddress);
        }


        if(Context.isMM()) 
        {
          // DMA (won't work due to threading bullshit)
          const dmaMgrSendReqAddress = this.resolveAddress(this.addressMap.DmaMgr_SendRequest0 || 0);
          if(dmaMgrSendReqAddress) {
            Z64Emulator.mocks[dmaMgrSendReqAddress] = (cpu: MipsiInstanceExports, memory: Uint8Array, vramStart: number, vromStart: number, size: number) => {
              //console.log("DMA: ", (vramStart>>>0).toString(16), (vromStart>>>0).toString(16), (size>>>0).toString(16));
              this.romToRam(vromStart, vromStart + size, vramStart);
            };
            this.mipsi.mockFunction(dmaMgrSendReqAddress);
          }

          // debug logging (dummy in the actual game)
          const osSyncPrintfAddress = this.resolveAddress(this.addressMap.osSyncPrintf || 0);
          if(osSyncPrintfAddress) 
          {
            Z64Emulator.mocks[osSyncPrintfAddress] = (cpu: MipsiInstanceExports, memory: Uint8Array, ptr: number, arg1: number, arg2: number, arg3: number) => {
              let textRaw = readZeroStringFromMemory(memory, ptr);
              textRaw = textRaw.replace("%0", (arg1>>>0).toString(16).padStart(8, "0"));
              textRaw = textRaw.replace("%1", (arg2>>>0).toString(16).padStart(8, "0"));
              textRaw = textRaw.replace("%2", (arg3>>>0).toString(16).padStart(8, "0"));

              cpu.fpuSetRegisterS32(MIPS_REG.$v0, 1);
              EventBus.send(BusName.N64_LOG, textRaw);
            };
              
            this.mipsi.mockFunction(osSyncPrintfAddress);
          }
        }

        setRegisterEditorData(this);
    }

    /**
     * Executes a function at the given address, with up to 4 arguments.
     * All registers are set-up automatically.
     * @param ramAddress VRAM or RAM address
     * @param args optional argument (0-2)
     * @returns true on success, false on error
     */
    executeFunction(ramAddress: number, args: number[], intrLimit = -1)
    {
        ramAddress = this.resolveAddress(ramAddress);
        this.loadedSavestate = "";
        this.mipsi.reset(); // clears registers and counters
        intrLimit = (intrLimit < 0) ? OPERATIONS_LIMIT : intrLimit;

        try {
          this.mipsi.callFunction(
            ramAddress, MemLayout.STACK_PTR, intrLimit, args.length,
            args[0] || 0, args[1] || 0, 
            args[2] || 0, args[3] || 0,
          );
        } catch(e) {
          console.error(e);
          return false;
        }

        const actualCmdCount = this.mipsi.getCommandCount()>>>0;
        if(actualCmdCount >= intrLimit && actualCmdCount < 0xFFFF_FF00) {
          console.warn("Executed less instructions: " + actualCmdCount + " >= " + intrLimit);
        }

        return true;
    }
}