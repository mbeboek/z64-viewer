/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

import Convert from "ansi-to-html";

const MAX_STRING_LEN = 32000;
const textDecoder = new TextDecoder('cseucpkdfmtjapanese');
const htmlConvert = new Convert();

export function readStringFromMemory(memory: Uint8Array, offset: number, len = 0): string
{
    offset &= 0x00FF_FFFFF;
    return textDecoder.decode(memory.subarray(offset, offset + len));
}


export function readZeroStringFromMemory(memory: Uint8Array, offset: number): string
{
    offset &= 0x00FF_FFFFF;
    let offsetEnd = offset;
    for(let i=0; i<MAX_STRING_LEN; ++i) {
        if(memory[offsetEnd] === 0)break;
        ++offsetEnd;
    }

    return textDecoder.decode(memory.subarray(offset, offsetEnd));
}

export const logTextToHtml = (text: string) => 
{
  return htmlConvert.toHtml(text
    //.replace(/&/g, "&")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, '"')
    .replace(/'/g, "'")
  );
};