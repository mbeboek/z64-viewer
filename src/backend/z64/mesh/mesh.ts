/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
import { v4 as uuid } from 'uuid';
import { emptyGpuObject, GpuObject, GpuObjectVertexSize } from "../gpu/data/object";
import { AABB, aabbGetExtend, createAABB } from '../../utils/aabb';

export enum Z64MeshObjectType {
  OPAQUE,
  TRANSPARENT
};

export interface Z64Mesh {  
  uuid: string,
  gpuObjectOpaque: GpuObject,
  gpuObjectTransparent: GpuObject,
  aabb: AABB,
  isMap?: boolean,
}

export const isZ64MeshFilled = (mesh?: Z64Mesh) => {
  return mesh && (
    mesh.gpuObjectOpaque.buffer.byteLength > 0 ||
    mesh.gpuObjectTransparent.buffer.byteLength > 0
  );
}

export const emptyZ64Mesh = (): Z64Mesh => ({
  uuid: uuid(),
  gpuObjectOpaque: emptyGpuObject(),
  gpuObjectTransparent: emptyGpuObject(),
  aabb: createAABB(),
});

export function calcZ64MeshAABB(z64Mesh: Z64Mesh): AABB
{
  const aabb = createAABB();
  const objects = [z64Mesh.gpuObjectOpaque, z64Mesh.gpuObjectTransparent];

  for(let obj of objects) 
  {
      const vertCount = obj.buffer.byteLength / GpuObjectVertexSize;
      let offset = 0x00;
      for(let v=0; v<vertCount; ++v) {
          const vert = [
            obj.view.getFloat32(offset+0, true),
            obj.view.getFloat32(offset+4, true),
            obj.view.getFloat32(offset+8, true),
          ];
          aabb.min[0] = Math.min(aabb.min[0], vert[0]);
          aabb.min[1] = Math.min(aabb.min[1], vert[1]);
          aabb.min[2] = Math.min(aabb.min[2], vert[2]);

          aabb.max[0] = Math.max(aabb.max[0], vert[0]);
          aabb.max[1] = Math.max(aabb.max[1], vert[1]);
          aabb.max[2] = Math.max(aabb.max[2], vert[2]);
          offset += GpuObjectVertexSize;
      }
  }

  const extend = aabbGetExtend(aabb);
  if(extend[0] < 0.5)aabb.max[0] += 0.5;
  if(extend[1] < 0.5)aabb.max[1] += 0.5;
  if(extend[2] < 0.5)aabb.max[2] += 0.5;

  return aabb;
};