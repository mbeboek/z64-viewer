/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

// offsets and pointers
export type RomOffset = number; // direct ROM addresses
export type SegmentOffset = number; // indirect segment addresses
export type DataOffset = number; // neutral offset (VRAM, RAM , ROM or data) used for buffers or mixed offset-types

// Docs: https://wiki.cloudmodding.com/oot/Addresses#Segment_Addresses
export enum SegmentOffsetType {
    DIRECT        = 0x00,
    SCENE         = 0x02,
    ROOM          = 0x03,
    GAMEPLAY_DATA = 0x04,
    ACTOR_DEPEND  = 0x05,
    OBJECT        = 0x06,
    SEGMENT_7     = 0x07,
    ICONS_ITEM_0  = 0x08,
    ICONS_ITEM_1  = 0x09,
    SEGMENT_A     = 0x0A,
    SEGMENT_B     = 0x0B,
    SEGMENT_C     = 0x0C,
    SEGMENT_D     = 0x0D,
    SEGMENT_E     = 0x0E,
    SEGMENT_F     = 0x0F,
};
