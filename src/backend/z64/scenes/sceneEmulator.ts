/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */
import { SavestateSlot, Z64Emulator } from '../emulator/z64Emulator';
import { RoomEntry } from "../rooms/roomList";
import { SegmentOffsetType } from "../offsets";
import { calcZ64MeshAABB, emptyZ64Mesh } from "../mesh/mesh";
import { emulateDisplayList } from "../gpu/executeDisplayList";
import { MemLayout } from "../emulator/memoryLayout";
import { closeDisplayList, resetDisplayList } from "../emulator/displayList";
import { SceneLighting } from "../lighting/lighting";
import { setSaveData } from "../emulator/actorEmuBridge";
import { SceneEntry } from "./sceneList";
/**
 * Returns data specific to the given actor instance (emulation-data and GPU objects)
 * This data might change depending on the paramter value or even the position.
 * Note: This function might take longer to execute, since it's executing actors functions in an emulator.
 * @param actor current actor instance
 * @param scene scene the actor belongs to
 * @param room room the actor belongs to (first room for scene actors)
 * @param actorOverlay overlay
 * @param objEntry obj table entry
 */
export const emulateScene = async (scene: SceneEntry, rooms: RoomEntry[], layer: number, lighting?: SceneLighting) => 
{
    if(scene.sceneId < 0) {
        console.warn("emulateScene without a scene loaded, ignore");
        return;
    }
    
    console.time("== Emulate scene " + scene.sceneId);
    
    const z64Emu = Z64Emulator.getInstance();

    //z64Emu.restoreSavestate(SavestateSlot.SCENE, scene.sceneId.toString());

    await z64Emu.resetAndBoot();
    z64Emu.mockDebugFunctions();

    const isChild = layer < 1;
    setSaveData(z64Emu, isChild);
    
    // Load room data into RAM (includes display-lists)
    for(let room of rooms) {
        //if(scene.altId > 0)room = room.altRooms[scene.altId] || room;
        z64Emu.romToRam(room.offsetStart, room.offsetEnd, room.ramOffsetStart);
    }

    z64Emu.loadScene(scene);
    z64Emu.callSceneDraw(scene);
    z64Emu.updateLights(lighting);
    closeDisplayList(z64Emu);

    // Emulate room, and get mesh
    for(let room of rooms) 
    {
        if(!room.meshData) {
            continue;
        }
        room.mesh = emptyZ64Mesh();

        z64Emu.setSegmentAddress(SegmentOffsetType.ROOM, room.ramOffsetStart);

        console.time("DPL Scene");
        room.mesh.gpuObjectOpaque = emulateDisplayList(z64Emu, [MemLayout.GFX_DL_OPAQUE, ...room.meshData.opaque]);
        room.mesh.gpuObjectTransparent = emulateDisplayList(z64Emu, [MemLayout.GFX_DL_TRANSP, ...room.meshData.transparent]);
        room.mesh.isMap = true;
        console.timeEnd("DPL Scene");

        room.mesh.aabb = calcZ64MeshAABB(room.mesh);

        // use main room mesh for all alternative rooms
        for(let altRoom of room.altRooms) {
            if(altRoom)altRoom.mesh = room.mesh; 
        }
    }

    //downloadBlob(z64Emu.getMemoryBuffer().slice(0, RAM_SIZE), "scene.bin", "");

    resetDisplayList(z64Emu);

    z64Emu.takeSavestate(SavestateSlot.SCENE, scene.sceneId.toString());    
    console.timeEnd("== Emulate scene " + scene.sceneId);
};

/*const downloadBlob = (data: any, fileName: any, mimeType: any) => {
  
    const blob = new Blob([data], {type: mimeType})
    const downloadURL = (data: any, fileName: any) => {
        const a = document.createElement('a');
        a.href = data;
        a.download = fileName;
        document.body.appendChild(a);
        a.style.display = 'none';
        a.click();
        a.remove();
    }

    const url = window.URL.createObjectURL(blob)
    downloadURL(url, fileName)
    setTimeout(() => window.URL.revokeObjectURL(url), 1000);
}*/