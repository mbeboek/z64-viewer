/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { patchFileVar } from "../../decomp/c/writer";
import { SceneDataHeader } from "../../decomp/scenes";

const END_COMMAND = "SCENE_CMD_END";

export function saveSceneHeader(header: SceneDataHeader, src: string) 
{
  // Sort header values
  let mapKeys = Object.keys(header.map);
  mapKeys.sort((a, b) => header.indices[a] - header.indices[b]);
  mapKeys = mapKeys.filter(k => k !== END_COMMAND);

  // Map to string
  const headerData: Array<string> = [];
  for(const headerName of mapKeys) {
    const data = header.map[headerName];
    headerData.push(headerName + "(" + data.join(", ") + ")");
  }
  headerData.push(END_COMMAND + "()");

  let newSrc = patchFileVar(src, {
    type: 'SceneCmd',
    name: header.name,
    data: headerData
  }, true);

  if(!newSrc)throw new Error("Could not patch source (headers)!");
  return newSrc;
}