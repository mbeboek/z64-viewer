/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { SceneEntry } from "./sceneList";

// Returns the first non-null scene-layer, falls back to the main scene
export const getSceneLayer = (scene: SceneEntry, layerId: number): SceneEntry => {
  if(layerId < 0)return scene;
  for(let i=layerId; i>=0; --i) {
    const layer = scene.altSetups[i];
    if(layer)return layer;
  }
  return scene;
};

/**
 * Patches a scene if the given input is a layer
 * @param baseScene base scene
 * @param sceneOrLayer copied scene that was edited
 * @returns 
 */
export function patchScene(baseScene: SceneEntry, sceneOrLayer: SceneEntry): SceneEntry {
  if(sceneOrLayer.altId < 0) {
    return sceneOrLayer;
  }
  const newLayers = [...baseScene.altSetups];
  newLayers[sceneOrLayer.altId] = sceneOrLayer;
  return {...baseScene, altSetups: newLayers};
}