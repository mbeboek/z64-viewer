/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { v4 as uuid } from 'uuid';
import { RomOffset } from '../offsets';
import { SceneLighting,  } from '../lighting/lighting';
import { ActorType } from '../types';
import { Decomp } from '../../decomp/sceneTable';
import { readAddressMapCached } from '../../decomp/addressMap';
import { SceneData, SceneDataHeader, parseSceneFile, getSceneCmdVarRef } from '../../decomp/scenes';
import { SceneActor } from '../actors/actorList';
import { CollisionData } from '../collision/collisionTypes';
import { Context } from '../../context';
import { ScenePath } from '../../decomp/parser/parsePaths';

const SCENE_LIST_MAX_ENTRIES = 256; // failsafe if end-detection fails, would still result in some corrupt data

export enum SceneActorDependency {
  NONE = 0,
  FIELD = 2,
  DUNGEON = 3
};

/**
 * Docs: https://wiki.cloudmodding.com/oot/Scene_Table
 */
export interface SceneEntry {
    sceneId: number;
    altId: number;
    offsetStart: RomOffset;
    offsetEnd: RomOffset;
    dgnFlag0: number;
    renderInitId: number;
    dgnFlag1: number;

    header: SceneDataHeader,

    lighting: SceneLighting[],
    paths: ScenePath[],

    altSetups: Array<SceneEntry|undefined>,

    actors: SceneActor[];
    decompData: Decomp.SceneDef,
    actorDependency: SceneActorDependency;
    textureBank: number; // MM only
    collision: CollisionData;
    soundPreset: number;
    soundAmbience: number;
    soundBGM: number;
}

export interface SceneListEntry {
  sceneId: number;
  offsetStart: RomOffset;
  offsetEnd: RomOffset;
  decompData: Decomp.SceneDef,
};

function mapScene(sceneEntry: SceneListEntry, sceneData: SceneData, header: SceneDataHeader, altId = -1): SceneEntry
{
  const scene: SceneEntry = {
    sceneId: sceneEntry.sceneId,
    altId,
    offsetStart: sceneEntry.offsetStart,
    offsetEnd: sceneEntry.offsetEnd,
    dgnFlag0: 0,
    renderInitId: sceneEntry.decompData.drawCfgId,
    dgnFlag1: 0,
    header: header,
    lighting: [],
    altSetups: [],
    actors: [],
    decompData: sceneEntry.decompData,
    actorDependency: sceneData.dependency,
    textureBank: sceneData.textureBank,
    collision: sceneData.collision,
    soundPreset: sceneData.sound[header.name].preset,
    soundAmbience: sceneData.sound[header.name].ambience,
    soundBGM: sceneData.sound[header.name].bgm,
    paths: [],
  };

  if(header.map.SCENE_CMD_ENV_LIGHT_SETTINGS) {
    const lightData = sceneData.lighting[header.map.SCENE_CMD_ENV_LIGHT_SETTINGS[1]];
    if(lightData)scene.lighting = lightData;
  }

  if(header.map.SCENE_CMD_TRANSITION_ACTOR_LIST) {
    const actorVarName = header.map.SCENE_CMD_TRANSITION_ACTOR_LIST[1] || "";
    const actors = sceneData.actors[actorVarName] || [];
    scene.actors = actors.map(a => ({
      uuid: uuid(),
      type: ActorType.SCENE,
      roomId: 0,

      targetRoom: a.roomFront,
      targetCamConf: a.camIdxFront,
      backRoom: a.roomBack,
      backCamConf: a.camIdxBack,
      cutsceneIndex: a.cutsceneIndex || 0, // MM only
      id: a.id,
      initValue: a.params,
      pos: a.pos,
      rot: [0, a.rotY, 0],
    } as SceneActor));
  }

  if(header.map.SCENE_CMD_PATH_LIST) {
    const pathVarName = getSceneCmdVarRef(header.map.SCENE_CMD_PATH_LIST, 0);
    scene.paths = sceneData.paths[pathVarName] || [];
  }

  if(altId === -1) 
  {
    for(const i in sceneData.altHeaders) 
    {
      const altHeader = sceneData.altHeaders[i];
      if(altHeader) {
        scene.altSetups.push(
          mapScene(sceneEntry, sceneData, altHeader, parseInt(i))
        );
      } else {
        scene.altSetups.push(undefined);
      }
    }
  }

  return scene;
}

export async function loadScene(sceneEntry: SceneListEntry): Promise<SceneEntry>
{
  const decompData = sceneEntry.decompData;

  const sceneData = await parseSceneFile(decompData.path, decompData.name + ".c", Context.game);
  return mapScene(sceneEntry, sceneData, sceneData.mainHeader);
}

/**
 * Loads all scenes.
 * This only includes the header structures and a few basic data-blocks.
 * Collision, rooms etc. mus be loaded manually.
 */
export const loadAllScenes = async (): Promise<SceneListEntry[]> =>
{
    // First, check decomp if available...
    let decompScenes: Decomp.SceneDef[] = [];
    let addressMap: Record<string, number> = {};
    try {
      decompScenes = await Decomp.parseSceneTable();
      addressMap = await readAddressMapCached();
    } catch(e) {
      console.error("Failed to load scenes from decomp: ", e);
    }

    // ...then look at the rom to get actual offsets
    const scenes: SceneListEntry[] = [];
    for(let i=0; i<SCENE_LIST_MAX_ENTRIES; ++i)
    {
      const decompData = decompScenes[i];
      if(!decompData)break;

      const offsetStart = addressMap["_" + decompData.name + "SegmentRomStart"] || 0;
      const offsetEnd = addressMap["_" + decompData.name + "SegmentRomEnd"] || 0;

      scenes.push({
        sceneId: i,
        offsetStart,
        offsetEnd,
        decompData
      });
    }
    return scenes;
};
