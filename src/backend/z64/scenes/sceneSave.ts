/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Context } from "../../context";
import { ActorTable, parseActorTableCached } from "../../decomp/actorTable";
import { BINANG_TO_DEG } from "../../decomp/c/utils";
import { patchFileVar } from "../../decomp/c/writer";
import FileAPI from "../../fileapi/fileAPI";
import { SceneActor } from "../actors/actorList";
import { savePaths } from "../path/pathSave";
import { saveSceneHeader } from "./sceneHeaderSave";
import { SceneEntry } from "./sceneList";

function convertActorOOT(actorTable: ActorTable, a: SceneActor) {
  return [
    a.targetRoom, a.targetCamConf,
    a.backRoom, a.backCamConf,
    (actorTable.list[a.id]?.name || a.id),
    a.pos[0], a.pos[1], a.pos[2],
    a.rot[1], a.initValue
  ];
}

function convertActorMM(actorTable: ActorTable, a: SceneActor) {
  const rotDeg = BINANG_TO_DEG(a.rot[1]);
  const rotValue = (rotDeg << 7) | a.cutsceneIndex;

  return [
    a.targetRoom, a.targetCamConf,
    a.backRoom, a.backCamConf,
    (actorTable.list[a.id]?.name || a.id),
    a.pos[0], a.pos[1], a.pos[2],
    "0x" + rotValue.toString(16),
     a.initValue
  ];
}

export async function saveScene(scene: SceneEntry)
{
  //console.log("Save Scene", scene);

  const fullScenePath = scene.decompData.path + "/" + scene.decompData.name + ".c";
  const fullSceneHeaderPath = scene.decompData.path + "/" + scene.decompData.name + ".h";

  let [src, srcHeader, actorTable] = await Promise.all([
    FileAPI.readFileText(fullScenePath),
    FileAPI.readFileText(fullSceneHeaderPath),
    await parseActorTableCached()
  ]);

  const layers = [scene, ...scene.altSetups.filter(Boolean)] as SceneEntry[];
  for(const layer of layers) 
  {
    const header = structuredClone(layer.header);  

    if(header.map.SCENE_CMD_TRANSITION_ACTOR_LIST) 
    {
      // Actors
      const actorVarName = (header.map.SCENE_CMD_TRANSITION_ACTOR_LIST[1] || "").toString();
      if(actorVarName) 
      {
        // Collect data to save
        const actorVars = Context.isMM()
          ? layer.actors.map(a => convertActorMM(actorTable, a))
          : layer.actors.map(a => convertActorOOT(actorTable, a));

        // Patch source (actor list)
        let newSrc = patchFileVar(src, {
          type: 'ActorEntry',
          name: actorVarName,
          data: actorVars
        }, true);

        if(!newSrc)throw new Error("Could not patch source!");
        src = newSrc;

        header.map.SCENE_CMD_TRANSITION_ACTOR_LIST[0] = actorVars.length;

      } else {
        console.log("Scene has no scene-actors to save");
      }
    }

    // Sound
    if(header.map.SCENE_CMD_SOUND_SETTINGS) {
      header.map.SCENE_CMD_SOUND_SETTINGS = [
        "0x" + layer.soundPreset.toString(16), 
        "0x" + layer.soundAmbience.toString(16), 
        "0x" + layer.soundBGM.toString(16)
      ];
    }

    // Paths
    [src, srcHeader] = savePaths(header, src, srcHeader, layer.paths);
    //savePaths(structuredClone(header), src, srcHeader, layer.paths);
    
    // Fix header and patch header
    const newSrc = saveSceneHeader(header, src);
    if(!newSrc)throw new Error("Could not patch source! (header)");
    src = newSrc;
  }

  // Save back to scene
  await Promise.all([
    FileAPI.writeFileText(fullScenePath, src),
    FileAPI.writeFileText(fullSceneHeaderPath, srcHeader)
  ]);
}
