/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

import { BinaryFile } from "../../utils/binaryFile";
import { RomOffset } from "../offsets";
import { VRamAddress } from "../../z64/types";
import { codeRamToRom, readAddressMapCached } from "../../decomp/addressMap";

export interface ActorOverlayFile {
   id: number,
   type: number,
   flags: number,
   objectId: number,
   instanceSize: number,

   ptrInit: number,
   ptrDestroy: number,
   ptrUpdate: number,
   ptrDraw: number,
}

export interface ActorOverlay {
    fileOffsetStart: RomOffset,
    fileOffsetEnd: RomOffset,

    fileVramOffsetStart: VRamAddress,
    fileVramOffsetEnd: VRamAddress,
    initVramOffset: VRamAddress,

    fileNameOffset: VRamAddress,
    fileName: string,

    allocationType: number,
    file?: ActorOverlayFile,
}

export const cloneActorOverlay = (ovl: ActorOverlay) => 
{
  const newOvl: ActorOverlay = {...ovl};
  newOvl.file = ovl.file ? {...ovl.file} : undefined;
  return newOvl;
}

/**
 * Moves the file pointer by a given offset (after reallocation)
 * @param ovl 
 * @param offset 
 */
export const overlayFileMovePointer = (ovl: ActorOverlay, offset: number) => {
  if(ovl.file) {
    if(ovl.file.ptrInit)ovl.file.ptrInit += offset;
    if(ovl.file.ptrDestroy)ovl.file.ptrDestroy += offset;
    if(ovl.file.ptrUpdate)ovl.file.ptrUpdate += offset;
    if(ovl.file.ptrDraw)ovl.file.ptrDraw += offset;
  }
}

export interface ActorOverlayTable {
    overlays: ActorOverlay[]
};

const readOverlayFile = (f: BinaryFile): ActorOverlayFile => ({
    id: f.read('s16'),
    type: f.readAlign('u8', 2),
    flags: f.read('u32'),
    objectId: f.readAlign('s16', 4),
    instanceSize: f.read('s32'),
    
    ptrInit: f.read('u32'),
    ptrDestroy: f.read('u32'),
    ptrUpdate: f.read('u32'),
    ptrDraw: f.read('u32'),
});

export const loadActorOverlayTable = async (f: BinaryFile): Promise<ActorOverlayTable> => 
{
    const addressMap = await readAddressMapCached();
    const ovlRomStart = codeRamToRom(addressMap, addressMap.gActorOverlayTable);
    const ovlRomEnd = codeRamToRom(addressMap, addressMap.gMaxActorId); // next-var right after table

    f.posPushAndSet(ovlRomStart);
    
    const overlayCount = Math.round((ovlRomEnd - ovlRomStart) / 32);

    const offsetCodeRam = addressMap._codeSegmentStart;
    const offsetCodeRom = addressMap._codeSegmentRomStart;

    const ovlTable: ActorOverlayTable = {
        overlays: new Array(overlayCount)
    };

    for(let i=0; i<overlayCount; ++i) 
    {
        const overlay: ActorOverlay = {
            fileOffsetStart: f.read('u32'), 
            fileOffsetEnd: f.read('u32'), 
            fileVramOffsetStart: f.read('u32'), 
            fileVramOffsetEnd: f.readAndSkip('u32', 4), 
            initVramOffset: f.read('u32'),
            fileNameOffset: f.read('u32'), 
            fileName:  "0x" + i.toString(16).padStart(4, '0'), 
            allocationType: (f.read('u32') >> 16) & 0xFF,
            file: undefined
        };

        // @TODO: is this a RAM address?
        /*if(overlay.fileNameOffset) {
            overlay.fileName = readStringFromMemory(f.buffer, overlay.fileNameOffset);
        }*/

        if(overlay.fileOffsetStart) {
            const initRelative = overlay.initVramOffset - overlay.fileVramOffsetStart;
            f.readAt(overlay.fileOffsetStart + initRelative, 
                () => overlay.file = readOverlayFile(f)
            );
        } else if(overlay.initVramOffset) {
            // Maps RAM to ROM,  @TODO: read from RA directly later on
            const offsetRom = (offsetCodeRom + overlay.initVramOffset - offsetCodeRam); 
            f.readAt(offsetRom, () => overlay.file = readOverlayFile(f));
        }

        ovlTable.overlays[i] = overlay;
    }

    f.posPop();
    return ovlTable;
};