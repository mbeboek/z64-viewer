/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { actorData as actorDataOOT } from "./oot/actorData";
import { actorData as actorDataMM } from "./mm/actorData";

import { chestData as chestDataOOT } from "./oot/chestData";
import { chestData as chestDataMM } from "./mm/chestData";

import { collectibleData as collectibleDataOOT } from "./oot/collectibleData";
import { collectibleData as collectibleDataMM } from "./mm/collectibleData";

import { Context } from "../../context";

export const getGameActorData   = () => Context.isMM() ? actorDataMM : actorDataOOT;
export const getGameChestData   = () => Context.isMM() ? chestDataMM : chestDataOOT;
export const getGameCollectData = () => Context.isMM() ? collectibleDataMM : collectibleDataOOT;