/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { ActorType, Vector3 } from "../types";

export interface Actor {
  uuid: string, // unique in a scene, random across reloads
  type: ActorType;
  id: number,
  roomId: number,
  pos: Vector3,
  rot: Vector3,
  initValue: number,
  spawnTime: number, // MM only
  cutsceneIndex: number, // MM only
  rotMask: number, // MM only
};

export interface SceneActor extends Actor {
  targetRoom: number,
  targetCamConf: number,
  backRoom: number,
  backCamConf: number,
  // special case for 'rot', only Y is used
};

export type GenericActor = Actor | SceneActor;