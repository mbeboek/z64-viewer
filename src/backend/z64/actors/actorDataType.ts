/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export type ActorDataParamType = 'type' | 'enum' | 'number' | 'bool' | 'flag' | 'collectible' | 'chest';
export type ActorDataParamTarget = 'params' | 'xrot' | 'yrot' | 'zrot';

export interface ActorDataParam 
{
  type: ActorDataParamType;
  name: string;
  mask: number;
  shift: number;
  target: ActorDataParamTarget;
  typeFilter: number[];

  values?: Array<{value: string, name: string}>;
}

export interface ActorDataEntry 
{
  name: string;
  category: string;
  objDeps: string[];
  notes: string;
  params: ActorDataParam[];
}

export type ActorData = Record<string, ActorDataEntry>;