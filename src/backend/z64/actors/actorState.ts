/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/


import { Actor, GenericActor, SceneActor } from "./actorList";
import { RoomEntry } from "../rooms/roomList";
import { SceneEntry } from "../scenes/sceneList";
import { ActorType, Vector3 } from "../types";
import { v4 as uuid } from 'uuid';
import { fixObjectDependencies } from './actorDeps';
import { getSceneLayer } from "../scenes/sceneState";

export enum PatchAction {
  PATCH, ADD, DELETE
};

export interface TransformActor {
  uuid: string;
  pos: Vector3;
  rot: Vector3; 
}

// Returns the first non-null room-layer, falls back to the main room
export const getRoomLayer = (room: RoomEntry, layerId: number): RoomEntry => {
  if(layerId < 0)return room;
  for(let i=layerId; i>=0; --i) {
    const layer = room.altRooms[i];
    if(layer)return layer;
  }
  return room;
};

export const patchPos = <T extends TransformActor>(actor: T, newPos: Vector3): T => (
  {...actor, pos: newPos}
);

export const patchPosIdx = <T extends TransformActor>(actor: T, idx: number, val: number): T => {
  const newPos = [...actor.pos];
  newPos[idx] = val;
  return {...actor, pos: newPos};
};

export const patchRot = <T extends TransformActor>(actor: T, newRot: Vector3): T => (
  {...actor, rot: newRot}
);

export const patchRotIdx = <T extends TransformActor>(actor: T, idx: number, val: number): T => {
  const newRos = [...actor.rot];
  newRos[idx] = val;
  return {...actor, rot: newRos};
};

export const patchActors = (
  action: PatchAction,
  scene: SceneEntry, rooms: RoomEntry[], layerIdx: number,
  setScene: (scene: SceneEntry) => void, setRooms: (rooms: RoomEntry[]) => void,
  actors: Actor[]
) => {
  
  const newActors: GenericActor[] = [];
  let addedActor = false;

  const actorMap: Record<string, Actor> = {};
  const sceneActors: Actor[] = [];
  const roomActors: Actor[] = [];

  for(const a of actors) {
    actorMap[a.uuid] = a;
    if(a.type === ActorType.SCENE) {
      a.rot[0] = 0;
      a.rot[2] = 0;
      sceneActors.push(a);
    }
    if(a.type === ActorType.ROOM)roomActors.push(a);
  }

  const patch = (actorList: GenericActor[], patchActors: Actor[]) => { 
    switch(action) 
    {
      case PatchAction.PATCH:
        return actorList.map(oldActor => { 
          const newActor = actorMap[oldActor.uuid];
          if(newActor)newActors.push(newActor);
          return newActor ? structuredClone(newActor) : oldActor;
        });

      case PatchAction.ADD:
        for(const a of actors)a.uuid = uuid();
        newActors.push(...patchActors);
        addedActor = true;
        return [...actorList, ...patchActors];

      case PatchAction.DELETE: {
        const delUUIDs = actors.map(a => a.uuid);
        return actorList.filter(a => !delUUIDs.includes(a.uuid));
      }

      default: return actorList;
    }
  };
  
  if(sceneActors.length && scene) 
  {
    const layer = getSceneLayer(scene, layerIdx);
    if(layer.altId < 0) {
      setScene({...scene, actors: patch(scene.actors, sceneActors) as SceneActor[]});
    } else {
      const newLayers = [...scene.altSetups];
      newLayers[layer.altId] = {...layer, actors: patch(layer.actors, sceneActors) as SceneActor[]};
      setScene({...scene, altSetups: newLayers});
    }
  }

  if(roomActors.length) 
  {
    setRooms(rooms.map(room => 
    {
      const currentRoomActors = roomActors.filter(a => a.roomId === room.roomId);
      // Only add new actors to room set in the actor itself
      if(currentRoomActors.length === 0)return room;
      
      const layer = getRoomLayer(room, layerIdx);
      if(layer.altId < 0) {
        const newRoom = {...room, actors: patch(room.actors, currentRoomActors)};
        return addedActor ? fixObjectDependencies(newRoom) : newRoom;
      }

      const newLayers = [...room.altRooms];
      const newRoom = {...layer, actors: patch(layer.actors, currentRoomActors)};
      newLayers[layer.altId] = addedActor ? fixObjectDependencies(newRoom) : newRoom;

      return {...room, altRooms: newLayers};
    }));
  }

  return newActors;
}

export function patchRooms(newRoom: RoomEntry, rooms: RoomEntry[]) {
  const newRooms = [...rooms];
  
  const roomIdx = newRooms.findIndex(r => r.roomId === newRoom.roomId);
  if(roomIdx === -1)return newRooms;

  if(newRoom.altId >= 0) {
    const layer = getRoomLayer(newRooms[roomIdx], newRoom.altId);
    const layerArrIdx = newRooms[roomIdx].altRooms.findIndex(l => l === layer);

    if(layerArrIdx === -1)return newRooms;

    newRooms[roomIdx].altRooms = [...newRooms[roomIdx].altRooms];
    newRooms[roomIdx].altRooms[layerArrIdx] = newRoom;
    return newRooms;
  }

  newRooms[roomIdx] = newRoom;
  return newRooms;
}

export function selectionAdd(actors: Actor[], actor: Actor) {
  return [
    ...actors.filter(a => a.uuid !== actor.uuid),
    actor
  ];
}

export function selectionAddMulti(actors: GenericActor[], newActors: GenericActor[]) {
  const res = [...actors];
  for(let i in res) {
    const uuid = res[i].uuid;
    const patchActor = newActors.find(a => a.uuid === uuid);
    if(patchActor)res[i] = patchActor;
  }
  return res;
}

export function selectionRemove(actors: Actor[], actor: Actor) {
  return [
    ...actors.filter(a => a.uuid !== actor.uuid),
  ];
}

export function findActorByUUID(scene: SceneEntry, rooms: RoomEntry[], uuid: string) 
{
  if(!uuid)return undefined;
  const layerId = scene.altId;

  const roomActors = layerId >= 0 
    ? rooms.flatMap(r => r.altRooms[layerId]?.actors || [])
    : rooms.flatMap(r => r.actors || [])

  let actor = roomActors.find(a => a.uuid === uuid);

  if(!actor && scene) {
    actor = scene.actors.find(a => a.uuid === uuid);
  }
  return actor;
}