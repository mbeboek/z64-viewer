/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BinaryFile } from "backend/utils/binaryFile";
import { codeRamToRom, readAddressMapCached } from "../../decomp/addressMap";
import { RomOffset } from "../offsets";

export interface ObjectTableEntry {
    index: number,
    offsetStart: RomOffset,
    offsetEnd: RomOffset,
    size: number,
}

export interface ObjectTable {
    objectIdxLinkAdult: number,
    objectIdxLinkChild: number,
    objects: ObjectTableEntry[],
};

export const loadObjectTable = async (file: BinaryFile): Promise<ObjectTable> => 
{
    const addressMap = await readAddressMapCached();
    
    const objLinkAddress = codeRamToRom(addressMap, addressMap.gLinkObjectIds);
    const objTableAddress = codeRamToRom(addressMap, addressMap.gObjectTable);
    const objTableSizeAddress = codeRamToRom(addressMap, addressMap.gObjectTableSize);

    file.posPush();

    file.pos(objTableSizeAddress);
    const tableSize = file.read('s32');
    
    file.pos(objLinkAddress);
    const objTable = {
        objectIdxLinkAdult: file.read('u16'),
        objectIdxLinkChild: file.read('u16'),
        objects: new Array<ObjectTableEntry>(tableSize),
    };

    file.pos(objTableAddress);
    for(let i=0; i<objTable.objects.length; ++i) {
        objTable.objects[i] = {
            index: i,
            offsetStart: file.read('u32'),
            offsetEnd: file.read('u32'),
            size: 0,
        };
        objTable.objects[i].size = objTable.objects[i].offsetEnd - objTable.objects[i].offsetStart;
        //console.log("Object @" + objTable.objects[i].offsetStart.toString(16));
    }

    file.posPop();
    return objTable;
};