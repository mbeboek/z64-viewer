/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

// Math stuff

// Vectors are independent of Three.js and just a normal array
export type Vector4 = [number, number, number, number];
export type Vector3 = [number, number, number];
export type Vector2 = [number, number];

export type VectorRGBFloat = [number, number, number];
export type VectorRGBAFloat = [number, number, number, number];

// Offsets
export type VRamAddress = number;

export enum ActorType {
  SCENE, ROOM
};