/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export function s16AngleToRad(s16Angle: number): number {
    return s16Angle / 0x7FFF * Math.PI;
}

export function radAngleToS16(f32Angle: number): number {
    return Math.round(f32Angle / Math.PI * 0x7FFF);
}