/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { memcpy } from "backend/utils/memory";
import { createYaz0Header, yaz0Decode, YAZ0_HEADER_SIZE } from "../../n64/yaz0/yaz0";
import { searchFileTableOffset } from "../fileTable/fileTable";

const UNCOMPRESSED_ROM_SIZE = 64 * 1024 * 1024; // 64MB

/**
 * Decompresses a rom and returns a new uncompressed buffer.
 * Returns undefined if something goes wrong.
 * @param compressedRom compressed input
 */
export const decompressRom = (compressedRom: Uint8Array): Uint8Array | undefined => 
{
  const compressedRomView = new DataView(compressedRom.buffer);
  const outputBuffer = new Uint8Array(UNCOMPRESSED_ROM_SIZE);

  let fileTableOffset = searchFileTableOffset(compressedRomView);
  if(fileTableOffset === 0) {
    return undefined;
  }

  for(;; fileTableOffset += 0x10)
  {
      const vromStart = compressedRomView.getUint32(fileTableOffset,    false);
      const vromEnd   = compressedRomView.getUint32(fileTableOffset+4,  false);
      const romStart  = compressedRomView.getUint32(fileTableOffset+8,  false);
      const romEnd    = compressedRomView.getUint32(fileTableOffset+12, false);
  
      const virtualFileSize = vromEnd - vromStart;
      //console.log([vromStart, vromEnd, romStart, romEnd].map(x => x.toString(16)), (vromEnd - vromStart).toString(16));

      if(vromEnd === 0)break; // End of the list
      
      // sanity and OOB check  
      //if(vromEnd < highestVrom)continue;
      if(vromEnd < vromStart || vromStart >= UNCOMPRESSED_ROM_SIZE || vromEnd >= UNCOMPRESSED_ROM_SIZE)continue;

      if(romEnd === 0)
      {   
        memcpy(
          outputBuffer, compressedRom, virtualFileSize, 
          vromStart, romStart
        );
      } else {
          if(romStart >= compressedRom.byteLength || romEnd >= compressedRom.byteLength) {
            console.warn("Compressed file is OOB in the input file!");
            continue;
          }

          const compressedFileSize = romEnd - romStart;

          if(compressedFileSize < YAZ0_HEADER_SIZE) {
            console.warn("Yaz0 file detected that is too small!"); // garbage data a certain mod has...
            continue;
          }

          const viewIn = new DataView(compressedRom.buffer, romStart, compressedFileSize);

          /** @TODO check if really needed, oot seems to skip the header in favor of the file table entry
          const yaz0Header = getYaz0Header(viewIn);
          if(!yaz0Header) {
            console.warn("Invalid Yaz0 header!");
            continue;
          }
          */
          const yaz0Header = createYaz0Header(virtualFileSize);

          if(yaz0Header.size !== virtualFileSize) {
              console.warn("Yaz0 <-> File-Table size mismatch!"); // since yaz0 knows the real size, ignore it
          }

          const viewOut = new DataView(outputBuffer.buffer, vromStart, yaz0Header.size);

          try {
            yaz0Decode(yaz0Header, viewIn, viewOut);
          } catch(e) {
            console.error("Error decompressing ROM section!: ", e);
          }
      }
  }

  return outputBuffer;
};