/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { KibakoWebsocket } from "../native/kibakoWebsocket";
import { get as idbGet, set as idbSet } from 'idb-keyval';

type ConfigKey = 'decompDir' | 'lastScene';

export async function getConfig(name: ConfigKey, defaultVal?: string|number): Promise<any>
{
  const ws = KibakoWebsocket.getInstance();
  if(ws) {
    const conf = await ws.cmdReadConfig();
    return conf[name] || defaultVal;
  } else {
    return await idbGet(name) || defaultVal
  }
}

export async function patchConfig(name: ConfigKey, value: any): Promise<void>
{
  const ws = KibakoWebsocket.getInstance();
  if(ws) {
    const conf = await ws.cmdReadConfig();
    conf[name] = value;
    await ws.cmdWriteConfig(conf);
  } else {
    await idbSet(name, value);
  }
}