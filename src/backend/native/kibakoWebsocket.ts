import { BusName, EventBus } from "../../frontend/helper/eventBus";

/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
const utf8Encoder = new TextEncoder();
const utf8Decoder = new TextDecoder();

enum Command {
  NOP = 0,
  READ_FILE = 1,
  WRITE_FILE = 2,
  OPEN_DIR = 3,
  SET_BASE_DIR = 4,
  SCAN_DIR = 5,
  EXEC_MAKE = 6,
  READ_CONFIG  = 7,
  WRITE_CONFIG = 8,
}

interface PromiseEntry {
  resolve: (data: Uint8Array) => void;
  reject: (data: any) => void;
}

interface DirScanResult {
  files: string[];
  dirs: string[];
};

export class KibakoWebsocket
{
  static instance?: KibakoWebsocket = undefined;
  ws?: WebSocket;  
  id = 0;

  promiseMap: Record<number, PromiseEntry> = {};

  static getInstance() {
    if(this.instance) {
      return this.instance.ws ? this.instance : undefined;
    }
    return undefined;
  }

  async init(): Promise<boolean>
  {
    // this is injected into the webview, check this first...
    let wsUrl = "";
    let wsKey = "";

    // if launched in a browser, params are set via GET params
    const urlParams = new URLSearchParams(window.location.search);
    const wsInfo = urlParams.get("wsInfo");
    if(wsInfo) {
      try {
        const info = JSON.parse(window.atob(wsInfo));
        wsUrl = info.url;
        wsKey = info.key;
      } catch(e) {
        console.error("Failed to parse WS-Info, fallback to non-native...", e, wsInfo);
        return false;
      }
    } else {
      console.log("Running in Web-Mode, ignore WS");
      return false;
    }

    (window as any).ws = this;

    return new Promise((resolve, reject) => 
    {
      this.ws = new WebSocket(wsUrl+"/"+wsKey);
      this.ws.binaryType = "arraybuffer";
      
      this.ws.onopen = () => {
        console.log("Open");
        KibakoWebsocket.instance = this;
        resolve(true);
      }
    
      this.ws.onerror = (e) => {
        console.error(e);
        reject(e);
      };

      this.ws.onclose = (e) => {
        console.error("WS closed: ", e);
        this.ws = undefined;
      };

      this.ws.addEventListener("message", msg =>
      {
        // Text: stdout-stream
        // Binary: commands + data
        
        if(!(msg.data instanceof ArrayBuffer)) {
          EventBus.send(BusName.STDOUT, msg.data);
          return;
        }

        const view = new DataView(msg.data);
        const id = view.getUint16(0);
        const errorCode = view.getUint8(2);

        const resolver = this.promiseMap[id];
        if(!resolver) {
          return console.error("Received response to unknown message: ", id);
        }

        const dataBuffer = new Uint8Array(msg.data).subarray(4);
        if(errorCode) {
          resolver.reject(errorCode + ": " + utf8Decoder.decode(dataBuffer));
        } else {
          resolver.resolve(dataBuffer);
        }

        delete this.promiseMap[id];
      });
    });
  }

  /**
   * Sends an encoded message to the websocket.
   * 
   * intenral format:
   * {
   *   u16 id;
   *   u8 command;
   *   u8 argSize;
   *   char[argSize]; // argument string, zero terminated
   *   u8 data[]; // rest of the payload, optional
   * }
   * @param message 
   * @returns 
   */
  private send(command: Command, arg: string = "", data?: ArrayBuffer): Promise<Uint8Array>
  {
    if(!this.ws)throw new Error("WS not loaded");

    const argBuff = utf8Encoder.encode(arg+"\0");

    if(argBuff.byteLength >= 0xFF) {
      console.error("Argument too large: ", arg);
      throw Error("Argument too large");
    }

    this.id = (this.id+1) & 0xFFFF;
    const id = this.id;
    const dataOffset = 4 + argBuff.byteLength;
    const dataSize = data?.byteLength || 0;
    
    const msgBuffer = new Uint8Array(dataOffset + dataSize);
    const msgBufferView = new DataView(msgBuffer.buffer);
    msgBufferView.setUint16(0, id);
    msgBufferView.setUint8(2, command);
    msgBufferView.setUint8(3, argBuff.byteLength);
    msgBuffer.set(argBuff, 4);

    if(data) {
      msgBuffer.set(new Uint8Array(data), dataOffset);
    }

    return new Promise((resolve, reject) => {
      this.promiseMap[id] = {resolve, reject};
      this.ws!.send(msgBuffer);
    });
  }

  cmdReadFile(path: string) {
    return this.send(Command.READ_FILE, path);
  }

  async cmdWriteFile(path: string, data: Uint8Array|string) {
    const dataNorm = (data instanceof Uint8Array)
      ? data
      : utf8Encoder.encode(data);
    await this.send(Command.WRITE_FILE, path, dataNorm);
  }

  async cmdOpenDir() {
     const resBuffer = await this.send(Command.OPEN_DIR, "Open Decomp");
     return utf8Decoder.decode(resBuffer);
  }

  cmdSetBaseDir(path: string) {
    return this.send(Command.SET_BASE_DIR, path);
  }

  async cmdScanDir(path: string) {
    const str = utf8Decoder.decode(await this.send(Command.SCAN_DIR, path));
    const res: DirScanResult = {files: [], dirs: []};

    for(const file of str.split("\0")) {
      const arr = (file[0] === "F") ? res.files : res.dirs;
      arr.push(file.substring(1));
    }
    return res;
  }

  async cmdExecMake(cmd: string) {
    const resBuffer = await this.send(Command.EXEC_MAKE, cmd);
    return utf8Decoder.decode(resBuffer);
  }

  async cmdReadConfig(): Promise<Record<string, string|number>> {
    try {
      const data = await this.send(Command.READ_CONFIG);
      return JSON.parse(utf8Decoder.decode(data));
    } catch(e) {
      console.warn("Error fetching config, return empty", e);
    }
    return {};
  }

  async cmdWriteConfig(conf: Record<string, string|number>) {
    const buff = utf8Encoder.encode(JSON.stringify(conf, null, 2));
    await this.send(Command.WRITE_CONFIG, "", buff);
  }
}