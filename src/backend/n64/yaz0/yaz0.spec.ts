/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { getYaz0Header, yaz0Decode } from "./yaz0";

const createHeader = (length: number) => [
    0x59, 0x61, 0x7a, 0x30, // "Yaz0"
    ((length >> 24) & 0xFF), ((length >> 16) & 0xFF), ((length >> 8) & 0xFF), (length & 0xFF), // size
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 // flags
];

const copyBytes = (offset: number, length: number) =>
{
    const bytes = [0,0];

    --offset;
    if(length > 0x11) // 2 bytes
    {
        bytes.push((length - 0x12) & 0xFF);
    }else{ // 3 bytes
        bytes[0]  = ((length-2)  & 0xF) << 4;
    }

    bytes[0] |= (offset >> 8) & 0xF;
    bytes[1]  = offset & 0xFF;

    return bytes;
}

const decodeAndAssert = (bufferInArray: number[], bufferTestArray: number[]) =>
{
    let bufferIn = new Uint8Array(bufferInArray);
    const viewIn = new DataView(bufferIn.buffer);

    const yaz0Header = getYaz0Header(viewIn);
    if(!yaz0Header) {
        throw new Error("Can't read Yaz0 header!");
    }
    
    const outputBuffer = new Uint8Array(yaz0Header.size);
    const viewOut = new DataView(outputBuffer.buffer);
    
    yaz0Decode(yaz0Header, viewIn, viewOut);

    const bufferTest = new Uint8Array(bufferTestArray);

    expect(viewOut).toEqual(new DataView(bufferTest.buffer));
}


describe('Yaz0 decoding', () =>
{
    describe('general', () =>
    {
        it('empty buffer / 0 bytes', function() 
        {   
            decodeAndAssert([
                ...createHeader(0), 0
            ],[]);
        });

        it('correct output size', function() 
        {   
            const size = 0x1230;
            decodeAndAssert([
                ...createHeader(size), 
                ...(new Array(size + (size/8))).fill(0xFF)
            ], new Array(size).fill(0xFF));
        });
    });

    describe('decoding', () =>
    {
        it('simple values', function() 
        {   
            decodeAndAssert([
                ...createHeader(4), 
                0b11110000,
                0xA1, 0xA2, 0xA3, 0xA4
            ],[
                0xA1, 0xA2, 0xA3, 0xA4
            ]);
        });

        it('multi header', function() 
        {   
            decodeAndAssert([
                ...createHeader(16), 
                0b11111111,
                1,2,3,4,5,6,7,8,
                0b11111111,
                11,12,13,14,15,16,17,18
            ],[
                1,2,3,4,5,6,7,8,11,12,13,14,15,16,17,18
            ]);
        });

        it('simple copy at the end', function() 
        {   
            decodeAndAssert([
                ...createHeader(6), 
                0b11100000,
                0xA0, 0xB0, 0xC0, ...copyBytes(3, 3)
            ],[
                0xA0, 0xB0, 0xC0,  0xA0, 0xB0, 0xC0
            ]);
        });

        it('simple copy in the middle', function() 
        {   
            decodeAndAssert([
                ...createHeader(7), 
                0b11101000,
                0xAA, 0xBB, 0xCC, ...copyBytes(3, 3), 0xDD
            ],[
                0xAA, 0xBB, 0xCC,  0xAA,0xBB,0xCC, 0xDD
            ]);
        });

        it('partially repeating copy', function() 
        {   
            decodeAndAssert([
                ...createHeader(6), 
                0b11100000,
                0xA0, 0xB0, 0xC0, ...copyBytes(2, 3)
            ],[
                0xA0, 0xB0, 0xC0,  0xB0, 0xC0, 0xB0
            ]);
        });

        it('repeating copy', function() 
        {   
            decodeAndAssert([
                ...createHeader(6), 
                0b11100000,
                0xA0, 0xB0, 0xC0, ...copyBytes(1, 3)
            ],[
                0xA0, 0xB0, 0xC0,  0xC0,0xC0,0xC0
            ]);
        });

        it('big copy', function() 
        {   
            let resultArray = new Array(101);
            resultArray.fill(0xAA);

            decodeAndAssert([
                ...createHeader(101), 
                0b10000000,
                0xAA, ...copyBytes(1, 100)
            ], resultArray);
        });
    });

    describe('error handling', () =>
    {
        it('incomplete header', function() 
        {   
            const data = Buffer.from([42,42,42]);
            expect(() => getYaz0Header(new DataView(data))).toThrowError();
        });
/*
        it('file-size in header too big', function() 
        {   
            expect(() => yaz0.decode(Buffer.from([
                ...createHeader(10), 
                0b10000000,
                0xA0
            ]))).throw("Error parsing file");
        });

        it('file-size in header too big and no data', function() 
        {   
            expect(() => yaz0.decode(Buffer.from([
                ...createHeader(10), 
                0b10101010,
            ]))).throw("Error parsing file");
        });

        it('header but no value', function() 
        {   
            expect(() => yaz0.decode(Buffer.from([
                ...createHeader(1), 
                0b10000000
            ]))).throw("Error parsing file");
        });

        it('header but no copy-data', function() 
        {   
            expect(() => yaz0.decode(Buffer.from([
                ...createHeader(1), 
                0b00000000
            ]))).throw("Error parsing file");
        });

        it('copy offset too big', function() 
        {   
            decodeAndAssert([
                ...createHeader(4), 
                0b10000000,
                0xA0, ...copyBytes(10, 3)
            ], [
                0xA0, 0xA0, 0xA0, 0xA0
            ]);
        });

        it('copy length too big', function() 
        {   
            decodeAndAssert([
                ...createHeader(4), 
                0b10000000,
                0xA0, ...copyBytes(10, 5)
            ], [
                0xA0, 0xA0, 0xA0, 0xA0
            ]);
        });
*/
    });
});
