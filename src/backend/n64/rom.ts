/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BinaryFile, Endian } from '../utils/binaryFile';

export interface RomInfo {
    endian: Endian,
    romName: string;
    id: string;
    country: number;
    version: number;
};

const BOM_MARKER_LITTLE = 0x37;

export const readRomInfo = (f: BinaryFile): RomInfo =>
{
    f.posPushAndSet(0);

    const endian = f.read('u8') === BOM_MARKER_LITTLE ? Endian.LITTLE : Endian.BIG;
    f.setEndian(endian);
    f.setMixedEndian(endian === Endian.LITTLE); // N64 has a 2-byte word size

    f.pos(0x20);
    const romName = f.readU32String() + f.readU32String() + f.readU32String();
    f.pos(0x3C);
    const id = f.readU16String();
    const country = f.read('u8');
    const version = f.read('u8');

    f.posPop();
    return {endian, romName, id, country, version};
};