/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

// Converter functions for fixed-point floats

export const fixed8ToFloat = (value: number) => value / (1 << 8);
export const fixed16ToFloat = (value: number) => value / (1 << 16);
export const fixed24ToFloat = (value: number) => value / (1 << 24);
export const fixed32ToFloat = (value: number) => value / (1 << 32);

export const fixed10p2ToFloat = (value: number) => value / (1 << 2);
export const fixed10p5ToFloat = (value: number) => value / (1 << 5);
export const fixed0p16ToFloat = (value: number) => value / (1 << 16);

export const clamp = (value: number, min: number, max: number) => {
  return Math.max(Math.min(value, max), min);
};

export const f32ToS8 = (value: number) => {
  return clamp(
    Math.round(value < 0 ? (value * 128) : (value * 127)), 
    -128, 127
  );
};

export const s8ToF32 = (value: number) => {
  return clamp(
    value < 0 ? (value / 128) : (value / 127),
    -1.0, 1.0
  );
};

export const f32ToS16 = (value: number) => {
  return clamp(
    Math.round(value < 0 ? (value * 32768) : (value * 32767)), 
    -32768, 32767
  );
};

export const s16ToF32 = (value: number) => {
  return clamp(
    value < 0 ? (value / 32768) : (value / 32767),
    -1.0, 1.0
  );
};