/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export type Callback = (data: any) => void;

export enum BusName {
  ACTOR_EMU,      // command to start/stop emulation
  ACTOR_EMU_DATA, // data returned from emulating an actor (e.g. pos, flags)
  ACTOR_TEXTURES, // textures generated from emulating an actor
  NOTIFICATION,   // display a temporary notification (e.g. when saving successfully)
  STDOUT,         // streams stdout from a native process (e.g. building oot with 'make')
  N64_LOG,        // streams games internal debug log
  LOG_PANEL,      // opens or closes the log panel
  HISTOR_ADD,     // send a request to add a new history entry
  HISTOR_CHANGED, // notifies that the histroy has changed (undo, redo, clear)
  PATH_SELECTION, // sends path-point data that have been selected in the 3d-viewport (on click)
}

export class EventBus 
{
  private static busMap: Record<BusName, Set<Callback>> = {
    [BusName.ACTOR_EMU]: new Set(),
    [BusName.ACTOR_EMU_DATA]: new Set(),
    [BusName.ACTOR_TEXTURES]: new Set(),
    [BusName.NOTIFICATION]: new Set(),
    [BusName.STDOUT]: new Set(),
    [BusName.N64_LOG]: new Set(),
    [BusName.LOG_PANEL]: new Set(),
    [BusName.HISTOR_ADD]: new Set(),
    [BusName.HISTOR_CHANGED]: new Set(),
    [BusName.PATH_SELECTION]: new Set(),
  };

  static send(name: BusName, data: any = undefined) {
    //console.log(`Send Event to ${name}: ${JSON.stringify(data)}`);
    const bus = this.busMap[name];

    setTimeout(() => {
      for(const cb of bus) {
        cb(data);
      }
    }, 0);
  }

  static subscribe(name: BusName, cb: Callback)  {
    this.busMap[name].add(cb);
  }

  static unsubscribe(name: BusName, cb: Callback) {
    this.busMap[name].delete(cb);
  }
};