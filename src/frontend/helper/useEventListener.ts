/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { useEffect } from "react";

export function useEventListener(eventName: string, callback: (ev: Event) => any)
{
  useEffect(() => {
    document.addEventListener(eventName, callback);
    return () => {
      document.removeEventListener(eventName, callback);
    };
  }, [callback, eventName]);
};

export function useCanvasKeyUp(onKeypress: (e: KeyboardEvent) => void) {
  useEffect(() => {
    const canvas = document.getElementById("canvas");
    canvas && canvas.addEventListener("keyup", onKeypress);
    return () => {
      canvas && canvas.removeEventListener("keyup", onKeypress);
    }
  }, [onKeypress]);
}