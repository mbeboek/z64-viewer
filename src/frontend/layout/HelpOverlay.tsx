/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';
import styled from 'styled-components';

// Components
import { IconButton } from 'frontend/elements/IconButton';

// Icons
import { XCircle } from 'react-bootstrap-icons';
import { Cell, Table } from 'frontend/elements/Table';


const TableContainer = styled.div`
  display: flex;

  table {
    width: 365px;
  }

  & > div {
    margin: 10px 26px;
  }

  & h1 {
    text-align:center;
  }
`;

const Container = styled.div`
  z-index: 1000;
  color: white;
  top: 0;
  left: 0;
  right: 0;
  width: 100%;
  bottom: 0;
  height: 100%;
  display: flex;
  position: fixed;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  background-color: rgba(0,0,0, 0.95);
`;

const Key = styled.span`
  width: 26px;
  border: 1px solid #777;
  height: 26px;
  margin: 2px;
  display: flex;
  box-shadow: black 3px 3px 2px;
  text-align: center;
  border-radius: 5px;
  flex-direction: column;
  justify-content: center;
  background-color: rgba(255,255,255,0.1);
`;

interface HelpOverlayProps {
  onClose: () => void;
}

const KeyShift = <Key style={{width: '65px'}}>⇑ SHIFT</Key>;

const HelpOverlayComponent = (props: HelpOverlayProps) => {

  return (
    <Container>
      <TableContainer>
      <div>
        <h1 style={{marginBottom: '12px', fontSize: 40}}>
            Viewport
        </h1>
        <Table>
          <thead>
            <tr>
              <th align="center">Key</th>
              <th align="center">Action</th>
            </tr>
          </thead>
          <tbody>
              <tr>
                <td>
                  <Cell>
                    <Key>W</Key>
                    <Key>A</Key>
                    <Key>S</Key>
                    <Key>D</Key>
                  </Cell>
                </td>
                <td>
                  Move on the X/Z axis
                </td>
              </tr>

              <tr>
                <td>
                  <Cell>
                    <Key>Q</Key>
                    <Key>E</Key>
                  </Cell>
                </td>
                <td>
                  Move on the Y axis
                </td>
              </tr>

              <tr>
                <td>
                  <Cell>
                    <span style={{marginRight: '5px'}}>Hold</span>
                    {KeyShift}
                  </Cell>
                </td>
                <td>
                  Move faster
                </td>
              </tr>

              <tr>
                <td align="center">
                  Left-Click + Mouse<br/>
                  Right-Click + Mouse
                </td>
                <td>
                  Adjust viewing direction
                </td>
              </tr>

              <tr>
                <td align="center">
                  Left-Click
                </td>
                <td>
                  Select single <br/>Actor / Path-Point
                </td>
              </tr>

              <tr>
                <td align="center">
                {KeyShift} + Left-Click
                </td>
                <td>
                  Select multiple <br/>Actor / Path-Point
                </td>
              </tr>
            
          </tbody>
        </Table>
      </div>

      <div>
      <h1 style={{marginBottom: '12px', fontSize: 40}}>
            Selection
        </h1>
        <Table>
          <thead>
            <tr>
              <th align="center">Key</th>
              <th align="center">Action</th>
            </tr>
          </thead>
          <tbody>
              <tr>
                <td>
                  <Cell>
                    <Key>R</Key>
                  </Cell>
                </td>
                <td>
                  Toggle: Rotation/Translation
                </td>
              </tr>

              <tr>
                <td>
                  <Cell>
                    <Key>F</Key>
                  </Cell>
                </td>
                <td>
                  Toggle: Local/Global Translation
                </td>
              </tr>
              <tr>
                <td><Cell><Key style={{width: 64}}>DELETE</Key></Cell></td>
                <td>
                  Delete Actor /<br/>
                  Delete Path-Point
                </td>
              </tr>
              <tr>
                <td><Cell><Key style={{width: 50}}>CTRL</Key> + <Key>D</Key></Cell></td>
                <td>
                  Duplicate Actor /<br/>
                  Add Path-Point
                </td>
              </tr>
              <tr>
                <td><Cell><Key style={{width: 50}}>CTRL</Key> + <Key>C</Key></Cell></td>
                <td>Copy Actor</td>
              </tr>
              <tr>
                <td><Cell><Key style={{width: 50}}>CTRL</Key> + <Key>V</Key></Cell></td>
                <td>Paste Actor &amp; select</td>
              </tr>
              <tr>
                <td>
                  <Cell>{KeyShift} +</Cell>               
                  <Cell><Key style={{width: 50}}>CTRL</Key> + <Key>V</Key></Cell>
                </td>
                <td>Paste Actor &amp; add to selection</td>
              </tr>
            </tbody>
          </Table>

      </div>

      <div>
      <h1 style={{marginBottom: '12px', fontSize: 40}}>
            Global
        </h1>
        <Table>
          <thead>
            <tr>
              <th align="center">Key</th>
              <th align="center">Action</th>
            </tr>
          </thead>
          <tbody>

              <tr>
                <td><Cell>
                  <Key style={{width: 50}}>CTRL</Key> + <Key>Z</Key>
                </Cell></td>
                <td>Undo</td>
              </tr>

              <tr>
                <td>
                  <Cell>
                    <Key style={{width: 50}}>CTRL</Key>  {KeyShift} + <Key>Z</Key>
                  </Cell>

                  <Cell>or</Cell>

                  <Cell>
                    <Key style={{width: 50}}>CTRL</Key> + <Key>Y</Key>
                  </Cell>
                </td>
                <td>Redo</td>
              </tr>

              <tr>
                <td>
                  <Cell>
                    <Key style={{width: 50}}>CTRL</Key> +
                    <Key>S</Key>
                  </Cell>
                </td>
                <td>Save</td>
              </tr>
              <tr>
                <td><Cell><Key>F8</Key></Cell></td>
                <td>Start/Stop Emulation</td>
              </tr>
              <tr>
                <td><Cell><Key>F9</Key></Cell></td>
                <td>Save, Build & Run</td>
              </tr>
            </tbody>
          </Table>

      </div>
      </TableContainer>

      <IconButton
        text="Close"
        Icon={XCircle}
        onClick={() => props.onClose() }
      />

    </Container>
  );
}

export const HelpOverlay = React.memo(HelpOverlayComponent, (prev, next) => {
    return true;
});
