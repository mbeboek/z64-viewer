/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useState, useEffect, useCallback } from 'react';

// Backend
import { loadRom } from '../backend/main';

// Styling
import styled from 'styled-components';

// Custom Components
import { HeaderBar } from './layout/HeaderBar';
import { PageSceneViewer } from './pages/scene/PageSceneViewer';
import { PageRomSelect } from './pages/romSelect/PageRomSelect';
import { Endian } from 'backend/utils/binaryFile';
import { decompressRom } from 'backend/z64/compression/decompressRom';
import { Z64Emulator } from 'backend/z64/emulator/z64Emulator';
import { Notifications, showNotification } from './layout/Notifications';
import { Context, Game } from '../backend/context';
import { PageActorParams } from './pages/tools/PageActorParams';
import { DebugKeysComponent } from './pages/tools/DebugKeys';
import Config from '../config';
import { preventDefault } from './helper/events';
import { useEventListener } from './helper/useEventListener';

const Root = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
`;

export const App = () => {

  const [romBuffer, setRomBuffer] = useState<Uint8Array>();
  const [dataLoaded, setDataLoaded] = useState(false);
  const [showDebugKeys, setshowDebugKeys] = useState(Config.SHOW_KEYS);

  const pageActorParams = window.location.href.endsWith("actor-params");

  const onData = (data: Uint8Array) => {
    setRomBuffer(data);
  };

  useEffect(() => {
    console.log("Reload ROM...");

    if(!romBuffer || romBuffer.length === 0) {
      console.log("No ROM loaded, skip reload!");
      return;
    }

    if(romBuffer.byteLength < (40 * 1024 * 1024)) 
    {
      const uncomp = decompressRom(romBuffer);
      if(uncomp === undefined) 
      {
        showNotification('error', "Could not decompress ROM!");  
        setRomBuffer(undefined);
      }

      setRomBuffer(uncomp);
      return;
    }

    const load = async () => 
    {
      const romInfo = await loadRom(romBuffer);

      if(romInfo.id !== "ZL" && romInfo.id !== "ZS")
      { 
        showNotification('error', "Wrong ROM detected ("+romInfo.id+")!");
        setRomBuffer(undefined);
        return;
      }
  
      if(romInfo.endian === Endian.LITTLE) {
        showNotification('error', "Little-Endian ROM detected!");
        setRomBuffer(undefined);
        return;
      }
  
      Context.rom = romBuffer;
      Context.game = (romInfo.id === "ZS" || romInfo.romName.includes("MAJORA")) ? Game.MM : Game.OOT;
      
      try {
        Z64Emulator.getInstance();
        await Z64Emulator.loadWasm();
        setDataLoaded(true);
      } catch(e: any) {
        console.error(e);
        showNotification('error', "Failed to load WASM Module: " + e.message);
      }
    }; 
    load().catch(e => console.error(e));
    
  }, [romBuffer]);

  const handleKey = useCallback((ev: Event) => {
    const e = ev as KeyboardEvent;
    if(!e.ctrlKey && e.code === "F1") {
      setshowDebugKeys(!showDebugKeys);
    }
    preventDefault(e);
  }, [showDebugKeys]);

  useEventListener('keydown', handleKey);

  return <Root>
    {!romBuffer && (
      !pageActorParams
        ? <PageRomSelect onData={onData} />
        : <PageActorParams />
    )}

    {dataLoaded && <>
      <HeaderBar/>
      <PageSceneViewer/>          
    </>}

    <Notifications />

    {showDebugKeys && <DebugKeysComponent />}
  </Root>;
};
