/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export const Colors = {
  PRIMARY_DARK: '#f57c00',
  PRIMARY: '#ff9800',
  PRIMARY_LIGHT: '#ffb74d',

  PRIMARY_DARK_RGB: '245, 124, 0',
  PRIMARY_RGB: '255, 152, 0',
  PRIMARY_LIGHT_RGB: '255, 183, 77',

  MONO_DARK: '#777', // border, <hr>
  MONO: '#AAA', // primary

  MONO_RGB: '155,155,155',

  INPUT_BG: '#484848',
  INPUT_BG_HOVER: '#5f5f5f',

  BG_LIGHT: '#333', // bar-left/right, buttons
  BG_DARK: '#222', // title-bar

  BG_LIGHT_RGB: '68,68,68',
  BG_DARK_RGB: '48,48,48',

  BORDER_DARK: '#111',
};