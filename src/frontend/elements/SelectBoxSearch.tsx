/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React, { useCallback, useRef } from 'react';

import SelectSearch from 'react-select-search';
import styled from 'styled-components';

const TOTAL_HEIGHT = 30;
const LABEL_HEIGHT = 14;

const SelectWrapper = styled.div/*_css*/`
  position: relative;
  width: 100%;
  height: ${TOTAL_HEIGHT}px;
  &:hover .select-name{
    opacity: 0; 
    background-color: ${Colors.INPUT_BG_HOVER};
  }
`;

const InputLabel = styled.div/*_css*/`
  font-size: 11px;
  position: relative;
  top: -${TOTAL_HEIGHT - 6}px;
  right: calc(-100% + 82px);
  width: 80px;
  
  z-index: 1;
  text-align: right;
  padding: 2px;
  height: ${LABEL_HEIGHT+8}px;
  color: #CCC;
  background-color: ${Colors.INPUT_BG};
  opacity: 1;
  pointer-events: none;

  transition: opacity 80ms cubic-bezier(0.4, 0, 0.2, 1) 10ms;
`;

export type SelectBoxOptions = Array<{name: string, value: string}>; 

interface Props {
    name: string;
    value: string;
    options: SelectBoxOptions;
    onChange:(value: string) => void;
};

export const SelectBoxSearchComponent = (props: Props) =>  
{
  const filteredOptions = useRef([] as any[]);

  const filterFunc = useCallback(
    (options: any[], val: string) => {
      filteredOptions.current = options.filter(o => o.name.toLowerCase().includes(val));
      return filteredOptions.current;
   }
  , [filteredOptions]);

  return <SelectWrapper>
    <SelectSearch 
        options={props.options} search placeholder={props.name} 
        filterOptions={[filterFunc]}
        value={props.value} 
        onChange={(newVal) => {
          const orgIndex = props.options.findIndex(e => e.value === newVal);
          props.onChange(filteredOptions.current[orgIndex]
              ? filteredOptions.current[orgIndex].value
              : newVal
          );
        }}
      />
    <InputLabel className='select-name'>{props.name} ▼</InputLabel>
  </SelectWrapper>;
};

export const SelectBoxSearch = React.memo(SelectBoxSearchComponent, (prev, next) => {
  return prev.name === next.name 
    && prev.value === next.value
    && prev.options === next.options
    && prev.onChange === next.onChange;
});