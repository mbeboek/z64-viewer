/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React from 'react';
import styled from 'styled-components';

const TOTAL_HEIGHT = 30;
const LABEL_HEIGHT = 14;

const SelectWrapper = styled.div/*css*/`
  position: relative;
  width: 100%;
  height: ${TOTAL_HEIGHT}px;
  padding-right: 8px;
  background-color: ${Colors.INPUT_BG};
  transition: background-color 80ms cubic-bezier(0.4, 0, 0.2, 1) 10ms;

  &:hover {
    background-color: ${Colors.INPUT_BG_HOVER};
    div { 
      opacity: 0; 
      background-color: ${Colors.INPUT_BG_HOVER};
    }
  }
`;

const SelectElem = styled.select/*_css*/`
  background-color: unset;
  color: #FFF;
  width: 100%;
  padding: 0 0px;
  margin: 0px 2px;
  height: ${TOTAL_HEIGHT - 2}px;
  border: 0px;
  font-size: 14px;

  &:hover {
    background-color: ${Colors.INPUT_BG_HOVER};
  }

  & option {
    background-color: #656565;
  }
`;

const InputLabel = styled.div/*_css*/`
  font-size: 11px;
  position: relative;
  top: -${TOTAL_HEIGHT - 6}px;
  right: calc(-100% + 94px);
  width: 80px;
  
  text-align: right;
  padding: 2px;
  height: ${LABEL_HEIGHT+8}px;
  color: #CCC;
  background-color: ${Colors.INPUT_BG};
  opacity: 1;
  pointer-events: none;

  transition: opacity 80ms cubic-bezier(0.4, 0, 0.2, 1) 10ms;
`;

interface SelectBoxComponentProps {
    name: string;
    value: string;
    options: [string|number, string][];
    small?: boolean;
    readOnly?: boolean;
    onChange?:(value: string) => void;
};

export const SelectBoxComponent = (props: SelectBoxComponentProps) =>  
{
    const readOnly = props.readOnly || !props.onChange;
    return <SelectWrapper>
      <SelectElem onChange={(e) => props.onChange?.(e.target.value)} value={props.value}>
        {props.options.map(data => 
          <option 
            key={data[0]} 
            value={data[0]} 
            disabled={readOnly}
          >{data[1]}</option>
        )}
      </SelectElem>
      {!props.small && <InputLabel>{props.name}</InputLabel>}
    </SelectWrapper>;
};

export const SelectBox = React.memo(SelectBoxComponent, (prev, next) => {
  return prev.name === next.name 
    && prev.value === next.value
    && prev.options === next.options
    && prev.small === next.small
    && prev.onChange === next.onChange
    && prev.readOnly === next.readOnly;
});