/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import styled from 'styled-components';

export const ListItem = styled.div/*css*/`
  padding: 6px 16px;
  display: flex;
  align-items: center;

  &:hover {
    background-color: rgba(255, 255, 255, 0.08);
  }
`;

export const ListItemText = styled.div/*css*/`
  padding: 8px 16px 0 16px;
  display: flex;
  align-items: center;
`;


export const ListItemFullWidth = styled(ListItem)/*css*/`
  & > div {
    width: 100%;
  }
`;

export const ListContainer = styled.div/*css*/`
  padding-top: 10px;
`;
