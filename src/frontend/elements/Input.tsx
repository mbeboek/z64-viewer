/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React from 'react';
import styled from 'styled-components';

const InputWrapper = styled.div/*css*/`
  display: flex;
  position: relative;
  background-color: ${Colors.INPUT_BG};
  transition: background-color 80ms cubic-bezier(0.4, 0, 0.2, 1) 10ms;
  height: 30px;

  &:hover {
    background-color: ${Colors.INPUT_BG_HOVER};

    div { 
      background-color: ${Colors.INPUT_BG_HOVER};
    }
  }
`;

const InputLabel = styled.div/*_css*/`
  padding: 3px 12px 3px 3px;
  font-size: 11px;
  color: #CCC;

  display: flex;
  flex-direction: column;
  justify-content: center;

  min-width: 10px;
  text-align: left;

  background-color: ${Colors.INPUT_BG};
  opacity: 1;
  pointer-events: none;

  border-bottom: 1px solid #CCC;
  border-right: 1px solid rgba(255,255,255, 0.1);

  transition: background-color 80ms cubic-bezier(0.4, 0, 0.2, 1) 10ms;
`;

const InputElem = styled.input/*css*/`
  color: white;
  width: 100%;
  border: none;
  outline: none;
  padding: 6px 8px;
  font-size: 14px;
  background: none;
  box-sizing: border-box;
  border-bottom: 1px solid #CCC;

  &::placeholder {
    opacity: 0;
  }
  &:focus {
    border-bottom-color: #ff9800;
  }
`;

interface InputComponentProps {
    name: string;
    value: string | number;
    readOnly?: boolean;
    onChange?:(value: any) => void;
};

export const InputComponent = (props: InputComponentProps) =>  
{
    return <InputWrapper>
        {(props.name.length>0) && <InputLabel>{props.name}</InputLabel>}
        <InputElem 
          type="text" 
          placeholder={props.name}
          value={props.value}
          readOnly={props.readOnly || !props.onChange}
          onChange={props.onChange}
          style={props.readOnly ? {cursor: 'not-allowed', userSelect: 'none'} : {}}
        />
      </InputWrapper>;
};

export const Input = React.memo(InputComponent, (prev, next) => {
  return prev.name === next.name 
    && prev.value === next.value
    && prev.onChange === next.onChange
    && prev.readOnly === next.readOnly;
});