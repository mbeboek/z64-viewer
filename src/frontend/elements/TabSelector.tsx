/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { ComponentType } from 'react';
import { IconProps } from 'react-bootstrap-icons';
import styled from 'styled-components';
import { Colors } from '../style/colors';

const TabContainer = styled.div`
  margin-top: 0px;
  display: flex;
  justify-content: space-between;
  
`;

const Tab = styled.div`
  position: relative;
  flex-grow: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 2px solid transparent;
  margin: 0 0px;
  padding: 4px 0;
  height: 32px;

  color: #ddd;
  background-color: ${Colors.BG_DARK};
  border-radius: 5px 5px 0 0;
  cursor: pointer;
  user-select: none;

  &.selected {
    background-color: ${Colors.BG_LIGHT};
    border-color: transparent;
    border-top-color: ${Colors.PRIMARY_LIGHT};
    color: white;
  }

  &:hover {
    background-color: ${Colors.MONO_DARK};
  }

  &.selected:hover {
    background-color: rgba(${Colors.BG_LIGHT_RGB}, 0.7);
  }

  &:active {
    opacity: 0.8;
  }
`;

export interface TabEntry {
  id: string;
  text: string;
  Icon: ComponentType<IconProps>;
};

interface Props {
    selectedId: string;
    entries: TabEntry[];
    onSelect: (id: string) => void;
};

const TabSelectorComponent = (props: Props) => {

  const {selectedId, entries, onSelect} = props;

  return (
      <TabContainer className='tab_selector'>{entries.map(e => 
        <Tab key={e.id} className={e.id === selectedId ? 'selected' : ''}
          onClick={() => onSelect(e.id)}
          title={e.text}
        >
          <e.Icon size={16} />
        </Tab>
      )}</TabContainer>
    
  );
}

export const TabSelector = React.memo(TabSelectorComponent, (prev, next) => {
  return prev.entries === next.entries
  && prev.selectedId === next.selectedId
  && prev.onSelect === next.onSelect;
});