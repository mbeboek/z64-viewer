/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React, { ComponentType } from 'react';
import { IconProps } from 'react-bootstrap-icons';
import styled from 'styled-components';

const Chip = styled.button`
  pointer-events: auto;
  box-shadow: black 0px 0px 2px;
  border: 1px solid ${Colors.MONO};
  border-radius: 16px;
  padding: 4px 12px;
  padding-right: 16px;
  height: 32px;
  min-width: 100px;
  background: none;
  color: white;
  margin: 4px;
  cursor: pointer;

  display: flex;
  align-items: center;
  justify-content: left;
  font-family: Roboto;
  font-weight: 500;
  font-size: 13px;

  transition: border-color 100ms linear 0ms, 
              background-color 70ms linear 0ms;

  &:hover {
    border-color: ${Colors.MONO_DARK};
    background-color: rgba(${Colors.MONO_RGB}, 0.1);
  }

  &:active {
    border-color: ${Colors.MONO_DARK};
    background-color: rgba(${Colors.MONO_RGB}, 0.3);
  }
`;

const ChipActive = styled(Chip)`
  border: 1px solid ${Colors.MONO};
  background-color: ${Colors.BG_LIGHT};
  border-color: ${Colors.BG_LIGHT};

  &:hover {
    background-color: rgba(${Colors.BG_LIGHT_RGB}, 1.0);
  }
`;

interface IconChipProps {
    text: string;
    Icon: ComponentType<IconProps>;
    onClick: (state: boolean) => void;
    active: boolean;
};

const IconChipComponent = (props: IconChipProps) =>  
{ 
  const ChipElem = props.active ? ChipActive : Chip;

  return <ChipElem onClick={() => props.onClick(!props.active)}>
      <props.Icon size={18} style={{marginRight: 8}} />
      {props.text}
    </ChipElem>;
};

export const IconChip = React.memo(IconChipComponent, (prev, next) => {
  return prev.text === next.text &&
    prev.active === next.active &&
    prev.onClick === next.onClick;
});