/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React, { useCallback, useRef } from 'react';
import { useEffect } from 'react';
import styled from 'styled-components';

const InputWrapper = styled.div/*css*/`
  display: flex;
  position: relative;
  background-color: ${Colors.INPUT_BG};
  transition: background-color 80ms cubic-bezier(0.4, 0, 0.2, 1) 10ms;
  height: 30px;

  &:hover {
    background-color: ${Colors.INPUT_BG_HOVER};

    div { 
      background-color: ${Colors.INPUT_BG_HOVER};
    }
  }
`;

const HexInfo = styled.span/*_css*/`
  color: #aaa;
  font-size: 10px;
  position: absolute;
  top: 4px;
  right: 4px;
`;

const InputLabel = styled.div/*_css*/`
  user-select: none;
  padding: 3px 12px 3px 3px;
  font-size: 11px;
  color: #CCC;

  display: flex;
  flex-direction: column;
  justify-content: center;

  min-width: 10px;
  text-align: left;

  background-color: ${Colors.INPUT_BG};
  opacity: 1;
  pointer-events: none;

  border-bottom: 1px solid #CCC;
  border-right: 1px solid rgba(255,255,255, 0.1);

  transition: background-color 80ms cubic-bezier(0.4, 0, 0.2, 1) 10ms;
`;

const InputElem = styled.input/*css*/`
  color: white;
  width: 100%;
  border: none;
  outline: none;
  padding: 6px 8px;
  font-size: 14px;
  background: none;
  box-sizing: border-box;
  border-bottom: 1px solid #CCC;

  &::placeholder {
    opacity: 0;
  }
  &:focus {
    border-bottom-color: #ff9800;
  }
`;

interface ComponentProps {
    name: string;
    value: number;
    index?: number;
    hex?: boolean;
    onChange?:(value: any, index: number) => void;
};

export const IntInputComponent = (props: ComponentProps) =>  
{
  const { index, value } = props;
  const onChange = props.onChange;
  const readOnly = !props.onChange;
  const hex = !!props.hex;

  const inputRef = useRef<HTMLInputElement>(null);

  const valToStr = useCallback((val: number) => hex 
    ? val.toString(16).toUpperCase()
    : val.toString()
  , [hex]);

  const strToVal = useCallback((val: string) => parseInt(val, hex ? 16 : 10), [hex]);

  if(inputRef.current) {
    inputRef.current.value = valToStr(value);
  }
  useEffect(() => {
    if(inputRef.current)inputRef.current.value = valToStr(value);
  }, [inputRef, value, valToStr]);

  const inputChange = useCallback((e: any) => 
  {
    if(!e.target)return;
    let targetVal = e.target.value;
    if(hex)targetVal = targetVal.toUpperCase();

    const valInt = strToVal(targetVal);

    // Only update real value when it's a valid int
    if(valToStr(valInt) === targetVal) {
      if(onChange)onChange(valInt, index || 0);
    }
    
    if(inputRef.current) {
      inputRef.current.value = e.target.value;
    }

  }, [inputRef, onChange, index, hex, strToVal, valToStr]);

  const inputBlur = useCallback(() => {
    if(inputRef.current) {
      // loosing focus should always put the input in a valid state
      if(valToStr(strToVal(inputRef.current.value)) !== inputRef.current.value) {
        inputRef.current.value = valToStr(value);
      }
    }
  }, [inputRef, value, valToStr, strToVal]);

  return <InputWrapper>
      {(props.name.length>0) && <InputLabel>{props.name}</InputLabel>}
      <InputElem 
        type={hex ? "text" : "number"}
        placeholder={props.name}
        ref={inputRef}
        onChange={inputChange}
        onBlur={inputBlur}
        readOnly={readOnly}
      />
      {hex && <HexInfo>(hex)</HexInfo>}
    </InputWrapper>;
};

export const IntInput = React.memo(IntInputComponent, (prev, next) => {
  return prev.name === next.name 
    && prev.value === next.value
    && prev.index === next.index
    && prev.hex === next.hex
    && prev.onChange === next.onChange;
});