/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { ListContainer } from 'frontend/elements/ListElements';
import { ListHeader } from 'frontend/elements/ListHeader';
import React, { ComponentType, useState } from 'react';
import { ChevronDown, ChevronUp, IconProps } from 'react-bootstrap-icons';

interface ListAccordionProps {
    text: string;
    Icon: ComponentType<IconProps>;
    iconColor?: string;
    children: React.ReactNode;
    defaultClose?: boolean;
};

const ListAccordionComponent = (props: ListAccordionProps) => {

  const [open, setOpen] = useState(!props.defaultClose);

  return (
      <ListContainer style={{flexGrow: 1, flexShrink: 1}}>
        <div onClick={() => setOpen(!open)} style={{cursor: 'pointer'}}>
            <ListHeader text={props.text} Icon={props.Icon} 
              IconRight={open ? ChevronUp : ChevronDown} 
              iconColor={props.iconColor}
            />
        </div>
        {open &&  props.children}
        </ListContainer>
    
  );
}

export const ListAccordion = React.memo(ListAccordionComponent, (prev, next) => {
  return prev.text === next.text &&
    prev.Icon === next.Icon &&
    prev.iconColor === next.iconColor &&
    prev.children === next.children;
});