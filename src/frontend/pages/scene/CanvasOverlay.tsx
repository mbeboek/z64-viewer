/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useEffect } from 'react';
import styled from 'styled-components';

import { Triangle, Water, Box, Pentagon, DoorClosed, Signpost, CircleSquare, BoundingBox } from 'react-bootstrap-icons';

import { LogOutput } from './barRight/LogOutput';
import { IconChip } from 'frontend/elements/IconChip';
import { useCallbackState } from 'frontend/helper/state';

const Container = styled.div`
  padding-left: 3px;
  display: flex;
  justify-content: space-between;
`;

export interface ObjectSettings {
  water: boolean,
  sceneActor: boolean,
  roomActors: boolean,
  roomActorsBoxes: boolean,
  collision: boolean,
  roomMesh: boolean,
  collider: boolean,
  paths: boolean;
};

export const defaultObjectSettings = () => ({
  sceneActor: true,
  roomActors: true,
  roomActorsBoxes: false,
  water: false,
  collision: false,
  roomMesh: true,
  collider: false,
  paths: true,
});

interface CanvasOverlayProps {
  initialSetting: ObjectSettings;
  onChange: (settings: ObjectSettings) => void;
};

const CanvasOverlayComponent = (props: CanvasOverlayProps) => {
  const onChange = props.onChange;

  const [water, toggleWater] = useCallbackState(props.initialSetting.water);
  const [sceneActor, toggleSceneActor] = useCallbackState(props.initialSetting.sceneActor);
  const [collision, toggleCollision] = useCallbackState(props.initialSetting.collision);
  const [roomMesh, toggleRoomMesh] = useCallbackState(props.initialSetting.roomMesh);
  const [roomActors, toggleRoomActors] = useCallbackState(props.initialSetting.roomActors);
  const [roomActorsBoxes, toggleRoomActorsBoxes] = useCallbackState(props.initialSetting.roomActorsBoxes);
  const [collider, toggleCollider] = useCallbackState(props.initialSetting.collider);
  const [paths, togglePaths] = useCallbackState(props.initialSetting.paths);

  useEffect(() => {
    const settings: ObjectSettings = {water, sceneActor, roomActors, roomActorsBoxes, collision, roomMesh, collider, paths};
    onChange(settings);
  }, [onChange, water, sceneActor, roomActors, roomActorsBoxes, collision, roomMesh, collider, paths]);

  return (
    <Container>
      <div>
       
        <IconChip
          text="Scene-Actors"
          onClick={toggleSceneActor}
          active={sceneActor}
          Icon={DoorClosed}
        />

        <IconChip
          text="Actors-Models"
          onClick={toggleRoomActors}
          active={roomActors}
          Icon={Signpost}
        />

        <IconChip
          text="Actors-Boxes"
          onClick={toggleRoomActorsBoxes}
          active={roomActorsBoxes}
          Icon={Box}
        />

        <IconChip
          text="Actor-Collider"
          onClick={toggleCollider}
          active={collider}
          Icon={CircleSquare}
        />

        <IconChip
          text="Paths"
          onClick={togglePaths}
          active={paths}
          Icon={BoundingBox}
        />

        <div style={{marginTop: 15}}></div>

        <IconChip
          text="Collision"
          onClick={toggleCollision}
          active={collision}
          Icon={Triangle}
        />
        
        <IconChip
          text="Mesh"
          onClick={toggleRoomMesh}
          active={roomMesh}
          Icon={Pentagon}
        />

        <IconChip
          text="Water"
          onClick={toggleWater}
          active={water}
          Icon={Water}
        />
      </div>

      <LogOutput />
  </Container>
  );
};

export const CanvasOverlay = React.memo(CanvasOverlayComponent, (prev, next) => {
  return prev.onChange === next.onChange;
});