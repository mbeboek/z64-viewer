/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useCallback } from 'react';
import { ArrowsCollapse, BoundingBox, PlusSquare, PlusSquareFill, Trash } from 'react-bootstrap-icons';
import { Context } from '../../../../backend/context';
import { ScenePath } from '../../../../backend/decomp/parser/parsePaths';
import { requestHistoryAdd } from '../../../../backend/editor/history';
import { hslToRGB, rgbToCSS } from '../../../../backend/utils/color';
import { intoToLetterAZ } from '../../../../backend/utils/math';
import { patchPathAdd, patchPathAddPoint, patchPathDelPoint, patchPathPos } from '../../../../backend/z64/path/pathState';
import { IconButton, SmallIconButtonDark } from '../../../elements/IconButton';
import { IntInput } from '../../../elements/IntInput';
import { ListAccordion } from '../../../elements/ListAccordion';
import { ListItem } from '../../../elements/ListElements';
import { getMousePosition3D } from '../../../renderer/Raycast';

interface Props {
    paths: ScenePath[],
    onChange: (newPaths: ScenePath[]) => void;
};

const ScenePathsComponent = (props: Props) => {
  const {paths, onChange} = props;

  const setPathAttribute = useCallback((pathIdx: number, name: string, val: number) => {
    const newPaths = [...paths];
    if(newPaths[pathIdx]) {
      newPaths[pathIdx] = {...newPaths[pathIdx], [name]: val};
    }
    onChange(newPaths);
    requestHistoryAdd();
  }, [paths, onChange]);

  const setPathPos = useCallback((pathIdx: number, pointIdx: number, axis: number, newValue: number) => {
    onChange(patchPathPos(paths, pathIdx, pointIdx, axis, newValue));
    requestHistoryAdd();
  }, [paths, onChange]);

  const addPath = useCallback(() => {
    onChange(patchPathAdd(paths, getMousePosition3D()));
    requestHistoryAdd();
  }, [paths, onChange]);

  const deletePath = useCallback((pathIdx: number) => {
    onChange(paths.filter((_,i) => i !== pathIdx));
    requestHistoryAdd();
  }, [paths, onChange]);

  const deletePathPoint = useCallback((pathIdx: number, pointIdx: number) => {
    onChange(patchPathDelPoint(paths, pathIdx, pointIdx));
    requestHistoryAdd();
  }, [paths, onChange]);

  const addPathPoint = useCallback((pathIdx: number, pointIdx: number) => {
    onChange(patchPathAddPoint(paths, pathIdx, pointIdx));
    requestHistoryAdd();
  }, [paths, onChange]);

  const closeLoop = useCallback((pathIdx: number) => {
    const path = paths[pathIdx];
    if(!path || path.points.length < 2)return;
    onChange(patchPathAddPoint(paths, pathIdx, 0, path.points.length-1));
    requestHistoryAdd();
  }, [paths, onChange]);

  return (<>
    {paths.map((path, idx) => <ListAccordion 
        key={path.uuid} text={`Path ${intoToLetterAZ(idx)} - (${path.points.length})`} Icon={BoundingBox} 
        defaultClose={idx > 0} 
        iconColor={rgbToCSS(hslToRGB(idx / paths.length))}
      >

      <ListItem style={{paddingRight: 20, justifyContent: 'center'}}> 
        <IconButton text={'Loop'} Icon={ArrowsCollapse} dark onClick={() => closeLoop(idx)} />
        <IconButton text={'Delete'} Icon={Trash} dark onClick={() => deletePath(idx)} />
      </ListItem>

      {Context.isMM() && 
        <ListItem style={{paddingRight: 20}}> 
          <IntInput name={'Value'} value={path.customValue} onChange={(v) => setPathAttribute(idx, 'customValue', v)} />
          <IntInput name={'Additional Index'} value={path.additionalPathIdx} onChange={(v) => setPathAttribute(idx, 'additionalPathIdx', v)} />
        </ListItem>
      }

      {path.points.map((point, pidx) => 
        <ListItem key={path.points.length + "_" + pidx}>
            <IntInput index={0} onChange={(v) => setPathPos(idx, pidx, 0, v)} name="" value={point[0]}/>
            <IntInput index={1} onChange={(v) => setPathPos(idx, pidx, 1, v)} name="" value={point[1]}/>
            <IntInput index={2} onChange={(v) => setPathPos(idx, pidx, 2, v)} name="" value={point[2]}/>

            {(path.points.length > 1) && 
              <SmallIconButtonDark title="Delete" onClick={() => deletePathPoint(idx, pidx)}>
                <Trash size={18} style={{minWidth: 30}}/>
              </SmallIconButtonDark>
            }

            <SmallIconButtonDark title="Add" onClick={() => addPathPoint(idx, pidx)}>
              <PlusSquareFill size={18} style={{minWidth: 30}}/>
            </SmallIconButtonDark>
        </ListItem>)
      }
      </ListAccordion>
    )}

    <ListItem style={{display: 'flex', justifyContent: 'center'}}>
      <IconButton text={'Add Path'} Icon={PlusSquare} dark onClick={addPath} />
    </ListItem>

  </>);
}

export const ScenePaths = React.memo(ScenePathsComponent, (prev, next) => {
    return prev.onChange === next.onChange &&
        prev.paths === next.paths;
});