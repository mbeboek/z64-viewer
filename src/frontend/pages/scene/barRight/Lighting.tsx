/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';
import styled from 'styled-components';

// Components
import { Cell, Table } from 'frontend/elements/Table';
import { Colors } from 'frontend/style/colors';

// Backend
import { SceneLighting } from 'backend/z64/lighting/lighting';
import { VectorRGBFloat } from 'backend/z64/types';

const LightTable = styled(Table)`
    width: 100%;

    tr th {
        font-weight: normal;
    }

    & tr td:first-child, & tr th:first-child {
        padding-left: 20px;
    }

    & tr td:last-child, & tr th:last-child {
        padding-right: 20px;
    }

    & tr:hover {
        background-color: #777777;
    }

    & tr td {
        border: 0;
        height: 18px;
        padding: 6px 8px;
    }
`;

const ColorElem = styled.div`
    width: 60px;
    height: 26px;
    border-radius: 6px;
    border: solid 2px rgba(0,0,0, 0.4);
    cursor: pointer;
`;

const colorToHex = (color: VectorRGBFloat): string => {
    return "#" 
        + (color[0]).toString(16).padStart(2, '0')
        + (color[1]).toString(16).padStart(2, '0')
        + (color[2]).toString(16).padStart(2, '0');
};

interface LightingComponentProps {
    sceneLightings: SceneLighting[],
    lightingIndex: number,
    setLightingIndex: (idx: number) => void,
};

const LightingComponent = (props: LightingComponentProps) => {

  return (
    <LightTable>
        <thead>
          <tr>
            <th><Cell>ID</Cell></th>
            <th align="center"><Cell>Ambient</Cell></th>
            <th align="center"><Cell>Light-A</Cell></th>
            <th align="center"><Cell>Light-B</Cell></th>
          </tr>
        </thead>

        <tbody>
        {props.sceneLightings.map((light, i) => (
            <tr 
                style={(i === props.lightingIndex) ? {
                    backgroundColor: Colors.PRIMARY_LIGHT
                } : {}}
                key={i} 
                onClick={() => props.setLightingIndex(i)}
            >
                <td><Cell>{i}</Cell></td>
                <td><Cell> <ColorElem style={{backgroundColor: colorToHex(light.ambientColor)}}>&nbsp;</ColorElem></Cell></td>
                <td><Cell> <ColorElem style={{backgroundColor: colorToHex(light.diff0Color)}}>&nbsp;</ColorElem></Cell></td>
                <td><Cell> <ColorElem style={{backgroundColor: colorToHex(light.diff1Color)}}>&nbsp;</ColorElem></Cell></td>
            </tr >
        ))}
        </tbody>
    </LightTable>
  );
}

export const Lighting = React.memo(LightingComponent, (prev, next) => {
    return prev.sceneLightings === next.sceneLightings &&
        prev.lightingIndex === next.lightingIndex &&
        prev.setLightingIndex === next.setLightingIndex;
});