/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';

// Data
import { GenericActor } from '../../../../backend/z64/actors/actorList';

// Components
import { ListContainer, ListItemFullWidth } from 'frontend/elements/ListElements';
import { ActorDataEntry, ActorDataParam, ActorDataParamTarget } from '../../../../backend/z64/actors/actorDataType';
import { IntInput } from '../../../elements/IntInput';
import { Checkbox } from '../../../elements/Checkbox';
import { SelectBoxSearch } from '../../../elements/SelectBoxSearch';
import { getGameChestData, getGameCollectData } from '../../../../backend/z64/actors/actorData';

type PatchFunction = (newActor: GenericActor) => void;

function getParamValue(actor: GenericActor, target: ActorDataParamTarget) {
  switch(target) {
    default:
    case 'params': return actor.initValue;
    case 'xrot': return actor.rot[0];
    case 'yrot': return actor.rot[1];
    case 'zrot': return actor.rot[2];
  }
}

function patchParamValue(actor: GenericActor, target: ActorDataParamTarget, newVal: number) {
  switch(target) {
    default:
    case 'params': actor.initValue = newVal; break;
    case 'xrot': actor.rot[0] = newVal; break;
    case 'yrot': actor.rot[1] = newVal; break;
    case 'zrot': actor.rot[2] = newVal; break;
  }
  return actor;
}

function getParamComponent(param: ActorDataParam, actor: GenericActor, patch: PatchFunction)
{
  const collectibleData = getGameCollectData();
  const chestData = getGameChestData();

  // Extract values and apply shifts and masks
  const values = param.values || [];
  const targetValue = getParamValue(actor, param.target);
  const maskedValue = targetValue & param.mask;
  const shift = param.shift || 0;
  const maskedShiftedVal = maskedValue >> shift;

  // Handle changes
  const onChange = (val: string|number) => {
    let newVal = targetValue & ~param.mask;
    newVal |= ((parseInt(val.toString()) || 0) << shift) & param.mask;

    const newActor = patchParamValue(structuredClone(actor), param.target, newVal);
    patch(newActor);
  };

  // Render UI
  switch(param.type) 
  {
    case 'type':
      return <SelectBoxSearch name={param.name} value={maskedShiftedVal+""} options={values} onChange={onChange}></SelectBoxSearch>;

    case 'enum':
      return <SelectBoxSearch name={param.name} value={maskedShiftedVal+""} options={values} onChange={onChange}></SelectBoxSearch>;

    case 'number':
      return <IntInput name={param.name} value={maskedShiftedVal} onChange={onChange}></IntInput>;

    case 'bool':
      return <Checkbox name={param.name} checked={!!maskedShiftedVal} value={0xFFFF} onChange={onChange}></Checkbox>;

    case 'flag':
      return <IntInput name={param.name} value={maskedShiftedVal} onChange={onChange}></IntInput>;

    case 'collectible':
      return <SelectBoxSearch name={param.name} value={maskedShiftedVal+""} options={collectibleData} onChange={onChange}></SelectBoxSearch>;

    case 'chest':
      return <SelectBoxSearch name={param.name} value={maskedShiftedVal+""} options={chestData} onChange={onChange}></SelectBoxSearch>;

    default:
      console.error("Unknown param type", param);
  }
}

interface Props {
  entry: ActorDataEntry;
  actor: GenericActor;
  patchActor: (newActor: GenericActor) => void;
};

const ActorParamsComponent = (props: Props) =>  
{
  const {entry, actor, patchActor} = props;

  return <ListContainer style={{paddingTop: 0}}>{entry.params.map(
    (param, i) => <ListItemFullWidth key={i}>
      {getParamComponent(param, actor, patchActor)}
    </ListItemFullWidth>
  )}</ListContainer>;
};

export const ActorParams = React.memo(ActorParamsComponent, (prev, next) => {
  return prev.entry === next.entry
    && prev.actor === next.actor
    && prev.patchActor === next.patchActor;
});