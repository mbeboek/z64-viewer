/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useCallback, useState } from 'react';

// Components
import { ListContainer, ListItem, ListItemText } from 'frontend/elements/ListElements';

// Icons
import { BoxFill, Pause, Play, Stop } from 'react-bootstrap-icons';

// Backend
import { GenericActor, SceneActor } from 'backend/z64/actors/actorList';
import { Input } from 'frontend/elements/Input';
import { ListHeader } from 'frontend/elements/ListHeader';
import { IconToggle } from 'frontend/elements/IconToggle';
import { ActorEmuData, EmuCommandType } from 'backend/z64/emulator/actorEmuBridge';
import { useEffect } from 'react';
import { BusName, EventBus } from 'frontend/helper/eventBus';
import { ActorFlags } from 'backend/z64/actors/actorFlags';
import { SelectBox } from 'frontend/elements/SelectBox';
import { GpuTexture } from 'backend/z64/gpu/data/texture';
import { Textures } from './Textures';
import { IntInput } from '../../../elements/IntInput';
import { patchPosIdx, patchRotIdx } from '../../../../backend/z64/actors/actorState';
import { actorTable } from '../../../../backend/decomp/actorTable';
import { ActorParams } from './ActorParams';
import { preventDefault } from '../../../helper/events';
import { getGameActorData } from '../../../../backend/z64/actors/actorData';
import { Context } from '../../../../backend/context';
import { MM_ROT_MASK_X, MM_ROT_MASK_Y, MM_ROT_MASK_Z } from '../../../../backend/decomp/rooms';
import { InputCheck } from '../../../elements/Checkbox';
import { DayList } from '../barLeft/DayList';
import { ActorType } from '../../../../backend/z64/types';
import { loopUInt } from '../../../../backend/utils/math';
import { useEventBus } from '../../../helper/useEventBus';
import { useEventListener } from '../../../helper/useEventListener';
import { requestHistoryAdd } from '../../../../backend/editor/history';

const FPS_OPTIONS: Array<[string,string]> = [
  ["5", "5"],
  ["15", "15"],
  ["20", "20"],
  ["30", "30"],
  ["60", "60"],
];

interface SelectedActorProps {
  actor: GenericActor;
  onChange: (actors: GenericActor[]) => void;
};

const SelectedActorComponent = (props: SelectedActorProps) =>  
{
    const {onChange, actor} = props;
    const sceneActor = actor as SceneActor;
    const actorUUID = actor.uuid;

    const actorData = getGameActorData();

    const [emuRunning, setEmuRunning] = useState(false);
    const [emuData, setEmuData] = useState<ActorEmuData>();
    const [emuFps, setEmuFps] = useState(20);
    const [mmDay, setMMDay] = useState(1);
    const [mmTime, setMMTime] = useState(0);
    const [textures, setTextures] = useState<GpuTexture[]>([]);

    const setFpsSelect = useCallback((val: string) => setEmuFps(parseInt(val)), [setEmuFps]);
    const setMMDaySelect = useCallback((val: string) => setMMDay(parseInt(val)), [setMMDay]);

    const onActorEmuData = useCallback((data: ActorEmuData) => setEmuData(data), [setEmuData]);
    const onActorTexture = useCallback((data: GpuTexture[]) => setTextures(data), [setTextures]);

    const onChangeWithHistory = useCallback((actors: GenericActor[]) => {
      onChange(actors);
      requestHistoryAdd();
    }, [onChange]);

    const stopEmu = useCallback(() => {
      setEmuRunning(false);
      setEmuData(undefined);
      EventBus.send(BusName.ACTOR_EMU, {type: EmuCommandType.STOP, actorUUID});
    }, [setEmuRunning, actorUUID]);

    const toggleEmu = useCallback(() => {
      EventBus.send(BusName.ACTOR_EMU, {
        type: !emuRunning ? EmuCommandType.START : EmuCommandType.PAUSE,
        actorUUID
      });

      setEmuRunning(!emuRunning);
    }, [setEmuRunning, emuRunning, actorUUID]);

    // Stop emulation when selecting a new actor
    useEffect(() => {
      stopEmu();
      setTextures([]);
    }, [actorUUID, stopEmu, setTextures]);

    useEventBus(BusName.ACTOR_EMU_DATA, onActorEmuData);
    useEventBus(BusName.ACTOR_TEXTURES, onActorTexture);

    const patchActor = useCallback((newActor: GenericActor) => {
      if(emuRunning) {
        EventBus.send(BusName.ACTOR_EMU, {type: EmuCommandType.STOP, actorUUID});
        onChangeWithHistory([newActor]);  
        setTimeout(() => {
          EventBus.send(BusName.ACTOR_EMU, {type: EmuCommandType.START, actorUUID});
        }, 50);
      } else {
        stopEmu();
        onChangeWithHistory([newActor]);  
      }
    }, [onChangeWithHistory, stopEmu, emuRunning, actorUUID]);

    // Scoped modifier functions
    const onChangeParam = useCallback((initValue: number) => onChangeWithHistory([{...actor, initValue}]), [onChangeWithHistory, actor]);
    const onChangePos = useCallback((val: number, idx: number) => onChangeWithHistory([patchPosIdx(actor, idx, val)]), [onChangeWithHistory, actor]);
    const onChangeRot = useCallback((val: number, idx: number) => onChangeWithHistory([patchRotIdx(actor, idx, val)]), [onChangeWithHistory, actor]);
    
    const onChangeRoomFrom = useCallback((val: number) => onChangeWithHistory([{...actor, backRoom: val}]), [onChangeWithHistory, actor]);
    const onChangeRoomTo   = useCallback((val: number) => onChangeWithHistory([{...actor, targetRoom: val}]), [onChangeWithHistory, actor]);

    const onChangeCamFrom = useCallback((val: number) => onChangeWithHistory([{...actor, backCamConf: val}]), [onChangeWithHistory, actor]);
    const onChangeCamTo   = useCallback((val: number) => onChangeWithHistory([{...actor, targetCamConf: val}]), [onChangeWithHistory, actor]);

    const onChangeRotMask = useCallback((singleMask: number, set: boolean) => 
      onChangeWithHistory([{
        ...actor, 
        rotMask: set ? (actor.rotMask | singleMask) : (actor.rotMask & (~singleMask))
      }]), 
    [onChangeWithHistory, actor]);

    const onChangeSpawnMask = useCallback((spawnTime: number) => 
      onChangeWithHistory([{...actor, spawnTime}]),
    [onChangeWithHistory, actor]);

    const onChangeCutscene = useCallback((val: number) => 
      onChangeWithHistory([{...actor, cutsceneIndex: loopUInt(val, 127)}]), 
    [onChangeWithHistory, actor]);

    useEffect(() => {
      EventBus.send(BusName.ACTOR_EMU, {type: EmuCommandType.FPS_SET, value: emuFps});
    }, [emuFps]);

    useEffect(() => {
      EventBus.send(BusName.ACTOR_EMU, {type: EmuCommandType.MM_DAY, value: mmDay});
      EventBus.send(BusName.ACTOR_EMU, {type: EmuCommandType.MM_TIME, value: mmTime * 0x1000});

      if(emuRunning) {
        EventBus.send(BusName.ACTOR_EMU, {type: EmuCommandType.STOP, actorUUID});
        setTimeout(() => {
          EventBus.send(BusName.ACTOR_EMU, {type: EmuCommandType.START, actorUUID});
        }, 50);
      } else {
        stopEmu();
      }
    }, [mmDay, mmTime,  actorUUID, emuRunning, stopEmu]);

    const handleKey = useCallback((ev: Event) => {
      const e = ev as KeyboardEvent; // typescript being stupid
      if(!e.ctrlKey && e.code === "F8")toggleEmu();
      preventDefault(e);
    }, [toggleEmu]);
  
    useEventListener('keydown', handleKey);

    const actorEnumName = actorTable.list[actor.id || 0]?.name || "";
    const actorEntry = actorData[actorEnumName];
    const actorName = actorEntry?.name || actorEnumName || actor.id.toString();
    
    return (
        <ListContainer>
            <ListHeader text={actorName} Icon={BoxFill} />
            <ListItem>
                <IntInput name="ID" value={actor.id} hex={true} />
                <IntInput 
                  onChange={onChangeParam} hex={true} 
                  name="Param" value={actor.initValue}/>
            </ListItem>
            <ListItem>
                <IntInput index={0} onChange={onChangePos} name="Pos" value={actor.pos[0]}/>
                <IntInput index={1} onChange={onChangePos} name=""     value={actor.pos[1]}/>
                <IntInput index={2} onChange={onChangePos} name=""     value={actor.pos[2]}/>
            </ListItem>
            <ListItem>
                <IntInput index={0} onChange={onChangeRot} name="Rot" value={actor.rot[0]}/>
                <IntInput index={1} onChange={onChangeRot} name=""     value={actor.rot[1]}/>
                <IntInput index={2} onChange={onChangeRot} name=""     value={actor.rot[2]}/>
            </ListItem>

            {
              (actor.type === ActorType.SCENE) && 
              <>
                <ListItemText>Transition</ListItemText>
                <ListItem>
                  <IntInput index={0} onChange={onChangeRoomFrom} name="Room" value={sceneActor.backRoom}/>
                  <IntInput index={1} onChange={onChangeRoomTo} name="To"     value={sceneActor.targetRoom}/>
                </ListItem>
                <ListItem>
                  <IntInput index={0} onChange={onChangeCamFrom} name="Camera" value={sceneActor.backCamConf}/>
                  <IntInput index={1} onChange={onChangeCamTo} name="To"       value={sceneActor.targetCamConf}/>
                </ListItem>
              </>
            }

            {
              (Context.isMM() && actor.type === ActorType.ROOM) && 
              <>
                <ListItem>
                  <div style={{flexGrow: 2, width: 150}}>Convert Rotations (XYZ)</div>
                  <InputCheck type="checkbox" checked={!!(actor.rotMask & MM_ROT_MASK_X)} onChange={e => onChangeRotMask(MM_ROT_MASK_X, e.currentTarget.checked)} />
                  <InputCheck type="checkbox" checked={!!(actor.rotMask & MM_ROT_MASK_Y)} onChange={e => onChangeRotMask(MM_ROT_MASK_Y, e.currentTarget.checked)} />
                  <InputCheck type="checkbox" checked={!!(actor.rotMask & MM_ROT_MASK_Z)} onChange={e => onChangeRotMask(MM_ROT_MASK_Z, e.currentTarget.checked)} />
                </ListItem>

                <ListItem>
                  <div style={{flexGrow: 2, width: 180}}>Actor-Cutscene</div>
                  <IntInput name={"Index"} value={actor.cutsceneIndex} onChange={onChangeCutscene}></IntInput>
                </ListItem>

                <ListItemText>Spawn-Time</ListItemText>
                <div style={{width: 280}}>
                  <DayList dayMask={actor.spawnTime} onChange={onChangeSpawnMask} />
                </div>
              </>
            }

            {(actorEntry && actorEntry.params.length > 0) && <>
                <ListItemText>Parameters</ListItemText>
                <ActorParams 
                  entry={actorEntry} 
                  actor={actor}
                  patchActor={patchActor}
                />
              </>
            }

            <ListItemText>Emulation</ListItemText>

            <ListItem>
              <IconToggle Icon={emuRunning ? Pause : Play} onClick={toggleEmu} active={emuRunning}  />
              <IconToggle Icon={Stop}  onClick={stopEmu} active={false} />
              
              <div style={{marginRight: 15}}> </div>
              
              <SelectBox name="FPS" options={FPS_OPTIONS} value={emuFps.toString()} onChange={setFpsSelect} />
            </ListItem>
            
            {
              Context.isMM() && <ListItem>
                <IntInput name={'Time (hours)'} value={mmTime} onChange={setMMTime} />

                <div style={{marginRight: 6}}> </div>

                <SelectBox name="Day" options={[
                  ["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"]
                ]} value={mmDay.toString()} onChange={setMMDaySelect} />
              </ListItem>
            }

            {textures && <Textures textures={textures} />}

            {emuData && 
                <>
                    <ListItem>
                      Emulated Data
                    </ListItem>
                    <ListItem>
                      <Input readOnly name="Pos" value={emuData.pos[0]}/>
                      <Input readOnly name=""     value={emuData.pos[1]}/>
                      <Input readOnly name=""     value={emuData.pos[2]}/>
                    </ListItem>
                    <ListItem>
                      <Input readOnly name="Scale" value={emuData.scale[0]}/>
                      <Input readOnly name="" value={emuData.scale[1]}/>
                      <Input readOnly name="" value={emuData.scale[2]}/>
                    </ListItem>
                    <ListItem>
                      <Input readOnly name="Veloc." value={emuData.velocity[0]}/>
                      <Input readOnly name="" value={emuData.velocity[1]}/>
                      <Input readOnly name="" value={emuData.velocity[2]}/>
                    </ListItem>
                    <ListItem>
                      <Input readOnly name="Speed" value={emuData.speed}/>
                      <Input readOnly name="Gravity" value={emuData.gravity}/>
                      <Input readOnly name="Min-VY" value={emuData.minVelocityY}/>
                    </ListItem>
                    <ListItem>
                      <Input readOnly name="Flags" value={emuData.flags.toString(16).padStart(8, '0')}/>
                      <Input readOnly name="Hidden (Lens)" value={emuData.flags & ActorFlags.INVISIBLE ? "Yes" : "No"}/>
                    </ListItem>
                </>
            }
        </ListContainer>
    );
};

export const SelectedActor = React.memo(SelectedActorComponent, (prev, next) => {
  return prev.actor.uuid === next.actor.uuid &&
    prev.actor.initValue === next.actor.initValue &&
    prev.actor.rotMask === next.actor.rotMask &&
    prev.actor.pos[0] === next.actor.pos[0] &&
    prev.actor.pos[1] === next.actor.pos[1] &&
    prev.actor.pos[2] === next.actor.pos[2] &&
    prev.actor.rot[0] === next.actor.rot[0] &&
    prev.actor.rot[1] === next.actor.rot[1] &&
    prev.actor.rot[2] === next.actor.rot[2] &&
    prev.onChange === next.onChange;
});