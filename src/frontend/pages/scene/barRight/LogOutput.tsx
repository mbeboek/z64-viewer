/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

// Components
import React, { useCallback, useRef, useState } from 'react';
import styled from 'styled-components';
import { Button } from '../../../elements/Button';

// Backend
import { SelectBox } from 'frontend/elements/SelectBox';
import { BusName } from 'frontend/helper/eventBus';
import { logTextToHtml } from '../../../../backend/z64/emulator/printf';
import { useEventBus } from '../../../helper/useEventBus';

const Container = styled.div`
    width: 600px;
    bottom: 0;
    height: 250px;
    padding: 4px;
    position: fixed;
    font-size: 12px;
    font-family: Japanese, Roboto;
    border-radius: 8px;
    pointer-events: auto;
    background-color: rgba(0,0,0,0.7);

    transition: height 50ms linear 0ms,
                width 50ms linear 0ms;
`;

const HeaderContainer = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
`;

const LogContainer = styled.pre`
    height: 190px;
    overflow: auto;
    margin-top: 2px;
`;

const LOG_MAX_LEN = 6000;

function appendLog(old: string, str: string): string
{
  const newStr = old + str;
  if(newStr.length > LOG_MAX_LEN) {
    return newStr.substring(newStr.length - LOG_MAX_LEN - 500);
  }
  return newStr;
}

export const LogOutputComponent = () =>  {
    const [type, setType] = useState("stdout");
    //const [type, setType] = useState("n64");

    const logN64 = useRef("");
    const logStdout = useRef("");

    const [rerenderId, setRerender] = useState(1);
    const [logOpen, setLogOpen] = useState(false);
    const preRef = useRef<HTMLPreElement>(null);

    const triggerRender = useCallback(() => {
      if(logOpen) {
        setRerender(+new Date());
        if(preRef.current) {
          preRef.current.scrollTop = preRef.current.scrollHeight;
        }
      }
    }, [logOpen, setRerender]);

    const clear = useCallback(() => {
      logN64.current = "";
      logStdout.current = "";
      triggerRender();
    }, [triggerRender]);

    const onStdout = useCallback((str: string) => {
      logStdout.current = appendLog(logStdout.current, str); 
      triggerRender();
    }, [triggerRender]);

    const onN64Log = useCallback((str: string) => {
      logN64.current = appendLog(logN64.current, str); 
      triggerRender();
    }, [triggerRender]);

    const onLogPanel = useCallback((str: any) => {
      setLogOpen(!!str);
    }, [setLogOpen]);

    useEventBus(BusName.STDOUT, onStdout);
    useEventBus(BusName.N64_LOG, onN64Log);
    useEventBus(BusName.LOG_PANEL, onLogPanel);

    return (
        <Container style={logOpen ? {} : {height: 44, width: 290}}>
            <HeaderContainer>
                <Button monochrome small text={logOpen ? "Hide" : "Show"}
                    onClick={() => setLogOpen(!logOpen) }
                />
                <Button monochrome small text="Clear"
                    onClick={clear}
                />

                <SelectBox small
                  name="Channel" options={[
                  ["n64", "osSyncPrintf()"],
                  ["stdout", "StdOut"],
                ]} value={type} onChange={setType} />
            
            </HeaderContainer>
            
            {logOpen && 
              <LogContainer
                  ref={preRef}
                  data-dummy={rerenderId}
                  dangerouslySetInnerHTML={{
                    __html: type === 'n64' ? logTextToHtml(logN64.current) : logStdout.current
                  }}
              />
            }
        </Container>
    );
};

export const LogOutput = React.memo(LogOutputComponent, (prev, next) => {
    return true;
});