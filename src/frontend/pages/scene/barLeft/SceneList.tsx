/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';
import styled from 'styled-components';
import { Colors } from 'frontend/style/colors';

// Backend
import { SceneListEntry } from '../../../../backend/z64/scenes/sceneList';
import { sceneNamesMM, sceneNamesOOT } from '../../../../backend/z64/scenes/sceneNames';
import { SceneListEntry as SceneListEntryComp } from './SceneListEntry';
import { Context, Game } from '../../../../backend/context';

const SceneRow = styled.div`
  min-width: 300px;
  cursor: pointer;
  &:hover {
    background-color: ${Colors.BG_DARK};
  }
`;

interface SceneListProps {
    scenes: SceneListEntry[];
    selectedId: number;
    searchText?: string;
    onSelect: (id: number) => void;
};

const SceneListComponent = (props: SceneListProps) => {
  let scenes = props.scenes;
  if(props.searchText) 
  {
    const nameList = Context.game === Game.OOT ? sceneNamesOOT : sceneNamesMM;
    const searchLower = props.searchText.toLowerCase();
    scenes = scenes.filter((scene) => {
      const nameLower = (nameList[scene.sceneId] || scene.sceneId+"").toLowerCase();
      const pathLower = scene.decompData.path.substring(14);
      return nameLower.includes(searchLower) || pathLower.includes(searchLower);
    });
  }

  scenes = scenes.filter(scene => scene.offsetStart);

  return (
    <React.Fragment>
    {
        scenes.map(scene => <SceneListEntryComp 
          key={scene.sceneId} 
          id={scene.sceneId}
          path={scene.decompData.path.substring(7)}
          isActive={scene.sceneId === props.selectedId}
          onSelect={props.onSelect}
        />)
    }
    {
      scenes.length === 0 && (
        <SceneRow style={{textAlign: 'center', marginTop: 5}}>No Results</SceneRow>
      )
    }
    </React.Fragment>
    
  );
};

export const SceneList = React.memo(SceneListComponent, (prev, next) => {
  // shortcut: only check the array length, since it changes reference each time
  return prev.scenes.length === next.scenes.length 
    && prev.searchText === next.searchText
    && prev.selectedId === next.selectedId
    && prev.onSelect === next.onSelect;
});