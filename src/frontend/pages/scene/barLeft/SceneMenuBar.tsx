/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useRef, useState } from 'react';

// Components
import styled from 'styled-components';
import { Input } from 'frontend/elements/Input';
import { ListAccordion } from 'frontend/elements/ListAccordion';
import { ListOl } from 'react-bootstrap-icons';

// Backend
import { SceneListEntry } from '../../../../backend/z64/scenes/sceneList';
import { SceneList } from './SceneList';

const SCENE_ENTRY_HEIGHT = 42;

export const SearchListEntry = styled.div/*_css*/`
  padding: 6px;
  padding-left: 10px;
  width: 290px;
`;

export const SearchListContainer = styled.div/*_css*/`
  padding-top: 0; 
  overflow-y: scroll; 
  overflow-x: hidden;
  height: calc(100vh - 550px);
`;

interface SceneMenuBarProps {
  scenes: SceneListEntry[],
  selectedSceneId: number,
  setSelectedSceneId: (id: number) => void,
};

const SceneMenuBarComponent = (props: SceneMenuBarProps) => {
  const [sceneFilter, setSceneFilter] = useState("");
  const resultList = useRef<HTMLDivElement>(null);
  
  // Scroll the item into view if it is too far away
  if(resultList.current) {
    const list = resultList.current;
    const scrollTarget = SCENE_ENTRY_HEIGHT * (props.selectedSceneId - 2);
    const scrollMin = scrollTarget - (SCENE_ENTRY_HEIGHT*6);
    const scrollMax = scrollTarget + (SCENE_ENTRY_HEIGHT*2);

    if(list.scrollTop < scrollMin || list.scrollTop > scrollMax) {
      setTimeout(() => { list.scrollTop = scrollTarget; }, 1);
    }
  }

  return (
    <ListAccordion text="Scene-Table" Icon={ListOl}>  
      <SearchListEntry>
          <Input 
            value={sceneFilter} 
            name="Filter"
            onChange={(e: any) => setSceneFilter(e.target.value)}  
          />
      </SearchListEntry>
      <SearchListContainer ref={resultList}>
          <SceneList
              scenes={props.scenes}
              selectedId={props.selectedSceneId}
              onSelect={props.setSelectedSceneId}
              searchText={sceneFilter}
          />
      </SearchListContainer>
  </ListAccordion>
  );
};

export const SceneMenuBar = React.memo(SceneMenuBarComponent, (prev, next) => {
  // shortcut: only check the array length, since it changes reference each time
  return prev.selectedSceneId === next.selectedSceneId
    && prev.scenes.length === next.scenes.length
    && prev.setSelectedSceneId === next.setSelectedSceneId;
});
