/**
* @copyright 2022 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import styled from "styled-components";
import React from 'react';
import { Colors } from "frontend/style/colors";

const AltContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  pointer-events: auto;
  padding: 6px 5px 0 10px;
`;

const AltButton = styled.span`
  cursor: pointer;
  user-select: none;

  width: 30px;
  height: 30px;
  margin: 2px;
  display: inline-block;
  text-align: center;
  padding-top: 6px;
  border-radius: 7px;

  color: rgba(255,255,255, 0.6);
  background-color: rgba(0,0,0, 0.25);


  transition: 
    color 70ms linear 0ms,
    opacity 50ms ease 0ms,
    background-color 70ms linear 0ms;

  &:hover {
    _color: white;
    _background-color: rgba(0,0,0, 0.15);
    opacity: 0.8;
  }
`;

const styleSelected = {
  color: '#111',
  backgroundColor: Colors.PRIMARY_LIGHT,
};

interface AltSceneProps {
  altIds: Array<number|undefined>;
  altAltSceneIdx: number;
  onChange: (altIdx: number) => void;
};

const AltSceneListComponent = (props: AltSceneProps) => {

  const {altAltSceneIdx, onChange, altIds} = props;

  const idsFilled = [] as number[];
  for(const id of altIds) {
    idsFilled.push(id === undefined ? (idsFilled[idsFilled.length-1] || -1) : id);
  }

  return <div>
  <AltContainer>
    <AltButton onClick={() => onChange(-1)}
      style={(altAltSceneIdx === -1) ? styleSelected : {}}
    >⊘
    </AltButton>
    {
      idsFilled.map((id, i) => {
        return (id === undefined ? <span>-</span> :
        <AltButton 
          key={i}
          style={(altAltSceneIdx === i) ? styleSelected : {}}
          onClick={() => onChange(i)}
        >
          {id !== i ? "←" : id.toString(10 + 26).toUpperCase()}
        </AltButton>)
      })
    }
  </AltContainer>
</div>;
};

export const AltSceneList = React.memo(AltSceneListComponent, (prev, next) => {
  return prev.altAltSceneIdx === next.altAltSceneIdx &&
    prev.altIds.join("_") === next.altIds.join("_") &&
    prev.onChange === next.onChange;
});