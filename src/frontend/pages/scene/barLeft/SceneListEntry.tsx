/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';
import styled from 'styled-components';
import { Colors } from 'frontend/style/colors';

// Backend
import { sceneNamesOOT, sceneNamesMM } from '../../../../backend/z64/scenes/sceneNames';
import { Context, Game } from '../../../../backend/context';

const SceneRow = styled.div`
  min-width: 300px;
  cursor: pointer;
  display: flex;
  align-items: center;
  &:hover {
    background-color: ${Colors.BG_DARK};
  }
`;

const IdIcon = styled.div`
  color: white;
  width: 34px;
  height: 34px;
  margin: 4px 14px 4px 10px;
  display: inline-block;
  text-align: center;
  padding-top: 7px;
  border-radius: 7px;
  background-color: rgba(0,0,0, 0.25);
`;

const NameText = styled.div`
  padding-top: 4px;
  font-size: 14px;
  line-height: 1;
`;

const PathText = styled.div`
  color: ${Colors.MONO_DARK};
  font-size: 12px;
  line-height: 1.4;
`;

const sceneRowSelected = {
  backgroundColor: Colors.PRIMARY_LIGHT,
  color: 'black',

  '&:hover': {
    backgroundColor: Colors.PRIMARY,
  }
};

interface SceneListEntryProps {
    id: number;
    path: string;
    isActive: boolean;
    onSelect: (id: number) => void;
};

const SceneListEntryComponent = (props: SceneListEntryProps) => {
  const nameList = Context.game === Game.OOT ? sceneNamesOOT : sceneNamesMM;
  
  return <SceneRow 
            style={props.isActive ? sceneRowSelected : {}}
            onClick={() => props?.onSelect(props.id)}
            className='menu_list_scene_entry'
          >
              <IdIcon>{
                  props.id.toString(16).toUpperCase().padStart(2, '0')
              }</IdIcon>
              <div>
                <NameText>{nameList[props.id] || props.id}</NameText>
                <PathText>{props.path}</PathText>
              </div>
          </SceneRow>;
};

export const SceneListEntry = React.memo(SceneListEntryComponent, (prev, next) => {
  return prev.id === next.id
    && prev.isActive === next.isActive
    && prev.onSelect === next.onSelect;
});