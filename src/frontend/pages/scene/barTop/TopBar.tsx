/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { KibakoWebsocket } from 'backend/native/kibakoWebsocket';
import { RoomEntry } from 'backend/z64/rooms/roomList';
import { saveRoom } from 'backend/z64/rooms/roomSave';
import { SceneEntry } from 'backend/z64/scenes/sceneList';
import { saveScene } from 'backend/z64/scenes/sceneSave';
import { showNotification } from 'frontend/layout/Notifications';
import { Colors } from 'frontend/style/colors';
import React, { useCallback, useState } from 'react';
import { ArrowClockwise, ArrowCounterclockwise, Hammer, Play, Stop } from 'react-bootstrap-icons';
import styled from 'styled-components';
import { historyGetState, historyRestore, HistoryRestoreFunction } from '../../../../backend/editor/history';
import { SmallIconButton } from '../../../elements/IconButton';
import { BusName, EventBus } from '../../../helper/eventBus';
import { ctrlAndKey, ctrlShiftAndKey, preventDefault } from '../../../helper/events';
import { useEventBus } from '../../../helper/useEventBus';
import { useEventListener } from '../../../helper/useEventListener';

const Container = styled.div`
  position: absolute;
  left: calc(50% - 100px);
  top: 6px; 
  width: 400;

  display: flex;
  align-items: center;
`;

const IconSpacer = styled.div`
  width: 32px;
`;

const SmallIconText = styled.div`
  border: 1px solid ${Colors.BG_LIGHT};
  background-color: ${Colors.BG_LIGHT};
  border-radius: 5px;
  margin: 0 2px;
  padding: 5px 4px;
  min-width: 32px;
  height: 32px;
  text-align: center;
  user-select: none;
`;

const WideIconText = styled(SmallIconText)`
  min-width: 64px;
  color: #AAA;
`;

async function saveSceneAndRooms(scene: SceneEntry, rooms: RoomEntry[]) {
  if(scene)await saveScene(scene);

  for(const room of rooms) {
      await saveRoom(scene, room);
  }
}

interface TopBarProps {
  scene: SceneEntry;
  rooms: RoomEntry[];
  setSceneRooms: HistoryRestoreFunction;
};

export const TopBarComponent = (props: TopBarProps) =>  
{  
  const {scene, rooms, setSceneRooms} = props;
  const IS_NATIVE = !!KibakoWebsocket.getInstance();
  const [, setForceRerender] = useState(0);

  const [buildActive, setBuildActive] = useState(false);

  const historyState = historyGetState();

  const save = useCallback(async () => 
  {
    try{
      await saveSceneAndRooms(scene, rooms);
      showNotification('success', "Scene saved!");
    } catch(e: any) {
      showNotification('error', "Error saving scene!");
      console.error(e);
    }

  }, [scene, rooms]);

  const buildAndRun = useCallback(async () => 
  {
    if(KibakoWebsocket.instance) {
      EventBus.send(BusName.LOG_PANEL, true);
      await saveSceneAndRooms(scene, rooms);
      showNotification('success', "Scene saved!");
      
      KibakoWebsocket.instance.cmdExecMake("test").then(() => {
        setBuildActive(false);
      }).catch(e => {
        showNotification('error', "Error starting game");
        setBuildActive(false);
      });

      setBuildActive(true);
    }
  }, [scene, rooms, setBuildActive]);

  const build = useCallback(async () => 
  {
    if(KibakoWebsocket.instance) {
      EventBus.send(BusName.LOG_PANEL, true);
      await saveSceneAndRooms(scene, rooms);
      showNotification('success', "Scene saved!");
      
      KibakoWebsocket.instance.cmdExecMake("").then(() => {
        EventBus.send(BusName.LOG_PANEL, false);
        setBuildActive(false);
      }).catch(e => {
        showNotification('error', "Error building game");
        setBuildActive(false);
      });

      setBuildActive(true);
    }
  }, [scene, rooms, setBuildActive]);
  
  const undo = useCallback(() => {
    historyRestore(true, scene, rooms, setSceneRooms);
  }, [scene, rooms, setSceneRooms]);

  const redo = useCallback(() => {
    historyRestore(false, scene, rooms, setSceneRooms);
  }, [scene, rooms, setSceneRooms]);

  const handleKey = useCallback((ev: Event) => {
    const e = ev as KeyboardEvent; // typescript being stupid
    
    if(IS_NATIVE && !e.ctrlKey && e.code === "F9") {
      buildAndRun();
    }

    if(ctrlAndKey(e, "s"))save();
    if(ctrlAndKey(e, 'z'))undo();
    if(ctrlAndKey(e, 'y') || ctrlShiftAndKey(e, 'z'))redo();

    preventDefault(e);
  }, [save, buildAndRun, undo, redo, IS_NATIVE]);

  useEventListener('keydown', handleKey);

  // histroy changes might be async (update after a scene/room change), force re-render here
  const rerender = useCallback(() => setForceRerender(Math.random()), [setForceRerender]);
  useEventBus(BusName.HISTOR_CHANGED, rerender);

  const BuildIcon = buildActive ? Stop : Play;

  return <Container>

    <SmallIconButton onClick={undo} title="Undo [Ctrl+Z]" >
      <ArrowCounterclockwise size={22}/>
    </SmallIconButton>

    <SmallIconButton onClick={redo} title="Redo [Ctrl+Y]" >
      <ArrowClockwise size={22} />
    </SmallIconButton>

    <WideIconText title="Position / History size">
      {historyState.current}/{historyState.total}
    </WideIconText>

    <IconSpacer></IconSpacer>

    <SmallIconButton onClick={save} title='Save [CTRL+S]'>
      <svg width="26" height="26" style={{marginTop: 0, marginLeft: 2}} viewBox="0 0 26 26">
          <path fill="currentColor" d="M5 3a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V5.5L18.5 3H17v6a1 1 0 0 1-1 1H8a1 1 0 0 1-1-1V3H5m7 1v5h3V4h-3m-5 8h10a1 1 0 0 1 1 1v6H6v-6a1 1 0 0 1 1-1Z"/>
      </svg>
    </SmallIconButton>

    {
      IS_NATIVE && <>
        <SmallIconButton onClick={build} title="Save & Build" >
          <Hammer size={20} color={'white'}/>
        </SmallIconButton>

        <SmallIconButton onClick={buildActive ? undefined : buildAndRun} title="Save, Build & Run [F9]" >
          <BuildIcon size={30} color={buildActive ? Colors.PRIMARY : 'white'}/>
        </SmallIconButton>
      </>
    }
  </Container>;
};

export const TopBar = React.memo(TopBarComponent, (prev, next) => {
  return prev.setSceneRooms === next.setSceneRooms
    && prev.scene === next.scene
    && prev.rooms === next.rooms;
});