/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useCallback, useState } from 'react';
import { ArrowLeftRight, BoxFill, CCircle, Link45deg, Sliders, TagFill } from 'react-bootstrap-icons';
import styled from 'styled-components';
import { Context, Game } from '../../../backend/context';
import { getGameActorData } from '../../../backend/z64/actors/actorData';
import { Actor } from '../../../backend/z64/actors/actorList';
import { patchRotIdx } from '../../../backend/z64/actors/actorState';
import { ActorType } from '../../../backend/z64/types';
import { IconButton } from '../../elements/IconButton';
import { Input } from '../../elements/Input';
import { IntInput } from '../../elements/IntInput';
import { ListContainer, ListItem, ListItemFullWidth, ListItemText } from '../../elements/ListElements';
import { ListHeader } from '../../elements/ListHeader';
import { SelectBoxSearch } from '../../elements/SelectBoxSearch';
import { Credits, LINK_GITLAB, LINK_YOUTUBE, Main, Title, TitleContainer } from '../romSelect/PageRomSelect';
import { ActorParams } from '../scene/barRight/ActorParams';
import packageJson from '../../../../package.json';

const LINK_COLOR = "#bfa63f";
const FAST64_LINK = "https://github.com/Fast-64/fast64/blob/main/fast64_internal/oot/data/xml/ActorList.xml";
const LINK_KIBAKO = "https://kibako.gitlab.io/";

const SmallTitleContainer = styled(TitleContainer)`
  min-height: 150px;
  max-height: 150px;
  user-select: none;

  & img {
    cursor: pointer;
    height: 86px;
    margin-right: 12px;
  }
`;

const SmallTitle = styled(Title)`
  user-select: none;
  cursor: pointer;
  font-size: 3.0em;
  line-height: 0.8;

  &:last-child {
    font-size: 2.5em;
  }
`;

const ArrowSpacer = styled.div`
  width: 75px;
  text-align: center;
`;

const ActorContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  overflow: hidden;
`;

const ActorListContainer = styled(ListContainer)`
  width: 50%;
  min-height: 400px;
  max-width: 360px;
  background-color: #252525;
  border-radius: 4px;
  padding: 13px 8px 8px 8px;
`;

const TextExplainer = styled.div`
  font-size: 16px;
  user-select: none;
  color: white;
  max-height: 30px;
  margin: 2px;
  color: #AAA;
  margin-bottom: 6px;

  display: flex;
  align-items:center;

  & svg {
    margin-top: -2px;
    cursor: pointer;
  }
`;

const LinkContainer = styled.span`
  padding-left: 5px;
  font-weight: bold;
  color: ${LINK_COLOR};
  cursor: pointer;
`;

type SelectSearchType = Array<{name: string, value: string}>;
let selectData: SelectSearchType = [];

function goToMainPage() {
  window.open(LINK_KIBAKO, "_blank")
};

const PageActorParamsComponent = () => {

  Context.game = Game.OOT;
  const [selectedActor, setSelectedActor] = useState("ACTOR_OBJ_KIBAKO");
  
  // Create a dummy actor to work with in the UI
  const [actor, setActor] = useState<Actor>({
    uuid: "dummy",
    type: ActorType.SCENE,
    cutsceneIndex: 0, roomId: 0,
    spawnTime: 0, rotMask: 0,
    id: 0, initValue: 0,
    pos: [0,0,0], rot: [0,0,0],
  });
  
  // Actor-Patcher
  const onChangeParam = useCallback((initValue: number) => setActor({...actor, initValue: Math.min(initValue, 0xFFFF)}), [setActor, actor]);
  const onChangeRot = useCallback((val: number, idx: number) => setActor(patchRotIdx(actor, idx, val)), [setActor, actor]);

  // Fetch Parameter and names
  const actorData = getGameActorData();
  const actorEntry = actorData[selectedActor];
  const actorName = actorEntry?.name || selectedActor || actor.id.toString();

  if(selectData.length === 0) {
    for(const id in actorData) {
      selectData.push({value: id, name: actorData[id].name});
    }
    selectData = selectData.sort((a,b) => a.name.localeCompare(b.name));
  }

  return (
    <Main>
      <SmallTitleContainer>
        <img onClick={goToMainPage} src="./img/box.svg" draggable="false" alt="Logo"/>
        <div onClick={goToMainPage}>
          <SmallTitle>Kibako</SmallTitle>
          <SmallTitle>きばこ</SmallTitle>
        </div>
      </SmallTitleContainer>

      <ActorContainer>
        <ActorListContainer>
          <ListHeader text={"Actor-Settings"} Icon={Sliders} />
          <ListItemText>Actor</ListItemText>
          <ListItem> 
              <SelectBoxSearch name="Actor" options={selectData} value={selectedActor} onChange={setSelectedActor}/>
          </ListItem>

          {(actorEntry && actorEntry.params.length > 0) ? <>
                <ListItemText>Settings</ListItemText>
                <ActorParams 
                  entry={actorEntry} 
                  actor={actor}
                  patchActor={setActor}
                />
              </>
              : <ListItemText>No Settings found!</ListItemText>
            }
        </ActorListContainer>

        <ArrowSpacer>
          <ArrowLeftRight size={45} color={"#222"}/>
        </ArrowSpacer>

        <ActorListContainer>
            <ListHeader text={actorName} Icon={BoxFill} />
            <ListItemText>Parameters</ListItemText>
            <ListItemFullWidth>
              <Input name={"Enum"} value={selectedActor} readOnly></Input>
            </ListItemFullWidth>
            <ListItemFullWidth>
                <IntInput 
                  onChange={onChangeParam} hex={true} 
                  name="Param" value={actor.initValue}/>
            </ListItemFullWidth>
            <ListItem>
                <IntInput index={0} onChange={onChangeRot} name="Rot" value={actor.rot[0]}/>
                <IntInput index={1} onChange={onChangeRot} name=""    value={actor.rot[1]}/>
                <IntInput index={2} onChange={onChangeRot} name=""    value={actor.rot[2]}/>
            </ListItem>
        </ActorListContainer>
      </ActorContainer>

      <TextExplainer>
        Data-Source:
        <LinkContainer onClick={()=> window.open(FAST64_LINK, "_blank")}>Fast64</LinkContainer>
        <Link45deg size={20} color={LINK_COLOR} onClick={goToMainPage}/>
      </TextExplainer>

      <TextExplainer>
        This Tool is part of the OoT/MM Map-Editor:
        <LinkContainer onClick={goToMainPage}>Kibako</LinkContainer>
        <Link45deg size={20} color={LINK_COLOR} onClick={goToMainPage}/>
      </TextExplainer>

      <Credits>
          <IconButton text={"v" + packageJson.version}  Icon={TagFill} 
              onClick={()=> window.open(LINK_GITLAB, "_blank")}
          />

          <IconButton text={"HailToDodongo"}  Icon={CCircle} 
            onClick={()=> window.open(LINK_YOUTUBE, "_blank")} 
          />
      </Credits>
    </Main>
  );
}

export const PageActorParams = React.memo(PageActorParamsComponent, (prev, next) => {
    return true;
});
