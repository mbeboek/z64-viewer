import React, { ChangeEvent, useRef, DragEvent } from 'react';
import { Upload } from 'react-bootstrap-icons';
import styled from 'styled-components';

export const DropzoneContainer = styled.form/*css*/`
    width: 400px;
    height: 250px;
    cursor: pointer;

    & div {
        min-height: 130px;
    }
    &:hover label {
        background-color: rgba(255,127,0, 0.2);
    }

    & input {
        height: 0;
        position: fixed;
        top: 0;
    }
`;

export const DropzoneContent = styled.label/*_css*/`
    width: 100%;
    border: 1px dashed rgb(255,127,0);
    height: 180px;
    display: flex;
    font-size: 18px;
    transition: background-color 100ms linear 0ms;
    align-items: center;
    border-radius: 8px;
    flex-direction: column;
    pointer-events: none;
    justify-content: center;
`;

interface RomSelectorProps {
    onData: (data: Uint8Array) => void;
};

export const RomSelector = (props: RomSelectorProps) => {

    const [dragActive, setDragActive] = React.useState(false);
    const inputRef = useRef<HTMLInputElement>(null);

    const handleDrag = (e: DragEvent) => {
        e.preventDefault();
        e.stopPropagation();
        if (e.type === "dragenter" || e.type === "dragover") {
          setDragActive(true);
        } else if (e.type === "dragleave") {
          setDragActive(false);
        }
    };

    const handleDrop = (e: DragEvent) => {
        e.preventDefault();
        e.stopPropagation();
        setDragActive(false);
        if (e.dataTransfer && e.dataTransfer.files && e.dataTransfer.files[0]) {
            loadFile(e.dataTransfer.files[0]);
        }
    };

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        if (e.target && e.target.files && e.target.files[0]) {
        loadFile(e.target.files[0]);
        }
    };

    const loadFile = (file: File) => {
        var reader = new FileReader();
        reader.onload = file => 
        {
            if(file && file.target) {
                props.onData(new Uint8Array(file.target.result as ArrayBufferLike));
            }
        };
        reader.readAsArrayBuffer(file);
    };

  return (
    <DropzoneContainer
        onSubmit={(e) => e.preventDefault()}
            onDragEnter={handleDrag} 
            onDragLeave={handleDrag} 
            onDragOver={handleDrag} 
            onDrop={handleDrop}
            onClick={() => inputRef.current?.click()}
    >
    <input ref={inputRef} 
        type="file" 
        id="input-file-select"
        multiple={false} 
        onChange={handleChange} 
    />
    <DropzoneContent htmlFor="input-file-select" style={dragActive ? {backgroundColor: 'rgba(255,127,0, 0.2)'} : {}}>
        <span style={{marginBottom: 15}}>Click or Drag &amp; Drop a file</span>
        <Upload size={32} />        
    </DropzoneContent>

</DropzoneContainer>
  );
};