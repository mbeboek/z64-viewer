/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const shader = /*glsl*/`
varying float vertexHeight;

void main() {
  vertexHeight = normalize(position.xyz).y;
  vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}
`;

export default shader;