/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import flags from './faceFlags';

const shader = /*glsl*/`#version 300 es

precision highp float;
precision highp int;

${flags}

// "discards" a vertrex, this is an invalid gl_Position value
#define discardVertex vec4(2.0, 2.0, 2.0, 0.0) 

layout(location = 0) in vec3 position;
layout(location = 1) in ivec4 faceTexIds;
layout(location = 2) in vec4 uv;
layout(location = 3) in vec4 uvEnd;
layout(location = 4) in vec4 normal;

layout(location = 5) in vec4 colorA1;
layout(location = 6) in vec4 colorB1;
layout(location = 7) in vec4 colorC1;
layout(location = 8) in vec4 colorD1;

layout(location = 9) in vec4 colorA2;
layout(location =10) in vec4 colorB2;
layout(location =11) in vec4 colorC2;
layout(location =12) in vec4 colorD2;

// ThreeJS globals
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 cameraPosition;
uniform bool isOrthographic;

uniform float time;

out vec4 combColorA1;
out vec4 combColorB1;
out vec4 combColorC1;
out vec4 combColorD1;

out vec4 combColorA2;
out vec4 combColorB2;
out vec4 combColorC2;
out vec4 combColorD2;

out vec3 uvA;
out vec3 uvB;

out vec2 uvEndA;
out vec2 uvEndB;

flat out ivec2 faceSetting;
out vec3 worldPos;

void main() 
{
  uvA = vec3(uv.xy, float(faceTexIds.z));
  uvB = vec3(uv.zw, float(faceTexIds.w));

  uvEndA = uvEnd.xy;
  uvEndB = uvEnd.zw;

  faceSetting = faceTexIds.xy;
  
  combColorA1 = colorA1;
  combColorB1 = colorB1;
  combColorC1 = colorC1;
  combColorD1 = colorD1;

  combColorA2 = colorA2;
  combColorB2 = colorB2;
  combColorC2 = colorC2;
  combColorD2 = colorD2;

  // front and backface culling, since we have normals and non-indexed faces
  // this can be done by creating degenerate-triangles.
  // this is faster that using discard in the fragment shader
  worldPos = position * 0.125;

  float facingDot = dot(normal.xyz, normalize(worldPos - cameraPosition));

  if((faceTexIds.x & FACE_FLAG_UV_GEN_LINEAR) != 0) {
    float facingDotB = dot(normal.xz, normalize(worldPos - cameraPosition).xz);
  
    uvA.x -= facingDot;
    uvA.y += facingDotB;
  }

  bool isFrontFace = (facingDot < 0.0);
  float faceCull = normal.w;

  float shouldCull = float(
    (faceCull > 0.0 && isFrontFace) ||
    (faceCull < 0.0  && !isFrontFace)
  ); 
  float shouldNotCull = 1.0 - shouldCull;

  gl_Position = shouldNotCull * projectionMatrix * modelViewMatrix * vec4( position, 1.0 )
              + (shouldCull * discardVertex);
}
`;

export default shader;