/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const shader = /*glsl*/`

uniform vec3 bottomColor;
uniform vec3 midColor;
uniform vec3 topColor;

  // vertexHeight has a range of (-1, 1)
varying float vertexHeight;

void main() 
{
  float heightNorm = (vertexHeight + 1.0) * 0.5;
  vec3 gradientTop    = mix(midColor,    topColor, max(0.0,  vertexHeight));
  vec3 gradientBottom = mix(midColor, bottomColor, max(0.0, -vertexHeight));
  
  gl_FragColor = vec4(
      mix(gradientBottom, gradientTop, heightNorm),
      1.0
    );
}
`;

export default shader;