/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const shader = /*glsl*/`
#include <common>

varying vec3 fragColor;

void main() {
  fragColor = transformDirection(normal, modelViewMatrix) * 0.5 + 0.5;
  vec4 worldPosition = modelMatrix * vec4( position, 1.0 );

  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}
`;

export default shader;