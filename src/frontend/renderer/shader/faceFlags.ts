/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const flags = /*glsl*/`

// Flags for index 0
  #define FACE_FLAG_TEX_0_U_CLAMP  (1 << 0)
  #define FACE_FLAG_TEX_0_U_MIRROR (1 << 1)
  #define FACE_FLAG_TEX_0_V_CLAMP  (1 << 2)
  #define FACE_FLAG_TEX_0_V_MIRROR (1 << 3)

  #define FACE_FLAG_TEX_1_U_CLAMP  (1 << 4)
  #define FACE_FLAG_TEX_1_U_MIRROR (1 << 5)
  #define FACE_FLAG_TEX_1_V_CLAMP  (1 << 6)
  #define FACE_FLAG_TEX_1_V_MIRROR (1 << 7)


// Flags for index 1
  #define FACE_FLAG_CCMUX_A2_COMBINED (1 << 0)
  #define FACE_FLAG_CCMUX_B2_COMBINED (1 << 1)
  #define FACE_FLAG_CCMUX_C2_COMBINED (1 << 2)
  #define FACE_FLAG_CCMUX_D2_COMBINED (1 << 3)

  #define FACE_FLAG_ACMUX_A2_COMBINED (1 << 4)
  #define FACE_FLAG_ACMUX_B2_COMBINED (1 << 5)
  #define FACE_FLAG_ACMUX_C2_COMBINED (1 << 6)
  #define FACE_FLAG_ACMUX_D2_COMBINED (1 << 7)

// Color/Alpha Mixer settings
#define FACE_FLAG_CCMUX_A1_FETCH (1 <<  8) // overwrite A1 in shader with...
#define FACE_FLAG_CCMUX_A1_TEX   (1 <<  9) // ... texture[0] or texture[1]...
#define FACE_FLAG_CCMUX_A1_ALPHA (1 << 10) // ...alpha if set, or color if not

#define FACE_FLAG_CCMUX_B1_FETCH (1 << 11) // overwrite B1 in shader with...
#define FACE_FLAG_CCMUX_B1_TEX   (1 << 12) // ... texture[0] or texture[1]...
#define FACE_FLAG_CCMUX_B1_ALPHA (1 << 13) // ...alpha if set, or color if not

#define FACE_FLAG_CCMUX_C1_FETCH (1 << 14) // overwrite C1 in shader with...
#define FACE_FLAG_CCMUX_C1_TEX   (1 << 15) // ... texture[0] or texture[1]...
#define FACE_FLAG_CCMUX_C1_ALPHA (1 << 16) // ...alpha if set, or color if not

#define FACE_FLAG_CCMUX_D1_FETCH (1 << 17) // overwrite C1 in shader with...
#define FACE_FLAG_CCMUX_D1_TEX   (1 << 18) // ... texture[0] or texture[1]...
#define FACE_FLAG_CCMUX_D1_ALPHA (1 << 19) // ...alpha if set, or color if not

#define FACE_FLAG_ACMUX_A1_FETCH (1 << 20) // overwrite A1 in shader with alpha of...
#define FACE_FLAG_ACMUX_A1_TEX   (1 << 21) // ... texture[0] or texture[1]...

#define FACE_FLAG_ACMUX_B1_FETCH (1 << 22) // overwrite B1 in shader with alpha of...
#define FACE_FLAG_ACMUX_B1_TEX   (1 << 23) // ... texture[0] or texture[1]...

#define FACE_FLAG_ACMUX_C1_FETCH (1 << 24) // overwrite C1 in shader with alpha of...
#define FACE_FLAG_ACMUX_C1_TEX   (1 << 25) // ... texture[0] or texture[1]...

#define FACE_FLAG_ACMUX_D1_FETCH (1 << 26) // overwrite B1 in shader with alpha of...
#define FACE_FLAG_ACMUX_D1_TEX   (1 << 27) // ... texture[0] or texture[1]...

#define FACE_FLAG_UV_GEN_LINEAR (1 << 28) // ... texture[0] or texture[1]...

// Helper
#define checkFlag(flag) ((faceSetting.x & flag) != 0)
#define getFlagToggle(flag) (float(checkFlag(flag)))
#define flagSelect(flag, a, b) (mix(a, b, float(checkFlag(flag))))


#define flagSelectX(flag, a, b) (mix(a, b, float( (faceSetting.x & flag) != 0 )))
#define flagSelectY(flag, a, b) (mix(a, b, float( (faceSetting.y & flag) != 0 )))

`;

export default flags;