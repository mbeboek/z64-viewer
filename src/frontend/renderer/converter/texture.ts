/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { GpuTexture } from "backend/z64/gpu/data/texture";
import { Texture, DataTexture, RGBAFormat, RepeatWrapping, LinearFilter, Data3DTexture } from "three";
import { Z64Emulator } from "../../../backend/z64/emulator/z64Emulator";

export interface UniformTexture3D {
    value: Data3DTexture | undefined
}

export const gpuTextureToThreeJS = (gpuTex: GpuTexture): Texture => {
    const tex =  new DataTexture(gpuTex.rgbaBuffer, gpuTex.tile.width, gpuTex.tile.height, RGBAFormat);
    
    tex.wrapS = RepeatWrapping;
    tex.wrapT = RepeatWrapping;

    tex.magFilter = LinearFilter;
    tex.minFilter = LinearFilter;

    return tex;
};

export const gpuTexturesTo3DThreeJS = (gpuTextures: GpuTexture[]): Data3DTexture | undefined => {

    if(gpuTextures.length === 0) {
        return undefined;
    }

    // get the biggest combined texture-dimension
    const maxWidth = Math.max(...gpuTextures.map(tex => tex.tile.width));
    const maxHeight = Math.max(...gpuTextures.map(tex => tex.tile.height));
    const layers = gpuTextures.length;

    // sanity check
    if(maxWidth > 128 || maxHeight > 128) {
        console.warn("Weird image sizes detected (%fx%d)!", maxWidth, maxHeight);
    }

    const byteSize = maxWidth * maxHeight * 4 * layers;

    const buffer = new Uint8Array(byteSize);
    let offset = 0;

    const emu = Z64Emulator.getInstance();
    const bufferInPtr = emu.mipsi.z64TextureGetUpscaleInputPtr();

    //console.log("3DTexture byte-size: " + byteSize + " | " + (byteSize / 1024) + "kb");

    // Copy each (possibly up-scaled) texture into the bigger 3d-buffer
    for(const gpuTexture of gpuTextures) 
    {
        let imageBuffer: Uint8Array | Uint8ClampedArray = gpuTexture.rgbaBuffer;
        const [width, height] = [gpuTexture.tile.width, gpuTexture.tile.height];
        if(gpuTexture.rgbaBuffer.length !== 0 && (maxWidth !== width || maxHeight !== height))
        {
            //console.time("Resize");

            emu.copyIntoMemory(gpuTexture.rgbaBuffer, bufferInPtr);
            const bufferOutPtr = emu.mipsi.z64TextureUpscaleWrapUV(width, height, maxWidth, maxHeight);
            emu.copyFromMemory(buffer, maxWidth*maxHeight*4, bufferOutPtr, offset);

            //console.timeEnd("Resize");
            //console.log([width, height], " => ", [maxWidth, maxHeight]);
        } else{
            buffer.set(imageBuffer, offset);
        }

        offset += (maxWidth * maxHeight * 4);
    }

    // create actual texture object and set params
    const tex =  new Data3DTexture(buffer, maxWidth, maxHeight, layers);
    tex.format = RGBAFormat;
    
    tex.wrapS = RepeatWrapping;
    tex.wrapT = RepeatWrapping;

    tex.magFilter = LinearFilter;
    tex.minFilter = LinearFilter;
    tex.needsUpdate = true;

    return tex;
};