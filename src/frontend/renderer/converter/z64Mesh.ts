/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BufferGeometry, Data3DTexture, InterleavedBuffer as InterlBuff, InterleavedBufferAttribute as InterlBuffAttr } from "three";
import { gpuTexturesTo3DThreeJS } from "./texture";
import { Z64Mesh, Z64MeshObjectType } from "backend/z64/mesh/mesh";
import { GpuObjectVertexSize } from "backend/z64/gpu/data/object";

// transformation from Z64 space to threejs
export const GLOBAL_OBJ_SCALE = 0.125;

/**
 * Cache entry that contains already created ThreeJS objects
 */
interface ThreeJsMeshObject {
  geometry: BufferGeometry;
  texture: Data3DTexture | undefined;
}

export const z64MeshToThreeJS = (z64Mesh: Z64Mesh, type: Z64MeshObjectType, geometry: BufferGeometry): ThreeJsMeshObject => {

    const gpuObject = type === Z64MeshObjectType.OPAQUE ? z64Mesh.gpuObjectOpaque : z64Mesh.gpuObjectTransparent;
 
    const buffF32 = new Float32Array(gpuObject.buffer);
    const buffS32 = new Int32Array(gpuObject.buffer);
    const buffS8 = new Int8Array(gpuObject.buffer);
    const buffU8 = new Uint8Array(gpuObject.buffer);

    const stride = GpuObjectVertexSize;

  //const a = new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x34  ,  false);

    //                                                                               stride,  count, offset, normalize
    geometry.setAttribute('position',     new InterlBuffAttr(new InterlBuff(buffF32, stride/4),  3,  0x00/4,  false));
    geometry.setAttribute('faceTexIds',   new InterlBuffAttr(new InterlBuff(buffS32, stride/4),  4,  0x0C/4,  false));
    geometry.setAttribute('uv',           new InterlBuffAttr(new InterlBuff(buffF32, stride/4),  4,  0x1C/4,  false));
    geometry.setAttribute('uvEnd',        new InterlBuffAttr(new InterlBuff(buffF32, stride/4),  4,  0x2C/4,  false));
    geometry.setAttribute('normal',       new InterlBuffAttr(new InterlBuff(buffS8,  stride  ),  4,  0x3C  ,   true));
    geometry.setAttribute('colorA1',      new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x40  ,   true));
    geometry.setAttribute('colorB1',      new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x44  ,   true));
    geometry.setAttribute('colorC1',      new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x48  ,   true));
    geometry.setAttribute('colorD1',      new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x4C  ,   true));
    geometry.setAttribute('colorA2',      new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x50  ,   true));
    geometry.setAttribute('colorB2',      new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x54  ,   true));
    geometry.setAttribute('colorC2',      new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x58  ,   true));
    geometry.setAttribute('colorD2',      new InterlBuffAttr(new InterlBuff(buffU8,  stride  ),  4,  0x5C  ,   true));
    
    return {
      geometry, 
      texture: gpuTexturesTo3DThreeJS(gpuObject.textures)
    };
};
