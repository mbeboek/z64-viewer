/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useEffect, useState, useRef } from 'react';
import { useThree, useFrame } from '@react-three/fiber';
import { Vector3, Group } from 'three';
import { TransformControls } from './plugins/TransformControls';
import { GLOBAL_OBJ_SCALE } from './converter/z64Mesh';
import { radAngleToS16, s16AngleToRad } from 'backend/z64/math/angle';
import { preventDefault } from '../helper/events';
import { vec3Add, vec3Diff } from '../../backend/utils/math';
import { patchPos, patchRot } from '../../backend/z64/actors/actorState';

const EVENT_NAMES_KEY: (keyof GlobalEventHandlersEventMap)[] = ["keydown", "keyup"];
const EVENT_NAMES_MOUSE: (keyof GlobalEventHandlersEventMap)[] = ["mousemove", "mousedown", "mouseup"];

const CAM_SPEED = 0.025;
const CAM_ROT_SPEED = 0.0035;

const KEYCODE_MOUSE_LEFT = 1;
const KEYCODE_MOUSE_RIGHT = 2;

let transformCtrl: TransformControls|undefined = undefined;

const state = {
  lastTime: window.performance.now(),
  camMoveDir: new Vector3(0, 0, 0),
  camTargetMoveDir: new Vector3(0, 0, 0),

  camMoveSpeed: 1,
  camMoveSpeedTarget: 1,
  mouseDown: false,
  cameraActive: true,

  modeRotate: false,
  modeLocal: false,

  keys: {
    left: false,
    right: false,
    forward: false,
    backward: false,
    up: false,
    down: false,
    fast: false,
    snap: false,
  }
}

type Vec3 = [number, number, number];
type TransformCallback = (newObjects: TransformObject[]) => void;
type StopCallback = () => void;

let refGroup: Group|undefined = undefined;

export interface TransformObject {
  uuid: string;
  pos: Vec3;
  rot: Vec3; 
}

let transRefPos: Vec3 = [0,0,0];
let transRefRot: Vec3 = [0,0,0];

const transformObjects = new Map<string, TransformObject>();

let transformCb: TransformCallback = () => {};
let stopCb: StopCallback = () => {};

export function enableTransform(objects: TransformObject[], callbackTrans: TransformCallback, callbackStop: StopCallback)
{
  if(objects.length === 0 || !transformCtrl || !refGroup)return;
  transformCb = callbackTrans;
  stopCb = callbackStop;

  transformObjects.clear();
  for(const obj of objects) {
    transformObjects.set(obj.uuid, obj);
  }

  //const refObject = objects[objects.length-1];
  const refObject = objects[0];
  transRefPos = refObject.pos;
  transRefRot = refObject.rot;

  if(transformCtrl && refGroup) {
    refGroup.position.set(
      transRefPos[0] * GLOBAL_OBJ_SCALE, 
      transRefPos[1] * GLOBAL_OBJ_SCALE, 
      transRefPos[2] * GLOBAL_OBJ_SCALE
    );

    refGroup.rotation.set(
      s16AngleToRad(transRefRot[0]),
      s16AngleToRad(transRefRot[1]),
      s16AngleToRad(transRefRot[2]),
      "YXZ"
    );

    transformCtrl.attach(refGroup);
  }
}

export function disableTransform(fullReset?: boolean) {
  transformObjects.clear();
  if(fullReset)transformCtrl?.detach();
}

function onTransform()
{
  if(!refGroup || transformObjects.size === 0)return;

  if(!state.modeRotate) 
  {
    const newPos: Vec3 = [
      Math.round(refGroup.position.x / GLOBAL_OBJ_SCALE),
      Math.round(refGroup.position.y / GLOBAL_OBJ_SCALE),
      Math.round(refGroup.position.z / GLOBAL_OBJ_SCALE),
    ];
    const offsetPos = vec3Diff(newPos, transRefPos);

    transformCb([...transformObjects.values()]
      .map(obj => patchPos(obj, vec3Add(obj.pos, offsetPos)))
    );

  } else {
    const newRot: Vec3 = [
      radAngleToS16(refGroup.rotation.x),
      radAngleToS16(refGroup.rotation.y),
      radAngleToS16(refGroup.rotation.z),
    ];
  
    const offsetRot = vec3Diff(newRot, transRefRot);
    transformCb([...transformObjects.values()]
      .map(obj => patchRot(obj, vec3Add(obj.rot, offsetRot)))
    );
  }
}

export const ControlsFPS = () => {

  const { camera, gl, scene } = useThree();
  const [init] = useState(false);

  const transformRef = useRef<Group>(null);
  if(transformRef.current) {
    refGroup = transformRef.current;
  }

  const canvasNode = gl.domElement;

  const handleKeys = (ev: Event) => {
    const e = ev as KeyboardEvent; // typescript being stupid
    //console.log(e);
    let isKeyDown = (e.type === "keydown");
    if(e.ctrlKey)isKeyDown = false;

    switch (e.code) 
    {
      case "ArrowRight": case "KeyD": state.keys.right = isKeyDown; break;
      case "ArrowLeft": case "KeyA": state.keys.left = isKeyDown; break;
      case "ArrowUp": case "KeyW": state.keys.forward = isKeyDown; break;
      case "ArrowDown": case "KeyS": state.keys.backward = isKeyDown; break;
      
      case "KeyQ": state.keys.down = isKeyDown; break;
      case "KeyE": state.keys.up = isKeyDown; break;

      case "ShiftLeft": case "ShiftRight": state.keys.fast = isKeyDown; break;
      case "ControlLeft": case "Controlright": 
        state.keys.snap = isKeyDown; 
        /*if(state.keys.snap && transformCtrl) {
          transformCtrl.setTranslationSnap(100.0);
        }*/
      break;

      case "KeyR": 
        if(isKeyDown && transformCtrl) {
          state.modeRotate = !state.modeRotate;
          transformCtrl.setMode(state.modeRotate ? 'rotate' : 'translate');
        }
      break;

      case "KeyF": 
        if(isKeyDown && transformCtrl) {
          state.modeLocal = !state.modeLocal;
          transformCtrl.setSpace(state.modeLocal ? 'local' : 'world');
        }
      break;
    }

    preventDefault(e);
  };

  const handleMouse = (ev: Event) => {
    const e = ev as MouseEvent; // typescript being stupid
    if(!state.cameraActive)return;

    if (!(e.buttons & (KEYCODE_MOUSE_LEFT | KEYCODE_MOUSE_RIGHT)))
      return;

    if(e.type !== "mousemove") 
    {
      state.mouseDown = (e.type === "mousedown");
    }
    else if (state.mouseDown) 
    {
      camera.rotation.x -= e.movementY * CAM_ROT_SPEED;
      camera.rotation.y -= e.movementX * CAM_ROT_SPEED;
    }
  };

  useFrame(() => {
    const now = window.performance.now();
    const deltaTime = now - state.lastTime;

    //fpsState.camMoveSpeed = fpsState.camMoveDir.lengthSq() === 0 ? 0 : CAM_SPEED;

    const speed = state.keys.fast ? 3.0 : 1.0;
    if(state.keys.right   )state.camMoveDir.x =  speed;
    if(state.keys.left    )state.camMoveDir.x = state.keys.right ? 0 : -speed;
    if(state.keys.forward )state.camMoveDir.z = -speed;
    if(state.keys.backward)state.camMoveDir.z = state.keys.forward ? 0 : speed;
    if(state.keys.up      )state.camMoveDir.y =  speed;
    if(state.keys.down    )state.camMoveDir.y = state.keys.up ? 0 : -speed;
    
    camera.translateOnAxis(
      state.camMoveDir,
      state.camMoveSpeed * CAM_SPEED * deltaTime
    );

    const speedDecay = 0.18;
    state.camMoveDir = state.camMoveDir.lerp(state.camTargetMoveDir, speedDecay);
    state.camMoveSpeed = state.camMoveSpeedTarget * speedDecay + state.camMoveSpeed * (1.0 - speedDecay);
    state.lastTime = now;
  });

  useEffect(() => {

    camera.rotation.order = 'YXZ';
    canvasNode.tabIndex = 1;
    camera.position.y = 10;

    EVENT_NAMES_KEY.forEach(name => canvasNode.addEventListener(name, handleKeys));
    EVENT_NAMES_MOUSE.forEach(name => canvasNode.addEventListener(name, handleMouse));

    canvasNode.oncontextmenu = (e: Event) => {
      e.preventDefault();
      e.stopPropagation();
    };

    // Object Transform
    console.log("init controls");
    
    if(transformCtrl) {
      scene.remove(transformCtrl);
      transformCtrl.dispose();
    }

    transformCtrl = new TransformControls(camera, gl.domElement);
    transformCtrl.space = "global";

		transformCtrl.addEventListener('objectChange', e => {
      if(e.target)onTransform();
    });

    transformCtrl.addEventListener( 'dragging-changed', (e) => {
      state.cameraActive = !e.value;
      if(stopCb && !e.value)stopCb();
    });

    scene.add(transformCtrl);

    return () => {
      camera.rotation.order = 'XYZ';
      canvasNode.tabIndex = 0;
      EVENT_NAMES_KEY.forEach(name => canvasNode.removeEventListener(name, handleKeys));
      EVENT_NAMES_MOUSE.forEach(name => canvasNode.removeEventListener(name, handleMouse));
      refGroup = undefined;
      if(transformCtrl) {
        scene.remove(transformCtrl);
        transformCtrl.dispose();
      }
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [camera, scene, init]);

  return (
    <group ref={transformRef}></group>
  );
};