/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useEffect, useRef } from 'react';
import { Mesh, BoxGeometry, MeshStandardMaterial, TextureLoader, NearestFilter, sRGBEncoding } from 'three';
import { OBJLoader  } from "three/examples/jsm/loaders/OBJLoader";
import { layerActor } from '../raycast';

const ACTOR_BOX_SCALE = 20;

// SUPERGLOBAL: since this component is static, create the buffer/texture only once per page-load
let geometry: BoxGeometry | undefined;
let material: MeshStandardMaterial | undefined;
let materialSpecial: MeshStandardMaterial | undefined;

interface DefaultCubeProps {
  special?: boolean;
  hidden?: boolean;
};

function loadMaterial(path: string, mat: MeshStandardMaterial) {
  new TextureLoader().load(path, texture => {
    if(mat) {
      texture.minFilter = NearestFilter;
      texture.magFilter = NearestFilter;
      texture.encoding = sRGBEncoding;
      mat.map = texture;
      mat.needsUpdate = true;
    }
  });
}

const DefaultCubeComponent = (props: DefaultCubeProps) => {
  const meshRef = useRef<Mesh>(null);

  useEffect(() => 
  {
    if(!geometry) {
      // Initial dummy box to avoid popping-in
      geometry = new BoxGeometry(1,1,1, 1,1,1); // size-XYZ, segments-XYZ
      geometry.scale(ACTOR_BOX_SCALE, ACTOR_BOX_SCALE, ACTOR_BOX_SCALE);

      const loader = new OBJLoader();
      loader.load("models/cube.obj", model => {
        if(geometry && model && model.children.length > 0) {
          const boxGeom = (model.children[0] as any).geometry;
          geometry.copy(boxGeom); // copy data to no screw-up references
          geometry.scale(ACTOR_BOX_SCALE, ACTOR_BOX_SCALE, ACTOR_BOX_SCALE);
        }
      });
    }

    if(props.special) {
      if(!materialSpecial) {
        materialSpecial = new MeshStandardMaterial();
        loadMaterial('models/cubeScene.png', materialSpecial);
      }
    } else {
      if(!material) {
        material = new MeshStandardMaterial();
        loadMaterial('models/cube.png', material);
      }
    }
    
    if(meshRef.current) {
      meshRef.current.geometry = geometry;
      meshRef.current.material = (props.special ? materialSpecial : material) || [];
      meshRef.current.visible = !props.hidden;
    }
  }, [props.hidden, props.special]);

  return (<mesh ref={meshRef} layers={layerActor()} ></mesh>);
};

export const DefaultCube = React.memo(DefaultCubeComponent, (prev, next) => {
  return prev.hidden === next.hidden;
});