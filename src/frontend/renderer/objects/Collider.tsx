/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
import React from 'react';
import { ColliderData } from "../../../backend/z64/emulator/collisionEmuBridge";
import { CylinderGeometry, DoubleSide, MeshStandardMaterial, SphereGeometry } from 'three';

interface Props {
  collider: ColliderData;
}

// Globals
const materials = [
  new MeshStandardMaterial({
    transparent: true, opacity: 0.3,
    side: DoubleSide,
    color: 0xFF0000,
  }),
  new MeshStandardMaterial({
    transparent: true, opacity: 0.3,
    side: DoubleSide,
    color: 0x0000FF,
  }),
  new MeshStandardMaterial({
    transparent: true, opacity: 0.3,
    side: DoubleSide,
    color: 0xFFFFFF,
  }),
];

const geomCylinder = new CylinderGeometry(1, 1, 1, 14);
const geomSphere = new SphereGeometry(1, 12, 8);

const ColliderComponent = (props: Props) => {

  const collider = props.collider;

  return <>
      {collider.cylinder.map((cyl, i) => 
        <mesh key={cyl.uuid} 
          position={[cyl.pos[0], cyl.pos[1] + cyl.shiftY, cyl.pos[2]]} 
          scale={[cyl.radius, cyl.height, cyl.radius]}
          geometry={geomCylinder}
          material={materials[cyl.type]}
        ></mesh>
      )}
      
      {collider.spheres.map((sph, i) => 
        <mesh key={sph.uuid} 
          position={[sph.pos[0], sph.pos[1], sph.pos[2]]} 
          scale={sph.radius}
          geometry={geomSphere}
          material={materials[sph.type]}
        ></mesh>
      )}

      {collider.triangles.map((tri, i) => 
        <mesh key={tri.uuid} material={materials[tri.type]}>
          <bufferGeometry index={null}>
            <bufferAttribute
                  attach='attributes-position'
                  array={tri.vertices}
                  count={tri.vertices.length / 3}
                  itemSize={3}
              />
              <bufferAttribute
                attach='attributes-normal'
                array={tri.normals}
                count={tri.normals.length / 3}
                itemSize={3}
            />
          </bufferGeometry>
        </mesh>
      )}
  </>;
};

export const Collider = React.memo(ColliderComponent, (prev, next) => {
  return prev.collider === next.collider;
});